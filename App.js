/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import Login from './src/component/login';
import Authentication from './src/component/authentication';
import Home from './src/component/home';
import CreditCardList from './src/component/creditcardlist';
import Offers from './src/component/offers';
import Rewards from './src/component/rewards';
import MyRewards from './src/component/myrewards';
import FaqScreen from './src/component/faqscreen';
import Apply from './src/component/apply';
import ApplySecondLevel from './src/component/applysecondlevel';
import ApplyThirdLevel from './src/component/applythirdlevel';
import ApplyFourthLevel from './src/component/applyfourthlevel';
import ApplyFifthLevel from './src/component/applyfifthlevel';
import EmiratesIdScan from './src/component/emiratesidscan';
import OTPVerification from './src/component/otpverifyscreen';
import EmiratesIdSample from './src/component/emiratesidsample';
import UploadFile from './src/component/uploadfile';
import MyMoney from './src/component/mymoneycard';
import EMICalculator from './src/component/emicalculator';
import SwiftCode from './src/component/swiftcode';
import CurrencyConvert from './src/component/currencyconvertor';
import GoldConvertor from './src/component/goldconvertor';
import MyGoals from './src/component/mygoals';
import FingerprintPopup from './src/component/fingerprint';
import OfferSelector from './src/component/offerselector';


import ApplyCreditCard from './src/component/applycreditcard';
import ApplySixthLevel from './src/component/applysixthlevel';

import CheckNow from './src/component/checknowform';
import CheckNow2 from './src/component/checknowform2';
import BankScan from './src/component/bankscan';
import PaymentScreen from './src/component/paymentscreen';
import PersonalLoanDetails from './src/component/personalloandetails';
import CreditCardDetails from './src/component/creditcarddetails';
import OtherLoans from './src/component/otherloans';
import Refer from './src/component/refer';
import Account from './src/component/account';

import FormDetails from './src/component/formdetails';


import CreditScore from './src/component/creditscore';

import { createStackNavigator, createAppContainer } from "react-navigation";


const AppNavigator = createStackNavigator({
  

  

  Home: {
    screen: Home
    
  },   Apply: {
    screen: Apply
    
  },  
  MyMoney: {
    screen: MyMoney
    
  }, 
  EmiratesIdScan:{
    screen:EmiratesIdScan
  },
 
  CreditScore: {
    screen: CreditScore
    
  }, 
  Login: {
    screen: Login
  },
  Authentication: {
    screen: Authentication
    
  }, 
  
  Offers: {
    screen: Offers
    
  }, 
  
  Rewards: {
    screen: Rewards
    
  }, 
   
  MyRewards: {
    screen: MyRewards
    
  }, 


   
  ApplySecondLevel: {
    screen: ApplySecondLevel
    
  }, 
  ApplyThirdLevel: {
    screen: ApplyThirdLevel
    
  }, 
  ApplyFourthLevel: {
    screen: ApplyFourthLevel
    
  },
  OTPVerification:{
    screen:OTPVerification
  }, 
  EmiratesIdSample:{
    screen:EmiratesIdSample
  },
  UploadFile:{
    screen:UploadFile
  }, 
  ApplyFifthLevel: {
    screen: ApplyFifthLevel
    
  },


  FaqScreen: {
    screen: FaqScreen
    
  }, 
  CreditCardList: {
    screen: CreditCardList
    
  }, 
  EMICalculator: {
    screen: EMICalculator
    
  },  
  SwiftCode: {
    screen: SwiftCode
    
  }, 
  CurrencyConvert: {
    screen: CurrencyConvert
    
  },
  
  GoldConvertor: {
    screen: GoldConvertor
    
  }, 
  MyGoals: {
    screen: MyGoals
    
  },   
  CheckNow: {
    screen: CheckNow
    
  }, 
  CheckNow2: {
    screen: CheckNow2
    
  }, 
  

  FingerprintPopup: {
    screen: FingerprintPopup
    
  }, 
  ApplyCreditCard: {
    screen: ApplyCreditCard
    
  },  
   ApplySixthLevel: {
    screen: ApplySixthLevel
    
  },   
  BankScan: {
   screen: BankScan
   
 }, 
 PaymentScreen: {
  screen: PaymentScreen
  
}, 
OfferSelector: {
  screen: OfferSelector
  
}, 

PersonalLoanDetails: {
  screen: PersonalLoanDetails
  
}, 
CreditCardDetails: {
  screen: CreditCardDetails
  
}, 

OtherLoans: {
  screen: OtherLoans
  
}, 
Refer: {
  screen: Refer
  
}, 
Account: {
  screen: Account
  
}, 
FormDetails: {
  screen: FormDetails
  
}, 
  
  
});

export default createAppContainer(AppNavigator);