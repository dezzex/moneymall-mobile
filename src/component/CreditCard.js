import React, { Component } from 'react'
import { View, Text ,Image,TouchableOpacity,PickerIOS,Picker,ActivityIndicator} from 'react-native';
import {width,height} from 'react-native-dimension';
import { Rating, AirbnbRating } from 'react-native-ratings';

export default class CreditCard extends Component {

    onApplyClicked(){
        if(this.props.index === 1){
            this.props.navigation.navigate("PersonalLoanDetails",{value:this.props.value,loan:this.props.loan,tenure:this.props.tenure,index:this.props.index});
   
        }
        else{
            this.props.navigation.navigate("CreditCardDetails",{value:this.props.value,loan:this.props.loan,tenure:this.props.tenure,index:this.props.index});
   
        }
   }

    render() {
        
        var value = this.props.value;
        return (
            <TouchableOpacity 
            style={{height:height(18),width:width(95),margin:height(2),borderWidth:1,borderColor:"#E6E6E6",borderRadius:12,alignItems:"center",justifyContent:"center",flexDirection:"row"}}
            onPress={this.onApplyClicked.bind(this)}>
                
                <View style={{width:width(39),height:height(15),alignItems:"center",justifyContent:"center",marginLeft:width(2),marginTop:height(4)}}>
                    {/* <Image source={require("../image/cashback.png")} style={{width:width(20),height:height(4),position:"absolute",top:-height(1),bottom:0,left:width(8),right: 0}} resizeMode="contain"/> */}
                   <View style={{width:width(30),height:height(12)}}>
                    <Image  source={{uri: value.card_image}} style={{width:width(30),height:height(12),borderRadius:12}} resizeMode="contain"/>
                    </View>
                    {/* <View style={{width:width(30),height:height(3)}}>
                    <Rating
                        ratingCount={3}
                        imageSize={12}
                        />
                        </View> */}
                </View>
                <View style={{width:width(50),height:height(15),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(60),height:height(6),alignItems:"center",justifyContent:"center"}}>
                <Text style={{flex: 1, flexWrap: 'wrap',fontSize:16,fontWeight:"bold",fontFamily:"Ubuntu"}}> {value.name} </Text>
                </View>
                <View style={{width:width(52),height:height(10)}}>
                    <View style={{width:width(52),height:height(5),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                        <View style={{width:width(24),height:height(2.5),alignItems:"center",justifyContent:"center"}}>
                        <Text style={{fontSize:value.annual_fees !== undefined ? value.annual_fees.includes("AED") ? 22 : 14 : 14,fontFamily:"Ubuntu-Bold"}}>

                            {value.monthly_repayment !== undefined ? value.monthly_repayment : value.annual_fees !== undefined ? value.annual_fees.includes("AED") ? value.annual_fees.replace("AED"," ") :    Number.isInteger(value.annual_fees.split(" ")[1]) ? value.annual_fees.split(" ")[1] : "0"
                : ""}
                        </Text>
                        </View>
                        {this.props.index === 1
                        ?
                        <View style={{width:width(24),height:height(2.5),alignItems:"center",justifyContent:"center"}}>
                        <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>
                            {value.cost_borrowing !== undefined ? value.cost_borrowing : value.fxrate}
                        </Text>
                        </View>
                        :
                        <View style={{width:width(24),height:height(2.5),alignItems:"center",justifyContent:"center"}}>
                            <TouchableOpacity style={{borderColor:this.props.value.id % 2 === 0 ? "red" : "green",borderWidth:2,borderRadius:12,width:width(18),height:height(4),alignItems:"center",justifyContent:"center"}}>
                                <Text style={{color:this.props.value.id % 2 === 0 ? "red" : "green",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                                {this.props.value.id % 2 === 0 ? "LOW" : "HIGH"}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        }
                    </View>
                    <View style={{width:width(52),height:height(5),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(24),height:height(2.5),alignItems:"center",justifyContent:"center"}}>
                        <View >
                        {value.monthly_repayment !== undefined ? <View><Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}> Monthly</Text>
                        <Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}> Repayment</Text></View> :
                        <View><Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}> Annual</Text>
                        <Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}> Fees</Text></View>
                        }
                        </View>
                        </View>
                        <View style={{width:width(24),height:height(2.5),alignItems:"center",justifyContent:"center"}}>
                        <View >
                        {value.monthly_repayment !== undefined ? <View><Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}> Cost of</Text>
                        <Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}> Borrowing</Text></View> : 

                           <View><Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}> Chances</Text>
                           <Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}>of Approval</Text></View>}
                        </View>
                        </View>
                    </View>
                </View>
                </View>
                <TouchableOpacity style={{width:width(6),height:height(15),alignItems:"center",justifyContent:"center"}} onPress={this.onApplyClicked.bind(this)}>
                <Image source={require("../image/goicon.png")} style={{width:width(5),height:height(3)}} resizeMode="contain"/>
                    
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }
}
