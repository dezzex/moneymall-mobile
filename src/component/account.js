//import liraries
import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,Picker,ActivityIndicator,AsyncStorage, TextInput} from 'react-native';
import {width,height} from 'react-native-dimension';

import LinearGradient from 'react-native-linear-gradient';

// create a component
class Account extends Component {
    static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:<TouchableOpacity style={{flexDirection:"row",width:width(25),alignItems:"center",justifyContent:"center"}} onPress={() => navigation.navigate("Home")}>
            <Image source={require("../image/back.png")} style={{width:width(3),height:height(3)}}/>
            <View style={{height:height(4),width:width(15),alignItems:"center",justifyContent:"center"}}>
            <Text style={{fontSize:18,fontFamily:"Ubuntu-Regular",color:"#fff"}}>Back</Text>
            </View>
        </TouchableOpacity>,
        headerStyle:{
          height:height(5),
          borderBottomWidth:0,
        backgroundColor: '#1972d7',
        }
    }
      };
    render() {
        return (
            <View style={styles.container}>
                <View  style={{width:width(100),height:height(20),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:24,color:"#fff",fontFamily:"Ubuntu-Regular"}}>DASHBOARD</Text>
                </View>
                <View  style={{width:width(85),height:height(25)}}>
                <View style={{width:width(85),height:height(15),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",borderRadius:18}}>
                <TouchableOpacity style={{width:width(25),height:width(25),borderRadius:width(25)/2,alignItems:"center",justifyContent:"center",backgroundColor:"#fff"}}>
                <Image source={require("../image/avatar.png")} style={{width:width(20),height:width(20),borderRadius:width(20)/2}} resizeMode="contain"/>
                </TouchableOpacity>
                <View style={{width:width(85),height:height(15),alignItems:"center",justifyContent:"flex-start"}}>
                <Text style={{fontSize:22,color:"#747474",fontFamily:"Ubuntu-Regular"}}>
                       Hello , Ahmed !
                    </Text> 
                    <Text style={{fontSize:14,color:"#747474",fontFamily:"Ubuntu-Regular"}}>
                       Last Login:10:33 AM,08.05.2020
                    </Text> 
                </View>
                
                </View>
               
                
                </View>
                <LinearGradient style={{borderRadius:25,alignItems:"center",justifyContent:"flex-start",width:width(85),height:height(25)}}
                 colors={["#1870cf","#1566bc","#125ea5","#0f578e"]} >
                     <View style={{height:height(10),width:width(65),alignItems:"flex-start",justifyContent:"flex-start",borderBottomColor:"#1972d7",borderBottomWidth:1}}>
                        <Text style={{fontSize:22,color:"#fff",fontFamily:"Ubuntu-Regular",marginBottom:height(2)}}>
                            Your Applications
                        </Text>

                        <Text style={{fontSize:12,color:"#fff",fontFamily:"Ubuntu-Regular"}}>
                            Credit Card Application was done on 26.04.2020.
                            Your Application Ref No #1655545
                        </Text>
                     </View>

                     <TouchableOpacity style={{height:height(15),width:width(65),alignItems:"center",justifyContent:"center"}} onPress={() => this.props.navigation.navigate("Refer")}>
                        <Text style={{fontSize:14,color:"#fff",fontFamily:"Ubuntu-Regular",marginBottom:height(2)}}>
                            Invite your freinds to Money Mall
                        </Text>

                        <Text style={{fontSize:26,color:"#fff",fontFamily:"Ubuntu-Regular"}}>
                           Refer & Earn
                        </Text>
                     </TouchableOpacity>
                    </LinearGradient>
                    <View  style={{width:width(85),height:height(30),alignItems:"center",justifyContent:"center"}}>
               
                    <View style={{width:width(85),height:height(15),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",borderRadius:18}}>

                        <View style={{width:width(85),height:height(5),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                        <Image source={require("../image/notify.png")} style={{width:width(6),height:height(6),margin:width(2)}} resizeMode="contain"/>
                        <Text style={{fontSize:18,color:"#747474",fontFamily:"Ubuntu-Regular"}}>
                            Notification Messages
                        </Text>

                        </View>
                        <View style={{width:width(85),height:height(10),alignItems:"center",justifyContent:"center"}}>
                        <Text style={{fontSize:16,color:"#747474",fontFamily:"Ubuntu-Regular"}}>
                           No Notification at the moment....
                        </Text>
                        </View>
                </View>
                    </View>
               
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height:height(95),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1972d7',
    },
});

//make this component available to the app
export default Account;
