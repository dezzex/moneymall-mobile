import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,Picker, CheckBox} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Slider from '@react-native-community/slider';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import  worldlist from './world.json';
import DateTimePicker from '@react-native-community/datetimepicker';

import { Header } from 'react-navigation-stack';

import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [
    {label: 'param1', value: 0 },
    {label: 'param2', value: 1 }
  ];
export default class Apply extends Component {

    static navigationOptions = {
        headerRight:
        <View style={{flexDirection:"row",width:width(20)}}>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        <Image source={require("../image/avatar.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
        </View>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        
        <Image style={{marginRight:width(3)}} source={require("../image/notification.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
       </View>
       </View>,
        
        headerLeft:<Image source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       ,
        headerStyle:{
          height:height(8),
        }
      };
  constructor(props) {
    super(props);
    this.state = {
        slideCompletionValue:1000,
        
        toCountry:'',  
        date: new Date("1990-01-01"),
        mode: 'date',
        male:true,
        female:false,
        rented:true,
        owned:false,
        companey:false,
        single:true,
        martial:false,
        year:"",
        salary:"",
        company:"",
        show:true,
      value: 0,
      isContinue:false

    };
  }
  onToCountryChanged(itemValue){
    this.setState({toCountry: itemValue})
    }
  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      date:date,
    });
  }
  show = mode => {
    this.setState({
      show: true,
      mode,
    });
  }

  datepicker = () => {
    this.show('date');
  }

  timepicker = () => {
    this.show('time');
  }

  onNextClicked(){
        this.props.navigation.navigate("ApplySecondLevel");
  }
  onDatePressed(){
    this.setState({show:!this.state.show})
    this.yearsinput.focus();
  }

  onMalePressed(){
      this.setState({female:false,male:true})
  }
  onFemalePressed(){
    this.setState({male:false,female:true})
}
onRentedPressed(){
    this.setState({rented:true,owned:false,companey:false})
}
onOwnedPressed(){
    this.setState({rented:false,owned:true,companey:false})
}
onCompanyPressed(){
    this.setState({rented:false,owned:false,companey:true})
}
onSingleClicked(){
    this.setState({single:true,martial:false})
}
onMartialClicked(){
  this.setState({single:false,martial:true})
}
    render() {
        
      var month = this.state.date.getMonth()+1;
        return (
          <KeyboardAwareScrollView
          keyboardVerticalOffset = {Header.HEIGHT + 20} // adjust the value here if you need more padding
          style = {{ flex: 1 }}
          behavior = "padding" >
            <View style={{flex:1,width:width(100),alignItems:"center"}}>
                <View style={{width:width(95),flexDirection:"row",height:height(12),marginTop:height(2),marginBottom:height(5),justifyContent:"center"}}>
                <View style={{width:width(15),alignItems:"center",marginRight:width(2)}}>
                <Image  source={require("../image/resume.png")} style={{width:width(12),height:height(10)}} resizeMode="contain"/>
                       
                </View>
                
                <View style={{width:width(80)}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",marginBottom:height(1)}}>About you</Text>
                <View style={{width:width(80)}}>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>Your personal details will stand you unique from others and Help</Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>us buld your profile.Its for the First time only.So,please fill the</Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>details below.</Text>
                </View>
                </View>
                </View>
                {this.state.isContinue !== true
                ?
                <View>
                <View style={{height:height(60),alignItems:"center"}}>
                <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>What's your gender ?</Text>
                    </View>
                    <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onMalePressed.bind(this)}> 
                          <CheckBox value={this.state.male} onChange={this.onMalePressed.bind(this)}/>
                          <Image  source={require("../image/male.png")} style={{width:width(5),height:height(3)}} resizeMode="contain"/>
                        <Text>Male</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onFemalePressed.bind(this)}> 
                          <CheckBox value={this.state.female} onChange={this.onFemalePressed.bind(this)}/>
                          <Image  source={require("../image/female.png")} style={{width:width(5),height:height(3)}} resizeMode="contain"/>
                        <Text>Female</Text>
                    </TouchableOpacity>
                    </View>

                </View>
                <View style={{height:height(15),width:width(90)}}>
                <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Date of Birth</Text>
                    </View> 
                    <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(1),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
             
             <TouchableOpacity
             onPress={this.onDatePressed.bind(this)} style={{width:width(80),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
            
             { this.state.show && <DateTimePicker value={this.state.date}
                 mode={this.state.mode}
                 is24Hour={true}
                 display="default"
                 
           
                 value={this.state.date}
                 onChange={this.setDate.bind(this)} />

     }
        <TextInput
        textAlign={"center"}
            style={{width:width(60),alignItems:"center",justifyContent:"center",color:"#000"}}
            value={this.state.date.getDate()+"/"+month+"/"+this.state.date.getFullYear()}
            
            ref={(input) => { this.dateofbirth = input; }}
            editable={false}
           //  placeholder={this.state.date.getDate()+"/"+month+"/"+this.state.date.getFullYear()}
            />
         
         </TouchableOpacity>
         <TouchableOpacity onPress={this.onDatePressed.bind(this)} style={{width:width(10),height:height(6),alignItems:"center",justifyContent:"center",backgroundColor:"#F6F6F6"}}>
            <Image source={require("../image/salary.png")} resizeMode="contain" style={{height:height(5),width:width(5)}}/>
            </TouchableOpacity>
         </View>
                {/* <TouchableOpacity  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}
                      onPress={this.onDatePressed.bind(this)}>
               
               { this.state.show && <DateTimePicker value={this.state.date}
                   mode={this.state.mode}
                   is24Hour={true}
                   display="default"
                   onChange={this.setDate.bind(this)} />

       }
          <TextInput
              style={{width:width(30),alignItems:"center",justifyContent:"center"}}
              value={this.state.date.getDate()+"/"+month+"/"+this.state.date.getFullYear()}
              placeholder={this.state.date.getDate()+"/"+month+"/"+this.state.date.getFullYear()}
              />
           
           </TouchableOpacity> */}
                </View>

               

                <View style={{height:height(15),width:width(90)}}>
                <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Years in UAE</Text>
                    </View> 
                <TouchableOpacity  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
               <TextInput placeholder="0 Years"  textAlign={"center"} value={this.state.year} onChangeText={(value) => this.setState({year:value})}
               keyboardType="number-pad"
               ref={(input) => { this.yearsinput = input; }}
               />
               
           
           </TouchableOpacity>
                </View>

                <View style={{height:height(15),width:width(90)}}>
                <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Salary</Text>
                    </View> 
                <TouchableOpacity  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
               <TextInput placeholder="5000 AED"  textAlign={"center"} value={this.state.salary} onChangeText={(value) => this.setState({salary:value})}
               keyboardType="number-pad"
               />
               
           
           </TouchableOpacity>
                </View>
                </View>
                <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={() => this.setState({isContinue:true})}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                        Continue
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
              </View>
              :
              <View>
                 <View style={{height:height(60),alignItems:"center"}}>
                   <View style={{height:height(15),width:width(90)}}>
                <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Nationality</Text>
                    </View> 
                <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                <Picker
                    style={{height:height(6),width:width(80),alignItems:"center"}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onToCountryChanged.bind(this)}>
                        {Object.keys(worldlist).map((key,idx)=>{
                            return(
                             <Picker.Item style={{marginLeft: width(2)}} label={"   "+worldlist[key].emoji+"  "+"      "+worldlist[key].name} value={worldlist[key]} key={idx} />
                            )
                        }
                        )}
                    </Picker>  
           
           </View>
                </View>
                 <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Residence Type</Text>
                    </View>
                    <View style={{width:width(90),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(27),height:height(8),marginLeft:width(4),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onRentedPressed.bind(this)}>
                          <CheckBox value={this.state.rented} onValueChange={this.onRentedPressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Rented</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(27),height:height(8),marginLeft:width(4),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onOwnedPressed.bind(this)}>
                          <CheckBox  value={this.state.owned} onValueChange={this.onOwnedPressed.bind(this)}/>
                          <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Owned</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(27),height:height(8),marginLeft:width(4),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onCompanyPressed.bind(this)}>
                          <CheckBox  value={this.state.companey} onValueChange={this.onCompanyPressed.bind(this)}/>
                          <View style={{width:width(18),height:height(8),alignItems:"center",justifyContent:"center"}}>
                          <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Company Provided</Text>
                          </View>
                    </TouchableOpacity>
                    </View>

                </View>

                <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Martial Status</Text>
                    </View>
                    <View style={{width:width(90),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(27),height:height(8),marginLeft:width(4),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onSingleClicked.bind(this)}>
                          <CheckBox value={this.state.single} onValueChange={this.onSingleClicked.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Single</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(27),height:height(8),marginLeft:width(4),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onMartialClicked.bind(this)}>
                          <CheckBox  value={this.state.martial} onValueChange={this.onMartialClicked.bind(this)}/>
                          <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Married</Text>
                    </TouchableOpacity>
                    </View>

                </View>
                
                <View style={{height:height(15),width:width(90)}}>
                <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Your Employer Company</Text>
                    </View> 
                <TouchableOpacity  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
               <TextInput placeholder="ADNOC"  textAlign={"center"} value={this.state.company} onChangeText={(value) => this.setState({company:value})}
               />
               
           
           </TouchableOpacity>
                </View>
                </View>
                <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                        Continue
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient> 
                </View>
              
                }

              
               
                {/* <View style={{flexDirection:"row",margin:height(3),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",borderColor:"#B8B8B8",elevation:4}}>
                <View style={{width:width(10),height:height(6),backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center"}}>
               <Image  source={require("../image/mailbg.png")} style={{width:width(5),height:height(5)}} resizeMode="contain"/>

               </View>
                <TouchableOpacity style={{width:width(80),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               <TextInput
               placeholder="Guest@outlook.com"
               style={{width:width(80),alignItems:"center",justifyContent:"center"}}
               />
            
            </TouchableOpacity>
            </View>

            <View style={{flexDirection:"row",margin:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",elevation:4}}>
                <View style={{width:width(10),height:height(6),backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center"}}>
               <Image  source={require("../image/contactbg.png")} style={{width:width(5),height:height(5)}} resizeMode="contain"/>

               </View>
                <TouchableOpacity style={{width:width(80),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               <TextInput
               placeholder="+971 50 486 1291"
               style={{width:width(80),alignItems:"center",justifyContent:"center"}}
               />
            
            </TouchableOpacity>
            </View>

            
            <View style={{flexDirection:"row",margin:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",elevation:4}}>
              <View style={{width:width(10),height:height(6),backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center"}}>
               <Image  source={require("../image/calendarbg.png")} style={{width:width(5),height:height(5)}} resizeMode="contain"/>

               </View>
                <TouchableOpacity style={{width:width(80),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               <TextInput
               placeholder="Monthly Salary"
               style={{width:width(80),alignItems:"center",justifyContent:"center"}}
               />
            
            </TouchableOpacity>
            </View>

            <View style={{width:width(80),height:height(3),justifyContent:"center",alignItems:"center"}}>
                <Slider
                    style={{width: width(60), height: height(3)}}
                    minimumValue={0}
                    maximumValue={10000}
                    value={1000}
                    step={100}
                    minimumTrackTintColor="#4A90E2"
                    maximumTrackTintColor="#000000"
                    thumbTintColor='#4A90E2'
                    onSlidingComplete={value =>
                      this.setState({
                        slideCompletionValue: value,
                        slideCompletionCount: this.state.slideCompletionCount + 1,
                      })
                    }
                  />
                  <Text>
                      {this.state.slideCompletionValue} AED
                  </Text>
                </View>

            <View style={{flexDirection:"row",width:width(95),height:height(5),alignItems:"center",justifyContent:"center",marginTop:height(2)}}>
            <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(25),height:height(5),margin:10,borderRadius:12}}
                 colors={["#0D9CF8","#0354F1"]} >
                    <Text style={{color:"#fff",fontStyle:"italic"}}>
                        AED 500
                    </Text>
                </LinearGradient>
                <TouchableOpacity style={{alignItems:"center",justifyContent:"center",width:width(25),borderWidth:1,borderColor:"#035AF1",height:height(6),margin:10,borderRadius:12}}
                >
                    <Text style={{fontStyle:"italic"}}>
                        AED 1000
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{alignItems:"center",justifyContent:"center",width:width(25),borderWidth:1,borderColor:"#035AF1",height:height(6),margin:10,borderRadius:12}}
                >
                    <Text style={{fontStyle:"italic"}}>
                        AED 1500
                    </Text>
                </TouchableOpacity>

            </View>
            <View style={{flexDirection:"row",width:width(95),height:height(5),alignItems:"center",justifyContent:"center",marginTop:height(2)}}>
            <TouchableOpacity style={{alignItems:"center",justifyContent:"center",borderWidth:1,borderColor:"#035AF1",width:width(25),height:height(5),margin:10,borderRadius:12}}
                >
                    <Text style={{fontStyle:"italic"}}>
                        AED 500
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{alignItems:"center",justifyContent:"center",width:width(25),borderWidth:1,borderColor:"#035AF1",height:height(5),margin:10,borderRadius:12}}
                >
                    <Text style={{fontStyle:"italic"}}>
                        AED 1000
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{alignItems:"center",justifyContent:"center",width:width(25),borderWidth:1,borderColor:"#035AF1",height:height(5),margin:10,borderRadius:12}}
                >
                    <Text style={{fontStyle:"italic"}}>
                        AED 1500
                    </Text>
                </TouchableOpacity>

            </View>

                   
            <View style={{flexDirection:"row",margin:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",elevation:4}}>
              <View style={{width:width(10),height:height(6),backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center"}}>
               <Image  source={require("../image/salary.png")} style={{width:width(5),height:height(5)}} resizeMode="contain"/>

               </View>
                <TouchableOpacity style={{width:width(80),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               <TextInput
               placeholder="Date of Birth"
               style={{width:width(80),alignItems:"center",justifyContent:"center"}}
               />
            
            </TouchableOpacity>
            </View>

            <View style={{flexDirection:"row",margin:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",elevation:4}}>
              <View style={{width:width(10),height:height(6),backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center"}}>
               <Image  source={require("../image/employer.png")} style={{width:width(5),height:height(5)}} resizeMode="contain"/>

               </View>
                <TouchableOpacity style={{width:width(80),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               <TextInput
               placeholder="Employer"
               style={{width:width(80),alignItems:"center",justifyContent:"center"}}
               />
            
            </TouchableOpacity>
            </View>

            <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        Next
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient> */}

            </View>
            </KeyboardAwareScrollView>
        )
    }
}
