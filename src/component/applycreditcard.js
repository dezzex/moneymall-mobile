import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,CheckBox,Picker} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

export default class ApplyCreditCard extends Component {

    static navigationOptions = {
        headerRight:
        <View style={{flexDirection:"row",width:width(20)}}>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        <Image source={require("../image/avatar.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
        </View>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        
        <Image style={{marginRight:width(3)}} source={require("../image/notification.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
       </View>
       </View>,
        
        headerLeft:<Image source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       ,
        headerStyle:{
          height:height(8),
        }
      };

      constructor(props){
        super(props)
        this.state={
            mon0:false,
            male:false,
            female:true,
            toCountry:'', 
            showDialog:false,
            balyes:false,
            balno:true,
            balyes2:false,
            balno2:true,
            moreloanyes:false,
            moreloanno:true,
            index:false
        }
    }
    onToCountryChanged(itemValue){
      this.setState({toCountry: itemValue})
      }
    onBalanceYesPressed(){
      this.setState({balyes:true,balno:false})
    }
    onBalanceNoPressed(){
      this.setState({balyes:false,balno:true})
    }  
    onBalanceYes2Pressed(){
      this.setState({balyes2:true,balno2:false})
    }
    onBalanceNo2Pressed(){
      this.setState({balyes2:false,balno2:true})
    }  
    onMoreLoanYes(){
      this.setState({moreloanyes:true,moreloanno:false})
    }
    onMoreLoanNo(){
      this.setState({moreloanyes:false,moreloanno:true})
    }
    onMalePressed(){
        this.setState({female:false,male:true,showDialog:true,index:true})
    }
    onFemalePressed(){
      this.setState({male:false,female:true,showDialog:false,index:false})
    }
    onNextClicked(){
        this.props.navigation.navigate("ApplySixthLevel");
    }

    render() {
        return (
            <View style={{flex:1,alignItems:"center"}}>
           

           <View style={{width:width(95),flexDirection:"row",height:height(12),marginTop:height(2),marginBottom:height(5),justifyContent:"center"}}>
                <View style={{width:width(15),alignItems:"center",marginRight:width(2)}}>
                <Image  source={require("../image/cardtitle.png")} style={{width:width(12),height:height(10)}} resizeMode="contain"/>
                       
                </View>
                
                <View style={{width:width(80)}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",marginBottom:height(1)}}>Credit cards you currently hold</Text>
                <View style={{width:width(80)}}>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>Your Existing Credit card details are required to check your  </Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>debt burden ratio.This information can help us to know the</Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>possibility of you getting approved.</Text>
                </View>
                </View>
                </View>
            
                <View style={{width:width(99),height:height(60),alignItems:"center",justifyContent:"center"}}>
        <ScrollView>
        <View style={{alignItems:"center"}}>
          
            <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Do you have any Existing Cards?</Text>
                    </View>
                    <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onMalePressed.bind(this)}> 
                          <CheckBox value={this.state.male} onValueChange={this.onMalePressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onFemalePressed.bind(this)}> 
                          <CheckBox value={this.state.female} onValueChange={this.onFemalePressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>No</Text>
                    </TouchableOpacity>
                    </View>

                </View>
              </View>
              {this.state.index
              ?
              <View>
            <View style={{width:width(90)}}>

            <View style={{height:height(15),width:width(90)}}>
              <View style={{height:height(3),marginLeft:width(1)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Bank</Text>
                    </View> 
                    <View style={{width:width(90),alignItems:"center",justifyContent:"center"}}>
                <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                <Picker
                    style={{height:height(6),width:width(90),alignItems:"center"}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onToCountryChanged.bind(this)}>
                        <Picker.Item style={{marginLeft: width(2)}} label={"Citi Bank"} value={"citibank"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"ADCB"} value={"adcb"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"FAB"} value={"fab"}  />
                           
                    </Picker> 
               </View>
               </View>
               </View>

               <View style={{height:height(15),width:width(90)}}>
              <View style={{height:height(3),marginLeft:width(1)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Card Type</Text>
                    </View> 
                    <View style={{width:width(90),alignItems:"center",justifyContent:"center"}}>
                <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                <Picker
                    style={{height:height(6),width:width(90),alignItems:"center"}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onToCountryChanged.bind(this)}>
                        <Picker.Item style={{marginLeft: width(2)}} label={"Personal Loan"} value={"personallaon"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"ADCB"} value={"adcb"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"FAB"} value={"fab"}  />
                           
                    </Picker> 
               </View>
               </View>
               </View>
               <View style={{height:height(15),width:width(90)}}>
               <View style={{height:height(3),marginLeft:width(1)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Installment</Text>
                    </View> 
                  <View style={{width:width(90),alignItems:"center"}}>
                <TouchableOpacity  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
               <TextInput placeholder="0 AED"  textAlign={"center"}/>
           </TouchableOpacity>
               </View>
               
           
                </View>
                <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Want to balance Transfer?</Text>
                    </View>
                    <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onBalanceYesPressed.bind(this)}> 
                          <CheckBox value={this.state.balyes} onChange={this.onBalanceYesPressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onBalanceNoPressed.bind(this)}> 
                          <CheckBox value={this.state.balno} onChange={this.onBalanceNoPressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>No</Text>
                    </TouchableOpacity>
                    </View>

                </View>
            </View>

            {this.state.moreloanyes
                ?
                <View style={{width:width(90)}}>

                <View style={{height:height(15),width:width(90)}}>
                  <View style={{height:height(3),marginLeft:width(1)}}>
                        <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Bank</Text>
                        </View> 
                        <View style={{width:width(90),alignItems:"center",justifyContent:"center"}}>
                    <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                   
                    <Picker
                        style={{height:height(6),width:width(90),alignItems:"center"}}
                        selectedValue={this.state.toCountry}
                        
                        onValueChange={this.onToCountryChanged.bind(this)}>
                            <Picker.Item style={{marginLeft: width(2)}} label={"Citi Bank"} value={"citibank"}  />
                            <Picker.Item style={{marginLeft: width(2)}} label={"ADCB"} value={"adcb"}  />
                            <Picker.Item style={{marginLeft: width(2)}} label={"FAB"} value={"fab"}  />
                               
                        </Picker> 
                   </View>
                   </View>
                   </View>
    
                   <View style={{height:height(15),width:width(90)}}>
                  <View style={{height:height(3),marginLeft:width(1)}}>
                        <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Card Type</Text>
                        </View> 
                        <View style={{width:width(90),alignItems:"center",justifyContent:"center"}}>
                    <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                   
                    <Picker
                        style={{height:height(6),width:width(90),alignItems:"center"}}
                        selectedValue={this.state.toCountry}
                        
                        onValueChange={this.onToCountryChanged.bind(this)}>
                            <Picker.Item style={{marginLeft: width(2)}} label={"Personal Loan"} value={"personallaon"}  />
                            <Picker.Item style={{marginLeft: width(2)}} label={"ADCB"} value={"adcb"}  />
                            <Picker.Item style={{marginLeft: width(2)}} label={"FAB"} value={"fab"}  />
                               
                        </Picker> 
                   </View>
                   </View>
                   </View>
                   <View style={{height:height(15),width:width(90)}}>
                   <View style={{height:height(3),marginLeft:width(1)}}>
                        <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Installment</Text>
                        </View> 
                      <View style={{width:width(90),alignItems:"center"}}>
                    <TouchableOpacity  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                   
                   <TextInput placeholder="0 AED"  textAlign={"center"}/>
               </TouchableOpacity>
                   </View>
                   
               
                    </View>
                    <View style={{height:height(15),width:width(90)}}>
                        <View style={{height:height(4)}}>
                        <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Want to balance Transfer?</Text>
                        </View>
                        <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                          <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                            borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onBalanceYes2Pressed.bind(this)}> 
                              <CheckBox value={this.state.balyes2} onChange={this.onBalanceYes2Pressed.bind(this)}/>
                             <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Yes</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                            borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onBalanceNo2Pressed.bind(this)}> 
                              <CheckBox value={this.state.balno2} onChange={this.onBalanceNo2Pressed.bind(this)}/>
                             <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>No</Text>
                        </TouchableOpacity>
                        </View>
    
                    </View>
                </View>
                
                :
                <View/>}
            
                <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Do you have any More Cards?</Text>
                    </View>
                    <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onMoreLoanYes.bind(this)}> 
                          <CheckBox value={this.state.moreloanyes} onValueChange={this.onMoreLoanYes.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onMoreLoanNo.bind(this)}> 
                          <CheckBox value={this.state.moreloanno} onValueChange={this.onMoreLoanNo.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>No</Text>
                    </TouchableOpacity>
                    </View>

                </View>
              
                
          </View>
          :
          <View/>
              }
              </ScrollView>
            </View>
            <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}}  onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                        Continue
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
               </View>
          
        )
    }
}
