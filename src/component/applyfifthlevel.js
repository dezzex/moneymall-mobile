import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,Modal,CheckBox,Picker,PickerIOS} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

export default class ApplyFifthLevel extends Component {
    static navigationOptions = {
        headerRight:
        <View style={{flexDirection:"row",width:width(20)}}>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        <Image source={require("../image/avatar.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
        </View>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        
        <Image style={{marginRight:width(3)}} source={require("../image/notification.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
       </View>
       </View>,
        
        headerLeft:<Image source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       ,
        headerStyle:{
          height:height(8),
        }
      };

      constructor(props){
        super(props)
        this.state={
            mon0:false,
            male:false,
            female:true,
            toCountry:'', 
            showDialog:false,
            balyes:false,
            balno:true,
            balyes2:false,
            balno2:true,
            moreloanyes:false,
            moreloanno:true,
            index:false
        }
    }
    onBalanceYesPressed(){
      this.setState({balyes:true,balno:false})
    }
    onBalanceNoPressed(){
      this.setState({balyes:false,balno:true})
    }  
    onBalanceYes2Pressed(){
      this.setState({balyes2:true,balno2:false})
    }
    onBalanceNo2Pressed(){
      this.setState({balyes2:false,balno2:true})
    }  
    onMoreLoanYes(){
      this.setState({moreloanyes:true,moreloanno:false})
    }
    onMoreLoanNo(){
      this.setState({moreloanyes:false,moreloanno:true})
    }
    onMalePressed(){
        this.setState({female:false,male:true,showDialog:true,index:true})
    }
    onFemalePressed(){
      this.setState({male:false,female:true,showDialog:false,index:false})
    }
    onNextClicked(){
        console.log('====================================');
        console.log("dddddddddd");
        console.log('====================================');
        this.props.navigation.navigate("ApplyCreditCard")
    }
    onMonPressed(index){
        console.log('====================================');
        console.log(index);
        console.log('====================================');
      if(index === 0)
          this.setState({mon0:!this.state.mon0})
    }
    onToCountryChanged(itemValue){
        this.setState({toCountry: itemValue})
        if(this.state.fromCountry !== ""){
            
          this.setState({result:rate * parseInt(this.state.amount)})
          }
        }
    render() {
        return (
            <View style={{flex:1,alignItems:"center"}}>
           
           <View style={{width:width(95),flexDirection:"row",height:height(12),marginTop:height(2),marginBottom:height(5),justifyContent:"center"}}>
                <View style={{width:width(15),alignItems:"center",marginRight:width(2)}}>
                <Image  source={require("../image/loanicon.png")} style={{width:width(12),height:height(10)}} resizeMode="contain"/>
                       
                </View>
                
                <View style={{width:width(80)}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",marginBottom:height(1)}}>Loans you already Have</Text>
                <View style={{width:width(80)}}>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>Your previous loan repayment history can improve your chances</Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>for approval and getting better deals.Its also very important to</Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>calculate your eligible amount.</Text>
                </View>
                </View>
                </View>


            {/* <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.showDialog}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}> */}
        <View style={{width:width(99),height:height(60),alignItems:"center",justifyContent:"center"}}>
        <ScrollView>
        <View style={{alignItems:"center"}}>
          
            <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Do you have any Existing Loans?</Text>
                    </View>
                    <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onMalePressed.bind(this)}> 
                          <CheckBox value={this.state.male} onValueChange={this.onMalePressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onFemalePressed.bind(this)}> 
                          <CheckBox value={this.state.female} onValueChange={this.onFemalePressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>No</Text>
                    </TouchableOpacity>
                    </View>

                </View>
              </View>
              {this.state.index
              ?
              <View>
            <View style={{width:width(90)}}>

            <View style={{height:height(15),width:width(90)}}>
              <View style={{height:height(3),marginLeft:width(1)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Bank</Text>
                    </View> 
                    <View style={{width:width(90),alignItems:"center",justifyContent:"center"}}>
                <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                <Picker
                    style={{height:height(6),width:width(90),alignItems:"center"}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onToCountryChanged.bind(this)}>
                        <Picker.Item style={{marginLeft: width(2)}} label={"Citi Bank"} value={"citibank"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"ADCB"} value={"adcb"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"FAB"} value={"fab"}  />
                           
                    </Picker> 
               </View>
               </View>
               </View>

               <View style={{height:height(15),width:width(90)}}>
              <View style={{height:height(3),marginLeft:width(1)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Loan Type</Text>
                    </View> 
                    <View style={{width:width(90),alignItems:"center",justifyContent:"center"}}>
                <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                <Picker
                    style={{height:height(6),width:width(90),alignItems:"center"}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onToCountryChanged.bind(this)}>
                        <Picker.Item style={{marginLeft: width(2)}} label={"Personal Loan"} value={"personallaon"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"ADCB"} value={"adcb"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"FAB"} value={"fab"}  />
                           
                    </Picker> 
               </View>
               </View>
               </View>
               <View style={{height:height(15),width:width(90)}}>
               <View style={{height:height(3),marginLeft:width(1)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Installment</Text>
                    </View> 
                  <View style={{width:width(90),alignItems:"center"}}>
                <TouchableOpacity  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
               <TextInput placeholder="0 AED"  textAlign={"center"}/>
           </TouchableOpacity>
               </View>
               
           
                </View>
                <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Want to balance Transfer?</Text>
                    </View>
                    <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onBalanceYesPressed.bind(this)}> 
                          <CheckBox value={this.state.balyes} onChange={this.onBalanceYesPressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onBalanceNoPressed.bind(this)}> 
                          <CheckBox value={this.state.balno} onChange={this.onBalanceNoPressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>No</Text>
                    </TouchableOpacity>
                    </View>

                </View>
            </View>
            
               
                {this.state.moreloanyes
                ?
                <View style={{width:width(90)}}>

                <View style={{height:height(15),width:width(90)}}>
                  <View style={{height:height(3),marginLeft:width(1)}}>
                        <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Bank</Text>
                        </View> 
                        <View style={{width:width(90),alignItems:"center",justifyContent:"center"}}>
                    <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                   
                    <Picker
                        style={{height:height(6),width:width(90),alignItems:"center"}}
                        selectedValue={this.state.toCountry}
                        
                        onValueChange={this.onToCountryChanged.bind(this)}>
                            <Picker.Item style={{marginLeft: width(2)}} label={"Citi Bank"} value={"citibank"}  />
                            <Picker.Item style={{marginLeft: width(2)}} label={"ADCB"} value={"adcb"}  />
                            <Picker.Item style={{marginLeft: width(2)}} label={"FAB"} value={"fab"}  />
                               
                        </Picker> 
                   </View>
                   </View>
                   </View>
    
                   <View style={{height:height(15),width:width(90)}}>
                  <View style={{height:height(3),marginLeft:width(1)}}>
                        <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Loan Type</Text>
                        </View> 
                        <View style={{width:width(90),alignItems:"center",justifyContent:"center"}}>
                    <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                   
                    <Picker
                        style={{height:height(6),width:width(90),alignItems:"center"}}
                        selectedValue={this.state.toCountry}
                        
                        onValueChange={this.onToCountryChanged.bind(this)}>
                            <Picker.Item style={{marginLeft: width(2)}} label={"Personal Loan"} value={"personallaon"}  />
                            <Picker.Item style={{marginLeft: width(2)}} label={"ADCB"} value={"adcb"}  />
                            <Picker.Item style={{marginLeft: width(2)}} label={"FAB"} value={"fab"}  />
                               
                        </Picker> 
                   </View>
                   </View>
                   </View>
                   <View style={{height:height(15),width:width(90)}}>
                   <View style={{height:height(3),marginLeft:width(1)}}>
                        <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Installment</Text>
                        </View> 
                      <View style={{width:width(90),alignItems:"center"}}>
                    <TouchableOpacity  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                   
                   <TextInput placeholder="0 AED"  textAlign={"center"}/>
               </TouchableOpacity>
                   </View>
                   
               
                    </View>
                    <View style={{height:height(15),width:width(90)}}>
                        <View style={{height:height(4)}}>
                        <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Want to balance Transfer?</Text>
                        </View>
                        <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                          <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                            borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onBalanceYes2Pressed.bind(this)}> 
                              <CheckBox value={this.state.balyes2} onChange={this.onBalanceYes2Pressed.bind(this)}/>
                             <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Yes</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                            borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onBalanceNo2Pressed.bind(this)}> 
                              <CheckBox value={this.state.balno2} onChange={this.onBalanceNo2Pressed.bind(this)}/>
                             <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>No</Text>
                        </TouchableOpacity>
                        </View>
    
                    </View>
                </View>
                
                :
                <View/>}

<View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Do you have any More Loans?</Text>
                    </View>
                    <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onMoreLoanYes.bind(this)}> 
                          <CheckBox value={this.state.moreloanyes} onValueChange={this.onMoreLoanYes.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onMoreLoanNo.bind(this)}> 
                          <CheckBox value={this.state.moreloanno} onValueChange={this.onMoreLoanNo.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>No</Text>
                    </TouchableOpacity>
                    </View>

                </View>
                
          </View>
          :
          <View/>
              }
              </ScrollView>
            </View>
            <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}}  onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                        Continue
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
              
            </View>
            
            
            //    <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
            //     <View style={{width:width(90),height:height(7)}}>
            //     <Text style={{fontSize:22,fontWeight:"200"}}>Existing Liabilities</Text>
            //     </View>

            //     <View style={{height:height(67)}}>
            //     {this.state.mon0
            //     ?
            //     <View style={{height:height(67)}}>
            //     <View style={{width:width(80),flexDirection:"row",elevation:8, shadowOpacity: 0.75,
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#E6E6E6",
            //     borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}>
            //         <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(40),height:height(6),borderRadius:6}}
            //  colors={["#0D9CF8","#0354F1"]} onPress={this.onMonPressed.bind(this,0)}>
            //      <Text style={{color:"#fff",fontStyle:"italic"}}>
            //         Loans 
            //      </Text>
            //     </LinearGradient>
            //     <TouchableOpacity style={{width:width(40),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,0)}>
            //         <Text style={{fontStyle:"italic"}}>
            //             Credit Cards
            //         </Text>
            //     </TouchableOpacity>
            
            // </View>
            // <View style={{marginTop:height(3),width:width(85)}}>
            // <Text>
            //     Bank Name
            // </Text>
            // <TextInput
            //     placeholder="HSBC Bank Ltd"
            //     fontStyle={"italic"}
            //     style={{width:width(80),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,height:height(6),
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#fff",borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}
            //     />
            // </View>
            // <View style={{marginTop:height(3),width:width(85)}}>
            // <Text>
            //     Loan  Type
            // </Text>
            // <Picker
            //     placeholder="HSBC Bank Ltd"
            //     fontStyle={"italic"}
            //     style={{width:width(80),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,height:height(6),
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#fff",borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}
            //     >
            //         <Picker.Item  style={{width:width(80),alignItems:"center",justifyContent: 'center'}} label={"Home Loan"} value={"Home Loan"}/>
            //         <Picker.Item  style={{width:width(80),alignItems:"center",justifyContent: 'center'}} label={"Car Loan"} value={"Car Loan"}/>
            //         <Picker.Item  style={{width:width(80),alignItems:"center",justifyContent: 'center'}} label={"Personal Loan"} value={"Personal Loan"}/>
            //     </Picker>

            // </View>
            // <View style={{marginTop:height(3),width:width(85)}}>
            // <Text>
            //     EMI
            // </Text>
            // <TextInput
            //     placeholder="AED 350"
            //     fontStyle={"italic"}
            //     style={{width:width(80),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,height:height(6),
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#fff",borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}
            //     />
            // </View>
            // <View style={{marginTop:height(3),width:width(85)}}>
            // <Text>
            //     Limit
            // </Text>
            // <TextInput
            //     placeholder="AED 700000"
            //     fontStyle={"italic"}
            //     style={{width:width(80),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,height:height(6),
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#fff",borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}
            //     />
            // </View>
            // <View style={{marginTop:height(3),width:width(85)}}>
            // <Text>
            //         Tenure
            // </Text>
            // <TextInput
            //     placeholder="36 Months"
            //     fontStyle={"italic"}
            //     style={{width:width(80),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,height:height(6),
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#fff",borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}
            //     />
            // </View>
            
            // </View>
            //     :
                
            //     <View style={{width:width(85),alignItems:"center",justifyContent:"center"}}>
            //     <View style={{width:width(85),flexDirection:"row",elevation:8, shadowOpacity: 0.75,
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#E6E6E6",
            //     borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}>
                 
            //     <TouchableOpacity style={{width:width(40),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,0)}>
            //         <Text style={{fontStyle:"italic"}}>
            //         Loans
            //         </Text>
            //     </TouchableOpacity>
            //     <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(45),height:height(6),borderRadius:6}}
            //  colors={["#0D9CF8","#0354F1"]} onPress={this.onMonPressed.bind(this,0)}>
            //      <Text style={{color:"#fff",fontStyle:"italic"}}>
            //            Credit Cards
            //      </Text>
            //     </LinearGradient>
            
            // </View>
            //     <View style={{height:height(67)}}>
            //     <View style={{marginTop:height(3),width:width(85)}}>
            // <Text>
            //         Bank Name
            // </Text>
            // <TextInput
            //     placeholder="HSBC Bank Ltd"
            //     fontStyle={"italic"}
            //     style={{width:width(80),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,height:height(8),
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#fff",borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}
            //     />
            //     </View>
            //     <View style={{marginTop:height(5),width:width(85)}}>
            // <Text>
            //         Limit
            // </Text>
            // <TextInput
            //     placeholder="AED 7000"
            //     fontStyle={"italic"}
            //     style={{width:width(80),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,height:height(8),
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#fff",borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}
            //     />
            //     </View>
            //     <View style={{marginTop:height(5),width:width(85)}}>
            // <Text>
            //         Outstanding
            // </Text>
            // <TextInput
            //     placeholder="AED 1500"
            //     fontStyle={"italic"}
            //     style={{width:width(80),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,height:height(10),
            //     shadowRadius: 5,
            //     shadowColor: 'red',
            //     borderColor:"#B8B8B8",
            //     alignItems:"center",
            //     justifyContent:"center",
            //     backgroundColor:"#fff",borderRadius:6,
            //     shadowOffset: { height: 0, width: 0 }}}
            //     />
            //     </View>
            //     <View  style={{width:width(60),height:height(15),marginLeft:width(10),alignItems:"center",justifyContent:"center"}}>
            //     <Text style={{fontStyle:"italic",fontSize:18,height:height(8)}}>
            //     Do you want to transfer this card
            //     balance to another bank ?
            //     </Text>
            //     </View>
            //     </View>
            //     </View>
            //     }
            //     </View>
              
            //     <View style={{width:width(80),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
            // <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(30),height:height(6)}}
            //      colors={["#0D9CF8","#0354F1"]} >
            //        <TouchableOpacity style={{width:width(30),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
            //             <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
            //             Buyout
            //           </Text>
            //        </TouchableOpacity>
              
            //   </LinearGradient>

            //   <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(30),height:height(6)}}
            //      colors={["#0D9CF8","#0354F1"]} >
            //        <TouchableOpacity style={{width:width(30),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
            //             <Text style={{color:"#fff",fontSize:18}}>
            //             New
            //           </Text>
            //        </TouchableOpacity>
              
            //   </LinearGradient>

            // </View>
            // <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6),marginTop:10}}
            //      colors={["#0D9CF8","#0354F1"]} >
            //        <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
            //             <Text style={{color:"#fff",fontSize:18}}>
            //             Compare & Apply
            //           </Text>
            //        </TouchableOpacity>
              
            //   </LinearGradient>
            //     </View>
        )
    }
}
