import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,CheckBox} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

export default class ApplyFourthLevel extends Component {
    static navigationOptions = {
        headerRight:
        <View style={{flexDirection:"row",width:width(20)}}>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        <Image source={require("../image/avatar.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
        </View>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        
        <Image style={{marginRight:width(3)}} source={require("../image/notification.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
       </View>
       </View>,
        
        headerLeft:<Image source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       ,
        headerStyle:{
          height:height(8),
        }
      };

      constructor(props){
          super(props)
          this.state={
              mon0:false,
              mon1:true,
              mon2:true,
              mon3:true,
              mon4:true,
              mon5:false,
          }
      }

      onMonPressed(index){
          console.log('====================================');
          console.log(index);
          console.log('====================================');
        if(index === 0)
            this.setState({mon0:!this.state.mon0})
        else if(index === 1)
        this.setState({mon1:!this.state.mon1})
        else if(index === 2)
        this.setState({mon2:!this.state.mon2}) 
        else if(index === 3)
        this.setState({mon3:!this.state.mon3}) 
        else if(index === 4)
        this.setState({mon4:!this.state.mon4}) 
        else if(index === 5)
        this.setState({mon5:!this.state.mon5}) 
      }

      onNextClicked(){
            this.props.navigation.navigate("ApplyFifthLevel");
      }
       
    render() {
        console.log('====================================');
        console.log(this.state.mon2);
        console.log('====================================');
        return (
            <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(90),margin:15}}>
                <Text style={{fontSize:22,fontFamily:"Ubuntu-Bold"}}>Additional variable Income</Text>
                </View>
                <View style={{width:width(95),height:height(6),margin:20,alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                
                <TextInput
                placeholder="Over Time"
                fontStyle={"italic"}
                style={{width:width(60),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                fontFamily:"Ubuntu-Italic",
                backgroundColor:"#fff",borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}
                />
                {this.state.mon0
                ?
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",
                marginLeft:width(3),
                borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}>
                    <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}
             colors={["#0D9CF8","#0354F1"]} onPress={this.onMonPressed.bind(this,0)}>
                 <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                        Year
                 </Text>
                </LinearGradient>
                <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,0)}>
                    <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                        Month
                    </Text>
                </TouchableOpacity>
            
            </View>
                :
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                    shadowRadius: 5,
                    shadowColor: 'red',
                    borderColor:"#B8B8B8",
                    alignItems:"center",
                    justifyContent:"center",
                    backgroundColor:"#fff",
                    marginLeft:width(3),
                    borderRadius:6,
                    shadowOffset: { height: 0, width: 0 }}}>
                    
                    <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,0)}>
                        <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                            Year
                        </Text>
                    </TouchableOpacity>
                    <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}  onPress={this.onMonPressed.bind(this,0)}
                 colors={["#0D9CF8","#0354F1"]}>
                     <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                         Month
                     </Text>
                    </LinearGradient>
                </View>
                }


                </View>
                <View style={{width:width(95),height:height(6),margin:20,alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                
                <TextInput
                placeholder="Commision"
                fontStyle={"italic"}
                style={{width:width(60),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                fontFamily:"Ubuntu-Italic",
                backgroundColor:"#fff",borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}
                />
                {this.state.mon5
                ?
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",
                marginLeft:width(3),
                borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}>
                    <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}
             colors={["#0D9CF8","#0354F1"]} onPress={this.onMonPressed.bind(this,5)}>
                 <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                        Year
                 </Text>
                </LinearGradient>
                <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,5)}>
                    <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                        Month
                    </Text>
                </TouchableOpacity>
            
            </View>
                :
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                    shadowRadius: 5,
                    shadowColor: 'red',
                    borderColor:"#B8B8B8",
                    alignItems:"center",
                    justifyContent:"center",
                    backgroundColor:"#fff",
                    marginLeft:width(3),
                    borderRadius:6,
                    shadowOffset: { height: 0, width: 0 }}}>
                    
                    <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,5)}>
                        <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                            Year
                        </Text>
                    </TouchableOpacity>
                    <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}  onPress={this.onMonPressed.bind(this,5)}
                 colors={["#0D9CF8","#0354F1"]}>
                     <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                         Month
                     </Text>
                    </LinearGradient>
                </View>
                }


                </View>
                <View style={{width:width(95),height:height(6),margin:20,alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                
                <TextInput
                placeholder="Bonus"
                fontStyle={"italic"}
                style={{width:width(60),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}
                />
                {this.state.mon1
                ?
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",
                marginLeft:width(3),
                borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}>
                     <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}
             colors={["#0D9CF8","#0354F1"]} onPress={this.onMonPressed.bind(this,1)}>
                 <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                        Year
                 </Text>
                </LinearGradient>
                <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,1)}>
                    <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                        Month
                    </Text>
                </TouchableOpacity>
            
            </View>
                :
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                    shadowRadius: 5,
                    shadowColor: 'red',
                    borderColor:"#B8B8B8",
                    alignItems:"center",
                    justifyContent:"center",
                    backgroundColor:"#fff",
                    marginLeft:width(3),
                    borderRadius:6,
                    shadowOffset: { height: 0, width: 0 }}}>
                    
                    <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,1)}>
                        <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                            Year
                        </Text>
                    </TouchableOpacity>
                    <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}  onPress={this.onMonPressed.bind(this,1)}
                 colors={["#0D9CF8","#0354F1"]}>
                     <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                         Month
                     </Text>
                    </LinearGradient>
                </View>
                }


                </View>
                <View style={{width:width(95),height:height(6),margin:20,alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                
                <TextInput
                placeholder="Rent Received"
                fontStyle={"italic"}
                style={{width:width(60),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}
                />
                {this.state.mon2
                ?
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",
                marginLeft:width(3),
                borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}>
                         <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}
             colors={["#0D9CF8","#0354F1"]} onPress={this.onMonPressed.bind(this,2)}>
                 <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                        Year
                 </Text>
                </LinearGradient>
                <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,2)}>
                    <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                        Month
                    </Text>
                </TouchableOpacity>
            
            </View>
                :
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                    shadowRadius: 5,
                    shadowColor: 'red',
                    borderColor:"#B8B8B8",
                    alignItems:"center",
                    justifyContent:"center",
                    backgroundColor:"#fff",
                    marginLeft:width(3),
                    borderRadius:6,
                    shadowOffset: { height: 0, width: 0 }}}>
                    
                    <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,2)}>
                        <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                            Year
                        </Text>
                    </TouchableOpacity>
                    <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}  onPress={this.onMonPressed.bind(this,2)}
                 colors={["#0D9CF8","#0354F1"]}>
                     <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                         Month
                     </Text>
                    </LinearGradient>
                </View>
                }


                </View>
                <View style={{width:width(95),height:height(6),margin:20,alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                
                <TextInput
                placeholder="Education Allowances"
                fontStyle={"italic"}
                style={{width:width(60),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}
                />
                {this.state.mon3
                ?
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",
                marginLeft:width(3),
                borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}>
                              <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}
             colors={["#0D9CF8","#0354F1"]} onPress={this.onMonPressed.bind(this,2)}>
                 <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                        Year
                 </Text>
                </LinearGradient>
                <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,3)}>
                    <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                        Month
                    </Text>
                </TouchableOpacity>
            
            </View>
                :
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                    shadowRadius: 5,
                    shadowColor: 'red',
                    borderColor:"#B8B8B8",
                    alignItems:"center",
                    justifyContent:"center",
                    backgroundColor:"#fff",
                    marginLeft:width(3),
                    borderRadius:6,
                    shadowOffset: { height: 0, width: 0 }}}>
                    
                    <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,3)}>
                        <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                            Year
                        </Text>
                    </TouchableOpacity>
                    <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}  onPress={this.onMonPressed.bind(this,3)}
                 colors={["#0D9CF8","#0354F1"]}>
                     <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                         Month
                     </Text>
                    </LinearGradient>
                </View>
                }


                </View>
                <View style={{width:width(95),height:height(6),margin:20,alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                
                <TextInput
                placeholder="Others"
                fontStyle={"italic"}
                style={{width:width(60),alignItems:"center",justifyContent:"center",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}
                />
                {this.state.mon4
                ?
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                borderColor:"#B8B8B8",
                alignItems:"center",
                justifyContent:"center",
                backgroundColor:"#fff",
                marginLeft:width(3),
                borderRadius:6,
                shadowOffset: { height: 0, width: 0 }}}>
                      <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}
             colors={["#0D9CF8","#0354F1"]} onPress={this.onMonPressed.bind(this,4)}>
                 <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                        Year
                 </Text>
                </LinearGradient>
                <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,4)}>
                    <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                        Month
                    </Text>
                </TouchableOpacity>
            
            </View>
                :
                <View style={{width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row",elevation:8, shadowOpacity: 0.75,
                    shadowRadius: 5,
                    shadowColor: 'red',
                    borderColor:"#B8B8B8",
                    alignItems:"center",
                    justifyContent:"center",
                    backgroundColor:"#fff",
                    marginLeft:width(3),
                    borderRadius:6,
                    shadowOffset: { height: 0, width: 0 }}}>
                    
                     
                    <TouchableOpacity style={{width:width(15),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:6}}  onPress={this.onMonPressed.bind(this,4)}>
                        <Text style={{fontStyle:"italic",fontFamily:"Ubuntu-Italic"}}>
                            Year
                        </Text>
                    </TouchableOpacity>
                    <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(15),height:height(6),borderRadius:6}}  onPress={this.onMonPressed.bind(this,4)}
                 colors={["#0D9CF8","#0354F1"]}>
                     <Text style={{fontFamily:"Ubuntu-Italic",color:"#fff"}}>
                         Month
                     </Text>
                    </LinearGradient>
                </View>
                }


                </View>
                
                <View style={{flexDirection:"row",width:width(95),alignItems:"center",justifyContent:"space-evenly"}}>
                <TouchableOpacity style={{alignItems:"center",justifyContent:"center",width:width(25),height:height(6),backgroundColor:"#B8B8B8"}}
                  >
                   <TouchableOpacity style={{width:width(25),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Medium"}}>
                        Skip
                      </Text>
                   </TouchableOpacity>
              
              </TouchableOpacity>

                <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(65),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(65),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                        Continue
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
              </View>
            </View>
        )
    }
}
