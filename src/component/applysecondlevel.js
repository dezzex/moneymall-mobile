import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,Picker,CheckBox} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DateTimePicker from '@react-native-community/datetimepicker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default class ApplySecondLevel extends Component {
    static navigationOptions = {
        headerRight:
        <View style={{flexDirection:"row",width:width(20)}}>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        <Image source={require("../image/avatar.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
        </View>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        
        <Image style={{marginRight:width(3)}} source={require("../image/notification.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
       </View>
       </View>,
        
        headerLeft:<Image source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       ,
        headerStyle:{
          height:height(8),
        }
      };
      constructor(props) {
        super(props);
        this.state = {
            toCountry:'', 
            date: new Date(),
            mode: 'date', 
            male:false,
            female:true,
        }
    } 
    setDate = (event, date) => {
        date = date || this.state.date;
    
        this.setState({
          show: Platform.OS === 'ios' ? true : false,
          date:date,
        });
      }
      show = mode => {
        this.setState({
          show: true,
          mode,
        });
      }
    
      datepicker = () => {
        this.show('date');
      }
    
      timepicker = () => {
        this.show('time');
      }
    
      onDatePressed(){
        this.setState({show:!this.state.show})
      }
    onToCountryChanged(itemValue){
        this.setState({toCountry: itemValue})
        if(this.state.fromCountry !== ""){
            
          this.setState({result:rate * parseInt(this.state.amount)})
          }
        }
      onNextClicked(){
          if(this.state.male){
          this.props.navigation.navigate("ApplyFourthLevel");
          }
          else{
            this.props.navigation.navigate("ApplyFifthLevel");
          }
      }
      
  onMalePressed(){
    this.setState({female:false,male:true})
}
onFemalePressed(){
  this.setState({male:false,female:true})
}
    render() {
        
      var month = this.state.date.getMonth()+1;
        return (
          <KeyboardAwareScrollView>
            <View style={{flex:1,alignItems:"center"}}>
  <View style={{width:width(95),flexDirection:"row",height:height(12),marginTop:height(2),marginBottom:height(5),justifyContent:"center"}}>
                <View style={{width:width(15),alignItems:"center",marginRight:width(2)}}>
                <Image  source={require("../image/salryicon.png")} style={{width:width(12),height:height(10)}} resizeMode="contain"/>
                       
                </View>
                
                <View style={{width:width(80)}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",marginBottom:height(1)}}>Your Work</Text>
                <View style={{width:width(80)}}>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>We ask for Professional details like employment status and</Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>income details.We use this information to match suitable and</Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>eligible procuts to Save your time.</Text>
                </View>
                </View>
                </View>
            
            <View style={{height:height(60),alignItems:"center"}}>
            <View style={{height:height(15),width:width(90)}}>
                <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Your salary account is with</Text>
                    </View> 
                <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                <Picker
                    style={{height:height(6),width:width(90),alignItems:"center"}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onToCountryChanged.bind(this)}>
                        <Picker.Item style={{marginLeft: width(2)}} label={"Citi Bank"} value={"citibank"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"ADCB"} value={"adcb"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"FAB"} value={"fab"}  />
                           
                    </Picker>  
           
           </View>
                </View>
                <View style={{height:height(15),width:width(90)}}>
                <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Your Designation is</Text>
                    </View> 
                <TouchableOpacity  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
               <TextInput placeholder="Software Developer"  textAlign={"center"}/>
               </TouchableOpacity>
               </View>
               <View style={{height:height(15),width:width(90)}}>
                <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>You joined your current employer on</Text>
                    </View> 
                    <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(1),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
             
             <TouchableOpacity
             onPress={this.onDatePressed.bind(this)} style={{width:width(80),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
            
             { this.state.show && <DateTimePicker value={this.state.date}
                 mode={this.state.mode}
                 is24Hour={true}
                 display="default"
                 
           
                 value={this.state.date}
                 onChange={this.setDate.bind(this)} />

     }
        <TextInput
        textAlign={"center"}
            style={{width:width(60),alignItems:"center",justifyContent:"center",color:"#000"}}
            value={this.state.date.getDate()+"/"+month+"/"+this.state.date.getFullYear()}
            
            ref={(input) => { this.dateofbirth = input; }}
            editable={false}
           //  placeholder={this.state.date.getDate()+"/"+month+"/"+this.state.date.getFullYear()}
            />
         
         </TouchableOpacity>
         <TouchableOpacity onPress={this.onDatePressed.bind(this)} style={{width:width(10),height:height(6),alignItems:"center",justifyContent:"center",backgroundColor:"#F6F6F6"}}>
            <Image source={require("../image/salary.png")} resizeMode="contain" style={{height:height(5),width:width(5)}}/>
            </TouchableOpacity>
         </View>
                
                </View>


                <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Do you have any additional Income ?</Text>
                    </View>
                    <View style={{width:width(70),marginLeft:width(10),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onMalePressed.bind(this)}> 
                          <CheckBox value={this.state.male} onChange={this.onMalePressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 1,borderRadius:5,borderColor:"silver"}} onPress={this.onFemalePressed.bind(this)}> 
                          <CheckBox value={this.state.female} onChange={this.onMalePressed.bind(this)}/>
                         <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>No</Text>
                    </TouchableOpacity>
                    </View>

                </View>
              </View>
                <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                        Continue
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>

            </View>


            </KeyboardAwareScrollView>
                

        )
    }
}
