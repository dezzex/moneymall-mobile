import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,Modal,CheckBox,Picker,Linking} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

export default class ApplySixthLevel extends Component {
    static navigationOptions = {
        headerRight:
        <View style={{flexDirection:"row",width:width(20)}}>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        <Image source={require("../image/avatar.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
        </View>
        <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
        
        <Image style={{marginRight:width(3)}} source={require("../image/notification.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
       </View>
       </View>,
        
        headerLeft:<Image source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       ,
        headerStyle:{
          height:height(8),
        }
      };

      constructor(props){
        super(props)
        this.state={
            
            toCountry:'', 
            dontknow:false,
            agree:true
        }
    }
    componentDidMount(){
      if(this.creditcard !== undefined)
          this.creditcard.focus();
    }
    onNextClicked(){
        console.log('====================================');
        console.log("dddddddddd");
        console.log('====================================');
        this.props.navigation.navigate("EmiratesIdScan")
    }
    onMonPressed(index){
        console.log('====================================');
        console.log(index);
        console.log('====================================');
      if(index === 0)
          this.setState({mon0:!this.state.mon0})
    }
    onToCountryChanged(itemValue){
        this.setState({toCountry: itemValue})
        if(this.state.fromCountry !== ""){
            
          this.setState({result:rate * parseInt(this.state.amount)})
          }
        }
    render() {
        return (
            <View style={{flex:1,alignItems:"center"}}>

<View style={{width:width(95),flexDirection:"row",height:height(12),marginTop:height(2),marginBottom:height(5),justifyContent:"center"}}>
                <View style={{width:width(15),alignItems:"center",marginRight:width(2)}}>
                <Image  source={require("../image/creditscore.png")} style={{width:width(12),height:height(10)}} resizeMode="contain"/>
                       
                </View>
                
                <View style={{width:width(80)}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",marginBottom:height(1)}}>Your Credit Score</Text>
                <View style={{width:width(80)}}>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>Great ! You are just one steps away ! We need your</Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>consent to check your credit history.Don't worry,</Text>
                <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>Checking on MoneyMall doesnt harm your credit score.</Text>
                </View>
                </View>
                </View>
           

           
            <View style={{height:height(60),alignItems:"center"}}>
            <View style={{height:height(15),width:width(90)}}>
                    <View style={{height:height(4)}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>What's your credit score ?</Text>
                    </View>
                    <View style={{width:width(90),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                <View  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                <TouchableOpacity disabled={this.state.dontknow}  style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                   {this.state.dontknow === false
                   ?
                   <TextInput  style={{width:width(90),height:height(6)}}  placeholder="0"
                   ref={(input) => { this.creditcard = input; }}  textAlign={"center"}/>
                   :
                   <View/>
                }
               </TouchableOpacity>
               </View>

               </View>
               <View  style={{width:width(90),flexDirection:"row",alignItems:"center",justifyContent:"center",height:height(6)}}>
                <TouchableOpacity  style={{width:width(90),flexDirection:"row",alignItems:"center",height:height(6)}}>
                <View style={{width:width(10)}}>
                <CheckBox
                 
                 value={this.state.dontknow}
                 onValueChange={() => this.setState({dontknow:!this.state.dontknow})}
               />
                </View>
                <TouchableOpacity onPress={() => this.setState({dontknow:!this.state.dontknow})}>
                   <Text style={{fontSize:16,fontFamily:"Ubuntu-Light"}}>I don't Know</Text>
                   </TouchableOpacity>
               </TouchableOpacity>
               </View>

               </View>
               <View style={{flexDirection:"row",marginTop:height(3),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8"}}>
                <TouchableOpacity flexDirection={"row"} style={{width:width(90),flexDirection:"row",alignItems:"center",justifyContent:"center",height:height(6)}}>
                <View style={{width:width(10)}}>
                <CheckBox
                 value={this.state.agree}
                 onValueChange={() => this.setState({agree:!this.state.agree})}
               />
               </View>
               <TouchableOpacity onPress={() => this.setState({agree:!this.state.agree})} style={{width:width(80)}}>
               <Text style={{fontSize: 10,fontFamily:"OpenSans-Regular"}} >
                     I agree to <Text onPress={() => Linking.openURL('https://moneymall.ae/terms-of-use')} style={{color:"#097FF5",textDecorationLine:"underline"}} textDecorationLine={"underline"}>terms of use of MoneyMall</Text>  & provide <Text onPress={() => Linking.openURL('https://moneymall.ae/consent')} style={{color:"#097FF5",textDecorationLine:"underline"}} textDecorationLine={"underline"}>my Consent</Text> to MoneyMall & Its partner banks to check my credit bureau records.
               </Text>
                </TouchableOpacity>
                </TouchableOpacity>
                </View>
                </View>
               <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(4),alignItems:"center",justifyContent:"center"}}  onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                        Continue
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>

                </View>
                

            
        
        )
    }
}
