import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,CheckBox} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

export default class ApplyThirdLevel extends Component {

    static navigationOptions = {
        headerRight:<Image style={{marginRight:width(3)}} source={require("../image/avatar.png")} width={width(8)} height={height(6)} resizeMode="contain"/>,
     
    headerLeft:<Image style={{marginLeft:width(3)}} source={require("../image/headerback.png")} width={width(10)} height={height(6)} resizeMode="contain"/>,
        headerStyle:{
         
      borderBottomWidth:0,
      height:height(8),
      elevation: 0,
      shadowOpacity: 0
        }
      };

      constructor(props){
          super(props)
          this.state={
              index:0,
              noofyears:0,
              noifyearsinuae:false
          }
      }

    onGenderPressed(idx){
        this.setState({index:idx})
    }
    onUpPressed(){
        this.setState({noofyears:this.state.noofyears+1})

    }
    onUpPressedUAE(){
        this.setState({noifyearsinuae:!this.state.noifyearsinuae})

    }
    onDownPressedUAE(){
        
        this.setState({noifyearsinuae:!this.state.noifyearsinuae})
    }

    onDownPressed(){
        if(this.state.noofyears === 0) return;
        this.setState({noofyears:this.state.noofyears-1})
    }
    onNextClicked(){
        this.props.navigation.navigate("ApplyFourthLevel");
    }

    render() {
        return (
            <View style={{flex:1,alignItems:"center"}}>
                <View style={{width:width(99),height:height(20),alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:16}}> Gender </Text>
                    {this.state.index === 0
                        ?
                    <View style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                        
                        <TouchableOpacity style={{width:width(20),alignItems:"center",justifyContent:"center"}} onPress={this.onGenderPressed.bind(this,0)}>
                            <Image source={require("../image/male.png")} style={{width:width(15),height:height(12)}} resizeMode="contain"/>
                            <TouchableOpacity style={{margin:5,borderBottomWidth:1,borderBottomColor:"#035AF1",width:width(10),alignItems:"center",justifyContent:"center"}}>
                            <Text>Male</Text>
                            </TouchableOpacity>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width:width(20),alignItems:"center",justifyContent:"center"}} onPress={this.onGenderPressed.bind(this,1)}>
                            <Image source={require("../image/female.png")} style={{width:width(15),height:height(12)}} resizeMode="contain"/>
                            <TouchableOpacity style={{margin:5,borderBottomWidth:1,borderBottomColor:"#fff",width:width(12),alignItems:"center",justifyContent:"center"}}>
                        
                            <Text>Female</Text>
                            </TouchableOpacity>
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                        
                    <TouchableOpacity style={{width:width(20),alignItems:"center",justifyContent:"center"}} onPress={this.onGenderPressed.bind(this,0)}>
                        <Image source={require("../image/male.png")} style={{width:width(15),height:height(12)}} resizeMode="contain"/>
                        <TouchableOpacity style={{margin:5,borderBottomWidth:1,borderBottomColor:"#fff",width:width(12),alignItems:"center",justifyContent:"center"}}>
                           
                        <Text>Male</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(20),alignItems:"center",justifyContent:"center"}} onPress={this.onGenderPressed.bind(this,1)}>
                        <Image source={require("../image/female.png")} style={{width:width(15),height:height(12)}} resizeMode="contain"/>
                        <TouchableOpacity style={{margin:5,borderBottomWidth:1,borderBottomColor:"#035AF1",width:width(12),alignItems:"center",justifyContent:"center"}}>
                            <Text>Female</Text>
                            </TouchableOpacity>
                    </TouchableOpacity>
                </View>
                    }
                </View>
                <View style={{width:width(99),height:height(10),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(80)}}>
                        <Text>
                            Martial Status
                        </Text>
                    </View>
                    <View style={{width:width(99),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                        <CheckBox col/>
                        <Text>Single</Text>
                        <CheckBox/>
                        <Text>Married</Text>
                    </View>
                </View>

                <View style={{height:height(40),alignItems:"center",justifyContent:"center"}}>

                <View style={{flexDirection:"row",width:width(99),height:height(5),margin:height(3),alignItems:"center",justifyContent:"center"}}>
                
                <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(80),margin:15}}>
                <Text>
                    Years in UAE
                </Text>
                </View>
                <View style={{width:width(80),flexDirection:"row",backgroundColor:"#fff",alignItems:"center",justifyContent:"center",elevation:8,height:height(5)}}>
                <View style={{width:width(60),alignItems:"center",justifyContent:"center"}}>
                
                <Text>
                   {this.state.noofyears} <Text>Years</Text>
                </Text>
                </View>
                <View style={{width:width(10)}}>
                <TouchableOpacity style={{width:width(5),height:height(3),alignItems:"center",justifyContent:"center"}} onPress={this.onUpPressed.bind(this)}>
                    <Image source={require("../image/up.png")} style={{width:width(3),height:height(2)}} resizeMode="contain"/>
                </TouchableOpacity>
                <TouchableOpacity style={{width:width(5),height:height(3),alignItems:"center",justifyContent:"center"}} onPress={this.onDownPressed.bind(this)}>
                    <Image source={require("../image/down.png")} style={{width:width(3),height:height(2)}} resizeMode="contain"/>
                </TouchableOpacity>
                      
                </View>


                </View>
                </View>
                </View>

                
                <View style={{flexDirection:"row",width:width(99),height:height(5),margin:height(3),alignItems:"center",justifyContent:"center"}}>
                
                <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(80),margin:15}}>
                <Text>
                    Resident Type
                </Text>
                </View>
                <View style={{width:width(80),flexDirection:"row",backgroundColor:"#fff",alignItems:"center",justifyContent:"center",elevation:8,height:height(5)}}>
                <TouchableOpacity style={{width:width(10),height:height(3),alignItems:"center",justifyContent:"center"}} onPress={this.onUpPressedUAE.bind(this)}>
                    <Image source={require("../image/left.png")} style={{width:width(3),height:height(2)}} resizeMode="contain"/>
                </TouchableOpacity>
                <View style={{width:width(60),alignItems:"center",justifyContent:"center"}}>
                {this.state.noifyearsinuae
                ?
                <Text>
                   Rented
                </Text>
                :
                <Text>
                   Owned
                </Text>
                }
                </View>
               
                <TouchableOpacity style={{width:width(10),height:height(3),alignItems:"center",justifyContent:"center"}} onPress={this.onDownPressedUAE.bind(this)}>
                    <Image source={require("../image/right.png")} style={{width:width(3),height:height(2)}} resizeMode="contain"/>
                </TouchableOpacity>
                      

                </View>
                </View>
                </View>
                </View>

                <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        Next
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>

            </View>
        )
    }
}
