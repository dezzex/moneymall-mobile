import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
  } from 'react-native';
  import {width,height} from 'react-native-dimension';
  import PINCode from '@haskkor/react-native-pincode'
import { TextInput } from 'react-native-gesture-handler';
export default class componentName extends Component {
    constructor(props){
        super(props)
        this.state={
            fingerPrint:true,
            first:0,
            second:0,
            third:0,
            fourth:0
        }
    }
    static navigationOptions = {
        header:null
    };
    onSignInPressed(){
            this.setState({fingerPrint:!this.state.fingerPrint})
    }
    onApplyPressed(){
        this.props.navigation.navigate("Home")
    }
    render() {
        return (
            <View style={styles.container}>
                {this.state.fingerPrint
                    ?
                    <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                    <View style={{flexDirection: 'column',width:width(99),alignItems:"center",justifyContent:"center"}}>
                    <Text style={{flexWrap:"nowrap"}}>
                    Input your fingerprint or the Lock ScreenPasscode to verify your identity andcontinue
                    </Text>
                    </View>
                   
                    <TouchableOpacity style={styles.signin} onPress={this.onSignInPressed.bind(this)}>
                    <Text style={styles.signintext}>
                            Sign In
                    </Text>
                </TouchableOpacity>
                    
                    </View>
                    :
                    <View style={{width:width(99),alignItems:"center",justifyContent:"center"}}>
                        <Text style={styles.verifyText}>
                        Verify Your Account
                        </Text>
                        <View style={{width:width(99),alignItems:"center",justifyContent:"center"}}>
                        <Text style={{flexWrap:"nowrap",alignItems:"flex-start"}}>
                        OTP has been sent to your mobile number and email 
                        </Text>

                        </View>
                        <View style={{width:width(99),flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:height(5)}}>
                        <TouchableOpacity style={styles.verifycontainer}>
                          <TextInput style={styles.verifyText}
                        value={this.state.first.toString()}
                        onChangeText={(text) => this.setState({first:text})}
                        placeholder="0">
                            </TextInput>
                        </TouchableOpacity>    
                     
                        <TouchableOpacity style={styles.verifycontainer}>
                        <TextInput style={styles.verifyText}
                        value={this.state.second.toString()}
                        onChangeText={(text) => this.setState({second:text})}
                        placeholder="0">
                            </TextInput>
                      </TouchableOpacity>  
                      <TouchableOpacity style={styles.verifycontainer}>
                      <TextInput style={styles.verifyText}
                      value={this.state.third.toString()}
                      onChangeText={(text) => this.setState({third:text})}
                      placeholder="0">
                          </TextInput>
                    </TouchableOpacity>    
                    <TouchableOpacity style={styles.verifycontainerlast}>
                    <TextInput style={styles.verifyText}
                    value={this.state.fourth.toString()}
                    onChangeText={(text) => this.setState({fourth:text})}
                    placeholder="0">
                        </TextInput>
                  </TouchableOpacity>   
                        </View>
                        <View style={{width:width(50),marginTop:height(2)}}>
                        <Text>
                        Didn't receive the OTP?
                        </Text>
                       
                        </View>
                        <View style={{flexDirection:"row",width:width(70),alignItems:"center",justifyContent:"space-between"}}>
                        <TouchableOpacity style={styles.receive} onPress={this.onSignInPressed.bind(this)}>
                        <Text style={styles.receivetext}>
                                Receive
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.apply} onPress={this.onApplyPressed.bind(this)}>
                    <Text style={styles.applytext}>
                            Apply
                    </Text>
                </TouchableOpacity>
                        </View>
                     
                     
                    </View>
                }
               
            </View>
        )
    }
}

const styles = StyleSheet.create({ 
    container:{
        flex:1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center'
    },

    signin:{
        borderWidth: 1,
        borderColor: "#097FF5",
        width:width(80),
        height:height(7),
        alignItems: 'center',
        justifyContent:'center',
        marginTop: height(4),
        borderRadius: 4,

    },
    verifyText:{
        fontSize: 26,
    },
    signintext:{
        fontSize: 16,
    },
    verifycontainer:{
        alignItems:"center",
        justifyContent:"center",
        width:width(20),
        height:height(4),
        borderRightWidth:1,
        borderRightColor:"#707070"

    },
    verifycontainerlast:{
        alignItems:"center",
        justifyContent:"center",
        width:width(10),
        height:height(4)

    },
    receive:{
        borderWidth: 1,
        borderColor: "#097FF5",
        width:width(30),
        height:height(5),
        alignItems: 'center',
        justifyContent:'center',
        marginTop: height(4),
        borderRadius: 4,

    },
    apply:{
        backgroundColor: "#097FF5",
        width:width(30),
        height:height(5),
        alignItems: 'center',
        justifyContent:'center',
        marginTop: height(4),
        borderRadius: 4,

    },
    applytext:{
        fontSize: 16,
        color:"#fff"
    },
    receivetext:{
        fontSize: 16,
    },
})
