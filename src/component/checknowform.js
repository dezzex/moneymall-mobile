import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,Keyboard } from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Slider from '@react-native-community/slider';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DateTimePicker from '@react-native-community/datetimepicker';

import  worldlist from './world.json';
export default class CheckNowForm extends Component {

  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };
  constructor(props) {
    super(props);
    this.state = {
        slideCompletionValue:1000,
        gender:"Male",
        show:false,
        mobileno:"",
                fromCountry:'',
                toCountry:'',
                date: new Date("07/06/89"),
                mode: 'date',
    };
  }
  componentDidMount(){
    this.firstTextInput.focus();
  }

  setDate = (event, date) => {
    console.log('====================================');
    console.log(date);
    console.log('====================================');
    date = date || this.state.date;

    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      date:date,
    });
  }

  show = mode => {
    this.setState({
      show: true,
      mode,
    });
  }

  datepicker = () => {
    this.show('date');
  }

  timepicker = () => {
    this.show('time');
  }
  onMobileNoChanged(text){
    if(this.state.mobileno.length === 0){
      text = "+971"+text;
    }
    
    if(text.length === 13){
      
    console.log("Dssssss",text.length);
    
    Keyboard.dismiss();
      this.mobileno.blur();
        this.onDatePressed();
    }
    this.setState({mobileno:text})
  }
  onNextClicked(){
        this.props.navigation.navigate("CheckNow2");
  }
     onDatePressed(){
       console.log('====================================');
       console.log("ddddddddd");
       console.log('====================================');
       this.setState({show:!this.state.show})
     }
    render() {
      
      var month = this.state.date.getMonth()+1;
        return (
          <KeyboardAwareScrollView>
            <View  style={{flex:1,alignItems:"center",justifyContent:"center",height:height(85)}}>

                <View style={{width:width(90),height:height(8),justifyContent:"center"}}>
                  <Text style={{fontSize:18,fontFamily:"Ubuntu-Regular"}}>
                  Hello there ! 
                  </Text>
                  <Text style={{fontSize:18,fontFamily:"Ubuntu-Regular"}}>
                  Let's Check your AECB Credit Score 
                  </Text>
                </View>
               <View style={{width:width(90),marginTop:height(2)}}>
               <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> EMIRATES ID </Text>
               </View>
             
                <View style={{flexDirection:"row",margin:height(1),alignItems:"center",justifyContent:"center",width:width(100)}}>
                <TouchableOpacity style={{width:width(15),alignItems:"center",justifyContent:"center",marginRight:width(3),backgroundColor:"#fff",borderColor:"#B8B8B8",flexDirection:"row",backgroundColor:"#F6F6F6",height:height(6)}}>
               
                <TextInput
               placeholder="784"
               keyboardType="number-pad"
               autoFocus={true}
               style={{width:width(12),alignItems:"center",justifyContent:"center",textAlign:"center"}}
               onSubmitEditing={() => { this.secondTextInput.focus(); }}
               onChangeText={text => onChangeText(text)}
               ref={(input) => { this.firstTextInput = input; }}
               onChangeText={(text) => {
                  text.length === 3 ?  this.secondTextInput.focus() : ""
                }}  
            maxLength={3}
               />
                </TouchableOpacity>
                
            <TouchableOpacity style={{width:width(20),alignItems:"center",marginRight:width(3),justifyContent:"center",borderColor:"#B8B8B8",backgroundColor:"#F6F6F6",height:height(6)}}>
               <TextInput
               placeholder="XXXX"
               keyboardType="number-pad"
               maxLength={4}
               style={{width:width(12),alignItems:"center",justifyContent:"center",textAlign:"center"}}
               onSubmitEditing={() => { this.thirdTextInput.focus(); }}
               onChangeText={(text) => { text.length === 4 ?  this.thirdTextInput.focus() : "" }}
               ref={(input) => { this.secondTextInput = input; }}
               />
            
            </TouchableOpacity>
            <TouchableOpacity style={{width:width(30),alignItems:"center",marginRight:width(3),justifyContent:"center",borderColor:"#B8B8B8",backgroundColor:"#F6F6F6",height:height(6)}}>
               <TextInput
               placeholder="XXXXXXX"
               maxLength={7}
               keyboardType="number-pad"
               style={{width:width(18),alignItems:"center",justifyContent:"center",textAlign:"center"}}
               onSubmitEditing={() => { this.fourthTextInput.focus(); }}
               onChangeText={(text) => { text.length === 7 ?  this.fourthTextInput.focus() : "" }}
               ref={(input) => { this.thirdTextInput = input; }}
               />
            
            </TouchableOpacity>
            <TouchableOpacity style={{width:width(15),alignItems:"center",marginRight:width(3),justifyContent:"center",borderColor:"#B8B8B8",backgroundColor:"#F6F6F6",height:height(6)}}>
               <TextInput
               placeholder="X"
               keyboardType="number-pad"
               maxLength={1}
               style={{width:width(6),alignItems:"center",justifyContent:"center",textAlign:"center"}}
               
               onSubmitEditing={() => { this.fifthTextInput.focus(); }}
               onChangeText={(text) => { text.length === 1 ?  this.fifthTextInput.focus() : "" }}
               ref={(input) => { this.fourthTextInput = input; }}
               />
            
            </TouchableOpacity>
            </View>
          
            <View style={{width:width(90),marginTop:height(2)}}>
               <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> FULL NAME </Text>
               </View>
            <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                 <TextInput
                  textAlign={"center"}
               placeholder="as per Emirates ID"
               style={{width:width(90),alignItems:"center",justifyContent:"center"}}
               ref={(input) => { this.fifthTextInput = input; }}
               type="name"
               />
          
            </TouchableOpacity>
            </View>
            

            <View style={{width:width(90)}}>
               <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> MOBILE NO </Text>
               </View>

            <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
                <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                 <TextInput
                 textAlign={"center"}
               placeholder="+971 -5X - XXX -XXXX"
               maxLength={13}
               value={this.state.mobileno}
               

               ref={(input) => { this.mobileno = input; }}
               onChangeText={this.onMobileNoChanged.bind(this)}
               style={{width:width(80),alignItems:"center",justifyContent:"center"}}
               />
               </TouchableOpacity>
               </View>

         
               <View style={{width:width(90)}}>
               <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> DATE OF BIRTH </Text>
               </View>
                   
            <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(1),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
             
                <TouchableOpacity
                onPress={this.onDatePressed.bind(this)} style={{width:width(80),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                { this.state.show && <DateTimePicker value={this.state.date}
                    mode={this.state.mode}
                    is24Hour={true}
                    display="default"
                    
              
                    value={this.state.date}
                    onChange={this.setDate.bind(this)} />

        }
           <TextInput
               style={{width:width(60),alignItems:"center",justifyContent:"center",color:"#000"}}
               value={this.state.date.getDate()+"/"+month+"/"+this.state.date.getFullYear()}
               textAlign={"center"}
               ref={(input) => { this.dateofbirth = input; }}
               editable={false}
              //  placeholder={this.state.date.getDate()+"/"+month+"/"+this.state.date.getFullYear()}
               />
            
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onDatePressed.bind(this)} style={{width:width(10),height:height(6),alignItems:"center",justifyContent:"center",backgroundColor:"#F6F6F6"}}>
               <Image source={require("../image/salary.png")} resizeMode="contain" style={{height:height(5),width:width(5)}}/>
               </TouchableOpacity>
            </View>

            <View style={{marginTop:height(1),marginBottom:height(1),width:width(90),height:height(15),justifyContent:"center"}}>
              <Text style={{color:"grey"}}>
              {'\u2713'} Its provided by AECB &  It only takes few minutes.
              </Text>
              <Text style={{color:"grey"}}>
              {'\u2713'} Checking on MoneyMall won't harm your score.
              </Text>
              <Text style={{color:"grey"}}>
              {'\u2713'} Your Data is 100% Secured.
              </Text>
            </View>

            <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        CONTINUE
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>

            </View >
            </KeyboardAwareScrollView>
        )
    }
}
