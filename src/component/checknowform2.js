import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,Picker,CheckBox ,Linking,
  Modal} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Slider from '@react-native-community/slider';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import  worldlist from './world.json';
export default class CheckNowForm extends Component {

  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };
  constructor(props) {
    super(props);
    this.state = {
        slideCompletionValue:1000,
                fromCountry:'',
                toCountry:'',       
                 gender:"Male",
                show:false,
                agree:true,
                male:false,
                female:true,
                modalvisible:false
    };
  }
  componentDidMount(){
    this.firstTextInput.focus();
  }

  onNextClicked(){
        this.props.navigation.navigate("OTPVerification");
  }
  onFromCountryChanged(itemValue){
    this.setState({fromCountry: itemValue})
    if( this.state.toCountry !== ""){
 
   this.setState({result:rate * parseInt(this.state.amount)})
    }
 }
 onTextPressed(){
   console.log("ddddddddddd");
   this.setState({modalvisible:true})
   
 }
 onModelClosed(){
   this.setState({modalvisible:false})
 }
 onToCountryChanged(itemValue){
     this.setState({toCountry: itemValue})
     if(this.state.fromCountry !== ""){
         
       this.setState({result:rate * parseInt(this.state.amount)})
       }
     }
     onMalePressed(){
      this.setState({female:false,male:true})
  }
  onFemalePressed(){
    this.setState({female:true,male:false})
}
     
  onFromCountryChanged(itemValue){
    this.setState({fromCountry: itemValue})
    if( this.state.toCountry !== ""){
 
   this.setState({result:rate * parseInt(this.state.amount)})
    }
 }
 onToCountryChanged(itemValue){
     this.setState({toCountry: itemValue})
     if(this.state.fromCountry !== ""){
         
       this.setState({result:rate * parseInt(this.state.amount)})
       }
     }
     onGenderChanged(itemValue){
      this.setState({gender: itemValue})
      }
    render() {
        return (
            <KeyboardAwareScrollView>
                <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalvisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={{width:width(99),height:height(80),alignItems:"center",justifyContent:"center"}}>
        <View style={{height:height(45),width:width(80),backgroundColor:"grey"}}>
          <View style={{width:width(80),height:height(4),backgroundColor:"#0354F1",justifyContent:"center",paddingLeft:width(5)}}>
            
          <Text style={{color:"#fff",fontSize:16,fontFamily:"Ubuntu-Bold"}}>
              Terms & Conditions
            </Text>
          </View>
          <View style={{width:width(80),height:height(35),backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",paddingLeft:width(2)}}>
            <ScrollView>
            <Text  style={{fontSize:14,fontFamily:"Ubuntu-Regular"}}>
            You accept and agree that MoneyMall reserves the right to reject your request for collecting, receiving, obtaining your AECB Credit Report and/or Score without assigning any reason thereof. 
            </Text>

            <Text  style={{fontSize:14,fontFamily:"Ubuntu-Regular",marginTop:height(3)}}>
            {'\u2B24'} You hereby authorize MoneyMall to share your personal details (as provided by you) with AECB to verify your identity and accordingly provide you the AECB Credit Report and/or Score. 
            </Text>
            <Text  style={{fontSize:14,fontFamily:"Ubuntu-Regular",marginTop:height(3)}}>
            {'\u2B24'} You fully understand and agree that the AECB Credit Score are provided to you for informationonly and should not be construed as financial advice. MoneyMall recommends that you confirm the accuracy and completeness of the information provided to AECB by your data providers (such as banks and finance companies) by reviewing your AECB Credit Report, to ensure the AECB Credit Score is calculated utilizing accurate and complete information. 
            </Text>
            <Text  style={{fontSize:14,fontFamily:"Ubuntu-Regular",marginTop:height(3)}}>
            {'\u2B24'} Your AECB Credit Report and/or Score provided/displayed to you is on an “as is”, “as available” basis and as provided by AECB. ▪ MoneyMall shall not accept any liability, whatsoever, for any issues pertaining to the content ofyour AECB Credit Report and/or Score 
            </Text>
            <Text  style={{fontSize:14,fontFamily:"Ubuntu-Regular",marginTop:height(3)}}>
            {'\u2B24'} MoneyMall reserves the right to update, change or cancel any or all these Terms and Conditions in its sole and absolute discretion and without prior notice.
            </Text>
            </ScrollView>

         
          </View>
          <View style={{width:width(80),height:height(6),alignItems:"center",justifyContent:"center",backgroundColor:"#F6F6F6"}}>
          <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(80),height:height(4)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(80),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onModelClosed.bind(this)}>
                        <Text style={{color:"#fff",fontSize:16,fontFamily:"Ubuntu-Regular"}}>
                        CLOSE
                      </Text>
                      </TouchableOpacity>
                      </LinearGradient>
</View>

        </View>
        </View>
        </Modal>
            <View style={{alignItems:"center",justifyContent:"center",height:height(90)}}>
            <View style={{alignItems:"center",justifyContent:"center",height:height(65)}} >

            <View style={{width:width(90)}}>
               <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> EMAIL </Text>
               </View>
            <View style={{flexDirection:"row",marginTop:height(3),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                 <TextInput
                 textAlign={"center"}
            placeholder="yourname@domain.com"
            style={{width:width(90),alignItems:"center",justifyContent:"center"}}
            
            ref={(input) => { this.firstTextInput = input; }}
            />
         
         </TouchableOpacity>
         </View>


         
                   
         <View style={{width:width(90)}}>
               <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> NATIONALITY </Text>
               </View>
               <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
               <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                           <Picker
                    style={{height:height(6),width:width(90),alignItems:"center"}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onToCountryChanged.bind(this)}>
                        {Object.keys(worldlist).map((key,idx)=>{
                            return(
                             <Picker.Item style={{marginLeft: width(2)}} label={"   "+worldlist[key].emoji+"  "+"      "+worldlist[key].name} value={worldlist[key]} key={idx} />
                            )
                        }
                        )}
                    </Picker>  
            
            </TouchableOpacity>
            </View>
            <View style={{width:width(90)}}>
               <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> GENDER </Text>
               </View>
               <View style={{width:width(70),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                      <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 0,borderRadius:5,borderColor:"silver"}} onPress={this.onMalePressed.bind(this)}> 
                          <CheckBox value={this.state.male} onChange={this.onMalePressed.bind(this)}/>
                          <Image  source={require("../image/male.png")} style={{width:width(5),height:height(3)}} resizeMode="contain"/>
                        <Text>Male</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row",
                        borderWidth: 0,borderRadius:5,borderColor:"silver"}} onPress={this.onFemalePressed.bind(this)}> 
                          <CheckBox value={this.state.female} onChange={this.onFemalePressed.bind(this)}/>
                          <Image  source={require("../image/female.png")} style={{width:width(5),height:height(3)}} resizeMode="contain"/>
                        <Text>Female</Text>
                    </TouchableOpacity>
                    </View>
            {/* <View style={{height:height(8),width:width(88),marginTop:height(1),marginBottom:height(3), shadowOffset: { width: 10, height: 10 },
                    shadowColor: 'black',
                    
                    alignItems:"center",
                    justifyContent:"center",
                    shadowOpacity: 1,
                    borderRadius:12,
                    
                    borderWidth:2,
                    borderColor:"#E6E6E6",
                    backgroundColor : "#F6F6F6"}}>
                           <Picker
                    style={{height:height(6),width:width(90),alignItems:"center"}}
                    selectedValue={this.state.gender}
                    
                    onValueChange={this.onGenderChanged.bind(this)}>
                       
                             <Picker.Item style={{marginLeft: width(2)}} label={"    Male"} value={"Male"}  />
                             <Picker.Item  style={{marginLeft: width(2)}}label={"    Female"} value={"Female"}  />
                            
                    </Picker>  
            
            </View> */}
            <View style={{width:width(90)}}>
               <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>PASSPORT NUMBER</Text>
               </View>

               <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
                <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             
                <TextInput
                
                textAlign={"center"}
               placeholder="Optional"
               editable={true}
               style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}}
               />
                </TouchableOpacity>
                </View>

                <View style={{flexDirection:"row",marginTop:height(3),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8"}}>
                <TouchableOpacity flexDirection={"row"} style={{width:width(90),flexDirection:"row",alignItems:"center",justifyContent:"center",height:height(6)}}>
                <View style={{width:width(10)}}>
                <CheckBox
                 value={this.state.agree}
                 onValueChange={() => this.setState({agree:!this.state.agree})}
               />
               </View>
               <TouchableOpacity onPress={() => this.setState({agree:!this.state.agree})} style={{width:width(80)}}>
               <Text style={{fontSize: 16,fontFamily:"OpenSans-Regular"}} onPress={this.onTextPressed.bind(this)} style={{color:"#097FF5",textDecorationLine:"underline"}} textDecorationLine={"underline"}>I accept all Terms & Conditions.
                     {/* I agree to <Text onPress={() => Linking.openURL('https://moneymall.ae/terms-of-use')} style={{color:"#097FF5",textDecorationLine:"underline"}} textDecorationLine={"underline"}>terms of use of AECB</Text>  & provide <Text onPress={() => Linking.openURL('https://moneymall.ae/consent')} style={{color:"#097FF5",textDecorationLine:"underline"}} textDecorationLine={"underline"}>my Consent</Text> to MoneyMall & Its partner banks to check my credit bureau records. */}
               </Text>
                </TouchableOpacity>
                </TouchableOpacity>
                </View>
            </View>
         
            <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(10),marginTop:height(3)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(10),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:26,fontFamily:"Ubuntu-Regular"}}>
                        Get Your Credit Score
                      </Text>
                      <Text style={{color:"#fff",fontSize:14,fontFamily:"Ubuntu-Regular"}}>
                        AECB Charges : <Text style={{color:"#fff",fontSize:14,fontFamily:"Ubuntu-Bold"}}>AED 31.50</Text>  Incl VAT
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>

             

              {/* <View style={{height:height(4),justifyContent:"center",alignItems:"center",width:width(90)}}>
              <Text style={{color:"grey",fontSize:16,fontFamily:"Ubuntu-Bold"}}>
              AECB Charges = AED 31.50 Incl VAT
              </Text>
              </View> */}
            </View >
            </KeyboardAwareScrollView>
        )
    }
}
