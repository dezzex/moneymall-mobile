//import liraries
import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,Picker,ActivityIndicator,AsyncStorage, TextInput} from 'react-native';
import {width,height} from 'react-native-dimension';

// create a component
class CreditCardDetails extends Component {

    static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
           <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       
        </TouchableOpacity>
       ,
        headerStyle:{
          height:height(8),
        }
        }
      };
    componentDidMount(){
        
      var value = this.props.navigation.getParam('value', undefined);
      console.log("fffffff",value);
      
    }
    getCardType(value){
        if(value.CardType.toLowerCase() === "visa"){
            return(
                <Image source={require("../image/criteria/visa.png")} style={{width:width(15),height:height(8),marginRight:width(10)}} resizeMode="contain"/>
            )
        }
        if(value.CardType.toLowerCase() === "mastercard"){
            return(
                <Image source={require("../image/criteria/mastercard.png")} style={{width:width(15),height:height(8),marginRight:width(10)}} resizeMode="contain"/>
            )
        }
    }
    getCashBack(value){
        if(value.cashback !== undefined && value.cashback === "yes"){
            return(
                <Image source={require("../image/criteria/loan.png")} style={{width:width(12),height:height(12)}} resizeMode="contain"/>
            )
        }
        else{
            return(
            <View/>
            )
        }
    }
    getIslamic(value){
        if(value.islmaic_financeloan !== undefined && value.islmaic_financeloan === "yes"){
            return(
                <Image source={require("../image/criteria/islamic.png")} style={{width:width(12),height:height(12)}} resizeMode="contain"/>
            )
        }
        else{
            return(
            <View/>
            )
        }
    }
    getReward(value){
        if(value.creditcard_rewards !== undefined && value.creditcard_rewards === "yes"){
            return(
                <Image source={require("../image/criteria/gift.png")} style={{width:width(12),height:height(12)}} resizeMode="contain"/>
            )
        }
        else{
            return(
            <View/>
            )
        }
    }
    getBalanceTransfer(value){
        if(value.creditcardbalance_transfer !== undefined && value.creditcardbalance_transfer === "yes"){
            return(
                <Image source={require("../image/criteria/balancetransfer.png")} style={{width:width(12),height:height(12)}} resizeMode="contain"/>
            )
        }
        else{
            return(
            <View/>
            )
        }
    }
    getShopping(value){
        if(value.creditcard_shopping !== undefined && value.creditcard_shopping === "yes"){
            return(
                <Image source={require("../image/criteria/shopping.png")} style={{width:width(12),height:height(8)}} resizeMode="contain"/>
            )
        }
        else{
            return(
            <View/>
            )
        }
    }
    render() {
        var value = this.props.navigation.getParam('value', undefined);
        var tenure = this.props.navigation.getParam('tenure', undefined);
        var loan = this.props.navigation.getParam('loan', undefined);
        return (
            <View style={styles.container}>
                <View style={{width:width(100),height:height(15),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",color:"#fff"}}>
                    {value.name.trim()}
                </Text>
                <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold",color:"#000"}}>
                    EXPLORE AND ENJOY THE WORLD OF
                </Text>
                <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold",color:"#000"}}>
                   TRAVEL AND LIFESTYLE PERKS
                </Text>
                </View>
                <View style={{height:height(32),marginTop:height(8)}}>
                <View style={{width:width(85),height:height(30),borderRadius:18,backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}}>
               <Image  source={{uri: value.card_image}} style={{width:width(65),height:height(20),borderRadius:12}} resizeMode="contain"/>

                <View style={{height:height(25),width:width(85),alignItems:"flex-start",justifyContent:"flex-start",flexDirection:"row"}}>
                <View style={{width:width(27),height:height(20),flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",color:"#000"}}>
                   {Number.isInteger(value.annual_fees.split(" ")[1]) ? value.annual_fees.split(" ")[1] : 0}
                </Text>
                <Text style={{fontSize:14,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Annual
                </Text>
                <Text style={{fontSize:14,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Fees
                </Text>
                </View>
                <View style={{width:width(27),height:height(20),flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",color:"#000"}}>
                   {value.interest_rate}
                </Text>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Interest
                </Text>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Rate
                </Text>
                </View>
                <View style={{width:width(27),height:height(20),flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
                <TouchableOpacity style={{width:width(22),alignItems:"center",justifyContent:"center"}}>
                                <Text style={{color:value.id % 2 === 0 ? "red" : "green",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                                {value.id % 2 === 0 ? "LOW" : "HIGH"}
                                </Text>
                            </TouchableOpacity>
                <View style={{width:width(22),height:height(4),marginLeft:width(5),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Chances of Approval
                </Text>
                </View>
                </View>
                </View>
                </View>
                </View>
                <View style={{height:height(34)}}>
                <View style={{width:width(85),height:height(32),borderRadius:12,backgroundColor:"#fff"}}>
                <View style={{width:width(85),alignItems:"flex-start",padding:height(1),justifyContent:"center",flexDirection:"row"}}>
                <Text style={{fontSize:20,fontFamily:"Ubuntu-Regular",color:"#344046"}}>
                   Best for 
                   
                </Text>
                <Text  style={{fontSize:20,fontFamily:"Ubuntu-Bold",color:"#344046",marginLeft:width(2)}}>
                      CASH BACK  
                   </Text>
                </View>
                <View style={{width:width(85),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                    {this.getCardType(value)}
                    {this.getCashBack(value)}
                    {this.getShopping(value)}
                    {this.getIslamic(value)}
                    {this.getReward(value)}
                    {this.getBalanceTransfer(value)}
                </View>
                <View style={{height:height(8),width:width(85),alignItems:"center",justifyContent:"center"}}>
                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",width:width(75),borderBottomWidth:1,borderBottomColor:"#747474"}}>
                    <View style={{width:width(55),alignItems:"flex-start",justifyContent:"center",paddingLeft:width(6)}}>
                    <Text style={{fontSize:16,color:"#747474"}}>
                        Minimum Salary:
                    </Text>
                    </View>
                    <View style={{width:width(30),alignItems:"flex-start",justifyContent:"center"}}>
                    <Text style={{fontSize:16,color:"#707273"}}>
                        AED 5,000
                    </Text>
                    </View>
                </View>

                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",width:width(75),borderBottomWidth:1,borderBottomColor:"#747474"}}>
                    <View style={{width:width(55),alignItems:"flex-start",justifyContent:"center",paddingLeft:width(6)}}>
                    <Text style={{fontSize:16,color:"#747474"}}>
                        Balance Transfer:
                    </Text>
                    </View>
                    <View style={{width:width(30),alignItems:"flex-start",justifyContent:"center"}}>
                    <Text style={{fontSize:16,color:"#707273"}}>
                        {value.baltransfer.toUpperCase()}
                    </Text>
                    </View>
                </View>
                </View>
                <View style={{width:width(85),height:height(12),alignItems:"center",justifyContent:"center"}}>
                    <TouchableOpacity style={{backgroundColor:"#65a3dd",borderRadius:25,width:width(80),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={() => this.props.navigation.navigate("Apply")}>
                    <Text style={{fontSize:26,color:"#fff",fontFamily:"Ubuntu-Bold"}}>
                    Apply Now
                    </Text>
                    </TouchableOpacity>

                </View>
                </View>
                </View>
            </View>
      );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height:height(90),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1972d7',
    },
});

//make this component available to the app
export default CreditCardDetails;
