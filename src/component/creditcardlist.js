import React, { Component } from 'react'
import { View, Text ,Image,TouchableOpacity,Keyboard,Picker,ActivityIndicator} from 'react-native';
import {width,height} from 'react-native-dimension';
import Slideshow from 'react-native-slideshow';
import LinearGradient from 'react-native-linear-gradient';
import CreditCard from './CreditCard';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Slider from '@react-native-community/slider';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { Header } from 'react-navigation-stack';
const axios = require('axios');


export default class CreditCardList extends Component {
  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };

      constructor(props) {
        super(props);
    
        this.state = {
          position: 1,
          interval: null,
          dataSource: [],
          filter:'',
          isloading:true,
          data:[],
          mobileno:'',
          salary:'',
          employername:'',
          loan:'',
          tenure:'',
          categorylist:[],
          index:0,
          isForm:false,
          isFilterVisible:false,
          slideCompletionValue: 1000,
    slideCompletionCount: 1000,
    slideCompletionCountMonth:6,
    category:0
        };
      }

      onMobileNoChanged(text){
        if(this.state.mobileno.length === 0){
          text = "+971"+text;
        }
        
        if(text.length === 13){
          
        console.log("Dssssss",text.length);
        
        Keyboard.dismiss();
          this.salaryinput.focus();
        }
        this.setState({mobileno:text})
      }

    componentDidMount() {
      var self = this;
      var url  = "";
   
      var url = this.props.navigation.getParam('url', undefined);
      console.log(url);
      
      
      var categorylist = this.props.navigation.getParam('categorylist', undefined);
      var index = this.props.navigation.getParam('index', undefined);
      if(index !== undefined)
          this.setState({index:index})
        if(url !== undefined){
              axios.get(url)
              .then(function (response) {
                if(response.status === 200 ){
                  var list = response.data;
                  var dataSource = [];
                    var data = [1,2,2,3]
                      data.map((val,idx) => {
                          var data={title:'',url:require("../image/banner.png")}
                          dataSource.push(data)
                      })
                     
                      if(list.data !== undefined)
                            self.setState({isloading:false,data:list.data,dataSource:dataSource,categorylist:categorylist,category:categorylist[0].value})
                }
            });
        }
        if(index === 1 && this.firstTextInput !== undefined)
          this.firstTextInput.focus();
    }
    onBannerClicked(index){
        
      }
      onBestChanged(){

      }

    componentWillUnmount() {
        clearInterval(this.state.interval);
      }
      onFilterChanged(itemValue){
        i18n.locale = itemValue;
        this.setState({filter: itemValue})
    }
   fetchNewData(url){
    var self = this;
    axios.get(url)
        .then(function (response) {
          if(response.status === 200){
            var list = response.data;
            self.setState({isloading:false,data:list.data})
          }
      })
   }
    onCategoryChanged(item,itemVal){
      console.log('====================================');
      console.log(item,itemVal);
      console.log('====================================');
      this.setState({category: item})
    
      // if(item === "homeloan"){
      //   this.fetchNewData("http://3.82.220.184/homeloan")
      // }
      // else if(item === "creditcard"){
      //   this.fetchNewData("http://3.82.220.184/homeloan")
      // }
      // else if(item === "personalloan"){
      //   this.fetchNewData("http://3.82.220.184/personalloan")
      // }
      // else if(item === "islamic"){
      //   this.fetchNewData("http://3.82.220.184/islamicfinance")
      // }
  }
    onFilterPressed(){
      this.setState({isFilterVisible: !this.state.isFilterVisible})
  }

  onApplyClicked(){
    this.setState({isFilterVisible: !this.state.isFilterVisible})
     
  }

  onFindClicked(){
    console.log(this.state);
    
      this.setState({isForm:false})
  }
    

    render() {
      var temp ='';
        return (
            <View style={{height:height(90),alignItems:"center",justifyContent:"center"}}>
              {this.state.isFilterVisible
              ?
              <View >
              <View style={{width:width(85),height:height(70),alignItems:"center",justifyContent:"center"}}>
              <View style={{width:width(80)}}>
                
              <View style={{height:height(6)}}>
                <Text style={{fontSize: 18,fontFamily:"Ubuntu-Bold"}}>
                  Bank
                </Text>
                </View>
                <View style={{flexDirection:"row",height:height(10)}}>
                <View style={{width:width(15),height:height(5),alignItems:"center",justifyContent:"center"}}>
                <Image source={require("../image/bankicon.png")} width={width(10)} height={height(5)} resizeMode="contain"/>
                    </View>  

                <View style={{width:width(80),height:height(5),justifyContent:"center",alignItems:"center",borderWidth:0.25,borderColor:"#707070"}}>
                               <Picker
                                selectedValue={this.state.filter}
                                style={{width:width(80),height:height(5)}}
                                onValueChange={(itemValue, itemIndex) => this.onLanguageChanged(itemValue, itemIndex)
                                    
                                }>
                                  {this.state.data.length > 0 && this.state.data.map((val,idx) => {
                                    if(temp !== val.provider){
                                      temp = val.provider; 
                                    return(
                                     <Picker.Item key={idx} label={val.provider} value={val.provider} />
                                    )
                                    
                                    }
                                  })}
                                </Picker> 
                            </View>
                </View>
              </View>

              <View style={{width:width(80)}}>
                <View style={{height:height(6)}}>
                <Text style={{fontSize: 18,fontFamily:"Ubuntu-Bold"}}>
                  Your Monthly Salary
                </Text>
                </View>
                <View style={{flexDirection:"row",height:height(10)}}>
                <View style={{width:width(15),height:height(5),alignItems:"center",justifyContent:"center"}}>
                <Image source={require("../image/feeicon.png")} width={width(10)} height={height(5)} resizeMode="contain"/>
                          </View>  

                <View style={{width:width(80),height:height(5),justifyContent:"center",alignItems:"center"}}>
                <Slider
                    style={{width: width(60), height: height(5)}}
                    minimumValue={0}
                    maximumValue={10000}
                    value={this.state.slideCompletionValue}
                    step={1000}
                    minimumTrackTintColor="#4A90E2"
                    maximumTrackTintColor="#000000"
                    thumbTintColor='#4A90E2'
                    onSlidingComplete={value =>
                      this.setState({
                        slideCompletionValue: value,
                        slideCompletionCount: this.state.slideCompletionCount + 1,
                      })
                    }
                  />
                  <Text>
                      {this.state.slideCompletionValue} AED
                  </Text>
                </View>
                </View>
                <View style={{height:height(6)}}>
                <Text style={{fontSize: 18,fontFamily:"Ubuntu-Bold"}}>
                  Your Tenure
                </Text>
                </View>
                <View style={{flexDirection:"row",height:height(10)}}>
                <View style={{width:width(15),height:height(5),alignItems:"center",justifyContent:"center"}}>
                <Image source={require("../image/feeicon.png")} width={width(10)} height={height(5)} resizeMode="contain"/>
                          </View>  

                <View style={{width:width(80),height:height(5),justifyContent:"center",alignItems:"center"}}>
                <Slider
                    style={{width: width(60), height: height(5)}}
                    minimumValue={0}
                    maximumValue={48}
                    value={this.state.slideCompletionCountMonth}
                    step={6}
                    minimumTrackTintColor="#4A90E2"
                    maximumTrackTintColor="#000000"
                    thumbTintColor='#4A90E2'
                    onSlidingComplete={value =>
                      this.setState({
                        slideCompletionCountMonth: value,
                        slideCompletionCount: this.state.slideCompletionCountMonth + 1,
                      })
                    }
                  />
                  <Text>
                      {this.state.slideCompletionCountMonth} Months
                  </Text>
                </View>
                </View>
              </View>

              <View style={{width:width(80),height:height(5)}}>
                
              <View style={{height:height(6)}}>
              <Text style={{fontSize: 18,fontFamily:"Ubuntu-Bold"}}>
                  Benefits
                </Text>
                </View>
                <View style={{flexDirection:"row",height:height(10)}}>
                <View style={{width:width(15),height:height(5),alignItems:"center",justifyContent:"center"}}>
                <Image source={require("../image/benefitsicon.png")} width={width(10)} height={height(5)} resizeMode="contain"/>
                          </View>  

                <View style={{width:width(80),height:height(5),justifyContent:"center",alignItems:"center",borderWidth:0.25,borderColor:"#707070"}}>
                               <Picker
                                selectedValue={this.state.filter}
                                style={{width:width(80),height:height(5)}}
                                onValueChange={(itemValue, itemIndex) => this.onLanguageChanged(itemValue, itemIndex)
                                    
                                }>
                                  {this.state.data.length > 0 && this.state.data.map((val,idx) => {
                                    return(
                                     <Picker.Item key={idx} label={val.provider} value={val.provider} />
                                    )
                                  })}
                                </Picker> 
                            </View>
                </View>
              </View>
             
              </View>
              <View style={{width:width(95),margin:height(2),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
              <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(40),height:height(8),marginTop:height(5)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(100),height:height(8),alignItems:"center",justifyContent:"center"}} onPress={this.onApplyClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        Apply
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
             </View>
            </View>
          :
                this.state.isForm 
                ?
                this.state.index === 0
                ?
                
                <View/>
                :
                this.state.index === 1
                ?
                  <View/>
                :
                <View/>
                :
                <View style={{height:height(90),alignItems:"flex-start",justifyContent:"flex-start"}}>
                  <Image source={this.state.index === 0 ?require("../image/creditbanner.png") :require("../image/homebanner.png")}resizeMode="contain" style={{width:width(100),height:height(26),marginTop:-height(1.5)}}/>
                  <LinearGradient style={{width:width(100),height:this.state.index === 1 ? height(10) : height(8),flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:-height(1.5)}}
              
              colors={["#464646","#464646"]}>
                 {this.state.index === 0
                  ?
                <View style={{width:width(45),flexDirection:"column"}}>
                  
                <Text style={{color:"#fff",fontFamily:"Ubuntu-Regular",fontSize:14,marginBottom:this.state.index === 1 ? height(2) : 0}}>
                      Best For
                  </Text>
                 
                  <Picker
                    style={{height:height(4),width:width(40),alignItems:"center",backgroundColor:"#fff"}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onBestChanged.bind(this)}>
                        <Picker.Item style={{marginLeft: width(2)}} label={"All Cards"} value={"allcards"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"Balance Transfer"} value={"baltransfer"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"Shopping"} value={"shopping"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"CashBack"} value={"cashback"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"Rewards"} value={"rewards"}  />
                        <Picker.Item style={{marginLeft: width(2)}} label={"Credit Builder"} value={"creditbuilder"}  />
                           
                    </Picker>
                </View>
                :
                <View/>
                 }
                {this.state.index === 1
                ?
                <View style={{flexDirection:"column",width:width(32),display:"flex",height:height(12),alignItems:"center",justifyContent:"center"}}>
                  <View style={{height:height(4),width:width(30),alignItems:"flex-start",justifyContent:"center"}}>
                    <Text style={{fontSize:11,color:"#fff"}}>
                      Loan Amount
                    </Text>
                    </View>
                    <TextInput style={{width:width(30),height:height(4),backgroundColor:"#fff",fontSize:11,textAlign:"center"}} value={this.state.loan}/>
                </View>
                :
                  <View/>}
                   {this.state.index === 1
                ?
                <View style={{flexDirection:"column",width:width(32),display:"flex",height:height(12),alignItems:"center",justifyContent:"center"}}>
                    <View style={{height:height(4),width:width(30),alignItems:"flex-start",justifyContent:"center"}}>
                 
                    <Text style={{fontSize:11,color:"#fff"}}>
                      Tenure
                    </Text>
                    </View>
                    <TextInput style={{width:width(30),height:height(4),backgroundColor:"#fff",fontSize:11,textAlign:"center"}} value={this.state.tenure}/>
                </View>
                :
                  <View/>}
                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",width:this.state.index === 0 ? width(20) : width(8),marginTop:height(4)}}>
                  {this.state.index === 0
                  ?
                  <View style={{width:width(12)}}>
                  <Text style={{color:"#fff",fontFamily:"Ubuntu-Regular",fontSize:14}}>
                      Sort
                  </Text>
                
                  </View>
                  :
                  <View/>
    }

                  <TouchableOpacity  style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}} onPress={this.onFilterPressed.bind(this)}>
                      <Image source={require("../image/funnel.png")} style={{height:height(5),width:width(5)}} resizeMode="contain"/>
                  </TouchableOpacity>
              </View>
              <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",marginTop:height(4),width:this.state.index === 0 ? width(10) : width(8)}}>
              {this.state.index === 0
                  ?
                  <View style={{width:width(12)}}>
                  <Text style={{color:"#fff",fontFamily:"Ubuntu-Regular",fontSize:14}}>
                      Filter
                  </Text>
                
                  </View>
                  :
                  <View/>
    }

                  <TouchableOpacity  style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}} onPress={this.onFilterPressed.bind(this)}>
                      <Image source={require("../image/sort.png")} style={{height:height(5),width:width(5)}}   resizeMode="contain"/>
                  </TouchableOpacity>
              </View>
      </LinearGradient>
      <View style={{alignItems:"center",justifyContent:"center",height:height(55)}}>
                  {this.state.isloading
                  ?
                  <View style={{height:height(55),alignItems:"center",justifyContent:"center"}}>
                  <ActivityIndicator/>
                  </View>
                  :
                  <View style={{alignItems:"center",justifyContent:"center"}}>
                
              <View style={{height:height(55)}}>
                  <ScrollView showsVerticalScrollIndicator={true}>
                    { this.state.data.map((val,idx) => {
                      if(val !== undefined && val[this.state.category] === "yes"){
                      return(
                        <CreditCard key={idx} value={val} index={this.state.index} navigation={this.props.navigation} loan={this.state.loan} tenure={this.state.tenure}/>
                      )
                      }
                  })

                  }

                  </ScrollView>
                  </View>
                  </View>
                
                  }
                </View>
              
              </View>
              }
                 
            </View>
        )
    }
}
