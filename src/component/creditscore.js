import React, { Component } from 'react';
import { View, Text ,Image,StyleSheet,TouchableOpacity,ImageBackground} from 'react-native';
import {width,height} from 'react-native-dimension';
import ProgressCircle from 'react-native-progress-circle'
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from 'react-native-gesture-handler';

const items =[
  {
    title:"Manage your Credit Cards",
    subtitle:"Get free Alerts to Save Money",
    subtitle2:"Any Bank,Any Card - One App",
    image:require("../image/security.png")
  },
  {
    title:"New Credit Card Offers",
    subtitle:"Great Deals on Credit Cards and Loans",
    subtitle2:"Check Eligibility before you apply.",
    image:require("../image/signaling.png")
  },
  {
    title:"Free Money Coaching",
    subtitle:"Select your Goals",
    subtitle2:"Credit Card will help you achieve it",
    image:require("../image/business.png")
  }
]

export default class creditscore extends Component {
  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };
  constructor(props) {
    super(props);
    this.state = {
            index:0
    };
  }
  componentDidMount() {
      var self = this;
      setInterval(() => {
               var index = this.state.index;

               index = index + 1;
               if(index > 2)
                    index = 0;
              
              
               self.setState({index:index}) 
      }, 3000)
      
  }
  onReportsPressed(){
    this.props.navigation.navigate("CreditCardList");
  }
  onCreditScoreClicked(){
    this.props.navigation.navigate("CreditScore")
  }
  onMyRewardsClicked(){
    this.props.navigation.navigate("Rewards")
  }
  onHomePressed(){
    this.props.navigation.navigate("Home")
  }
onCardPressed(index){
  if(index === 0){
    
    this.props.navigation.navigate("MyMoney")
  }
  else if(index === 1){
    this.props.navigation.navigate("OfferSelector")
  }
  else if(index === 2){
    this.props.navigation.navigate("MyGoals")
  }
}
  getRewardCards(item,idx){
    return(
      <TouchableOpacity key={idx} style={{width:width(80),height:height(20),marginLeft:width(5),marginRight:width(5),alignItems:"center",justifyContent:"center",backgroundColor:"#0354F1",borderRadius:12}} onPress={this.onCardPressed.bind(this,idx)}>
      <View style={{height:height(12),width:width(80),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
      <Image source={item.image} resizeMode="contain" style={{width:width(20),height:height(8)}}/>
      <View style={{width:width(40)}}>
      <Text style={{fontSize:24,fontFamily:"Ubuntu-Regular",color:"#fff"}}>
        {item.title}
      </Text>
      </View>
      </View>
      <View style={{width:width(80),height:height(4),alignItems:"center",justifyContent:"center"}}>
      <Text style={{fontSize:16,fontFamily:"Ubuntu-Regular",color:"#fff"}}>
        {item.subtitle}
      </Text>
      <Text style={{fontSize:16,fontFamily:"Ubuntu-Regular",color:"#fff"}}>
      {item.subtitle2}
      </Text>
      </View>
    </TouchableOpacity>
    )
  }
  
  onCheckPressed(){
    this.props.navigation.navigate("CreditScore")
  }
  onCreditScoreClicked(){
    this.props.navigation.navigate("CreditScore")
  }
  onMyRewardsClicked(){
    this.props.navigation.navigate("Rewards")
  }
  onMyMoneyPressed(){
    this.props.navigation.navigate("MyMoney")
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems:"center",justifyContent:"flex-start",height:height(80)}}>
          <View style={{width:width(90),alignItems:"center",justifyContent:"center",marginTop:10}}>
          <Text style={{fontSize:20,fontFamily:"Ubuntu-Regular"}}>Welcome,Ahmed</Text>
          </View>
          <View style={{height:height(50),alignItems:"center",justifyContent:"center"}}>
          <ProgressCircle
        percent={70}
        radius={height(22)}
        borderWidth={height(3)}
        color="#3399FF"
        bgColor="#fff"
        // shadowColor="#999"
  >
  {this.state.index == 0
    ?
  <View style={{flexDirection: 'column',height:height(50),alignItems: 'center',justifyContent:"center"}}>
            <View style={{height:height(25),alignItems:"center",justifyContent:"center"}}>
      <Text style={{fontSize:20,fontFamily:"Ubuntu-Regular"}}>
            Your Credit Score
            </Text>
            <View style={{width:width(60),height:height(15),alignItems:"center",justifyContent:"center"}}>
            <Text style={{fontSize:height(15),fontFamily:"Ubuntu-Bold",color:"#0e4cb0"}}>
            {'720'}
            </Text>
            </View>
            <Text style={{fontSize:16,fontFamily:"Ubuntu-Regular"}}>
            {'Out of 900'}
            </Text>
            </View>
       </View>
      :
      this.state.index === 1
      ?
      <View style={{flexDirection: 'column',height:height(50),alignItems: 'center',justifyContent:"center"}}>
      <View style={{height:height(27),alignItems:"center",justifyContent:"center"}}>
        <View style={{height:height(5),alignItems:"center",justifyContent:"center"}}>
      <Text style={{fontSize:20,fontFamily:"Ubuntu-Regular"}}>
      Your Credit Score is
      </Text>
      </View>
      <View style={{width:width(60),height:height(15),alignItems:"center",justifyContent:"center"}}>
      <Text style={{fontSize:height(8),fontFamily:"Ubuntu-Bold",color:"#0e4cb0"}}>
      GOOD
      </Text>
      <Text style={{fontSize:16,fontFamily:"Ubuntu-Regular"}}>
      Checked on 12.04.2020
      </Text>
      </View>
     
      <View style={{height:height(5),marginTop:height(2),alignItems:"center"}}>
       <TouchableOpacity style={{width:width(35),height:height(3),alignItems:"center",justifyContent:"center",backgroundColor:"#0354F1",borderRadius:12}} onPress={() => this.props.navigation.navigate("PaymentScreen")}>
       <Text style={{fontSize:16,fontFamily:"Ubuntu-Regular",color:"#fff"}}>
      Check Again
      </Text>
       </TouchableOpacity>
       </View>
      </View>
 </View>
      :
      <View style={{flexDirection: 'column',height:height(50),alignItems: 'center',justifyContent:"center"}}>
      <View style={{height:height(25),alignItems:"center",justifyContent:"center"}}>
      
      <View style={{width:width(70),height:height(23),alignItems:"center",justifyContent:"center"}}>
      <Text style={{fontSize:34,fontFamily:"Ubuntu-Bold",color:"#0e4cb0"}}>
      Get Full
      </Text>
      <Text style={{fontSize:34,fontFamily:"Ubuntu-Bold",color:"#0e4cb0"}}>
      Credit Report
      </Text>
      </View>
     
      <View style={{height:height(5),alignItems:"center"}}>
       <TouchableOpacity style={{width:width(35),height:height(3),alignItems:"center",justifyContent:"center",backgroundColor:"#0354F1",borderRadius:12}}>
       <Text style={{fontSize:16,fontFamily:"Ubuntu-Regular",color:"#fff"}}>
      Buy Now
      </Text>
       </TouchableOpacity>
       </View>
      </View>
 </View>
  }
   

    </ProgressCircle>
    
    <View style={{width:width(10),height:height(3),flexDirection: 'row',alignItems: 'center',justifyContent:"space-between"}}>
            <Image source={this.state.index === 0 ? require("../image/selectedicon.png") : require("../image/unselectedicon.png")} width={height(2)} height={height(2)} resizeMode="contain"/>
            <Image source={this.state.index === 1 ? require("../image/selectedicon.png") : require("../image/unselectedicon.png")} width={height(2)} height={height(2)} resizeMode="contain"/>
            <Image source={this.state.index === 2 ? require("../image/selectedicon.png") : require("../image/unselectedicon.png")} width={height(2)} height={height(2)} resizeMode="contain"/>
            
    </View>
    {this.state.index !== 1
      ?
    <View style={{width:width(90),alignItems:"flex-end",justifyContent:"flex-end",marginTop:-height(6)}}>
      
    <Text style={{fontSize:12,fontFamily:"Ubuntu-Regular"}}>
       Provided By
       </Text>
    <Image source={require("../image/aecb.png")} resizeMode="contain" style={{width:width(25),height:height(5)}}/>
    </View>
    :
    <View/>
  }
    </View>
    <View style={{width:width(99),alignItems:"center",justifyContent:"center"}}>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
         {items.map((item,idx) =>{
           return(
             this.getRewardCards(item,idx)
           )
         })}
      </ScrollView>
    </View>
    {/* <View style={{height:height(25),width:width(98),flexDirection: 'row',justifyContent:"space-between"}}>
       <TouchableOpacity style={{width:width(32),height:width(30),flexDirection: 'column',alignItems: 'center',justifyContent:"center",
       shadowOpacity: 
       0.75,
       shadowRadius: 5,
       elevation:8,
       shadowColor: '#707070',
       backgroundColor:"#fff",
       shadowOffset: { height: 0, width: 0 }}}
       onPress={this.onReportsPressed.bind(this)}>
       <Image source={require("../image/reporticon.png")} style={{width:width(15),height:width(15),alignItems: 'center',justifyContent:"center"}}  resizeMode="contain"/>
       <View style={{width:width(30),height:width(10),alignItems:"center",justifyContent:"center"}}>
       <Text style={{fontSize:14,fontFamily:"Ubuntu-Regular"}}>
       See your reports
       </Text>
       </View>
       
       </TouchableOpacity>

       <TouchableOpacity style={{width:width(32),height:width(30),flexDirection: 'column',alignItems: 'center',justifyContent:"center",
       shadowOpacity: 0.75,
       shadowRadius: 5,
       elevation:8,
       shadowColor: '#707070',
       backgroundColor:"#fff",
       shadowOffset: { height: 0, width: 0 }}}>
       <Image source={require("../image/offericon.png")}  style={{width:width(15),height:width(15),alignItems: 'center',justifyContent:"center"}}  resizeMode="contain"/>
       <View style={{width:width(30),height:width(10),alignItems:"center",justifyContent:"center"}}>
       <Text style={{fontSize:14,fontFamily:"Ubuntu-Regular"}}>
       See your Offers
       </Text>
       </View>
       
       </TouchableOpacity>

       <TouchableOpacity style={{width:width(32),height:width(30),flexDirection: 'column',alignItems: 'center',justifyContent:"center",
       shadowOpacity: 0.75,
       shadowRadius: 5,
       elevation:8,
       shadowColor: '#707070',
       backgroundColor:"#fff",
       shadowOffset: { height: 0, width: 0 }}}>
       <ImageBackground source={require("../image/scoreicon.png")} style={{width:width(15),height:width(15),alignItems: 'center',justifyContent:"center"}}  resizeMode="contain">
        <Text>{'100%'}</Text>
       </ImageBackground>
       <View style={{width:width(32),height:width(12),alignItems:"center",justifyContent:"center"}}>
       <Text style={{fontSize:14,fontFamily:"Ubuntu-Regular"}}>
       Improve Your Scores
       </Text>
       </View>
       
       </TouchableOpacity>
    </View>
     */}
    </View>
    <View style={{width:width(100),height:height(10),flexDirection: 'row',shadowOpacity: 0.75,borderTopLeftRadius: 25,
      borderTopRightRadius:25,
      shadowRadius: 15,
      elevation:8,
      shadowColor: '#707070',
      backgroundColor:"#fff",
      shadowOffset: { height: 10, width: 10 }}}>
        <TouchableOpacity style={{width:width(20),height:height(10),alignItems: 'center',borderTopLeftRadius: 25}} onPress={() => this.props.navigation.navigate("Home")}>
        <Image  source={require("../image/moneymallbottom.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Home
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyMoneyPressed.bind(this)}>
        <Image  source={require("../image/mymoneybottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
             My Cards
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onCreditScoreClicked.bind(this)}>
        <Image  source={require("../image/creditbottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <View style={{width:width(20),alignItems:"center"}}>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Credit
        </Text>
        </View>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}}>
        <Image  source={require("../image/mygoalbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Goals
        </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyRewardsClicked.bind(this)}>
        <Image  source={require("../image/myrewardbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Magnates
        </Text>
        </TouchableOpacity>
      
      </View>
           </View>

    );
  }
}
const styles = StyleSheet.create({ 
    container:{
        flex:1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center'
    },
})