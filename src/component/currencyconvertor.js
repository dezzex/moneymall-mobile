import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    Picker,
    ActivityIndicator,
    AsyncStorage
} from 'react-native'; 
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import CountryPicker from 'react-native-country-picker-modal'
const axios = require('axios');

import  worldlist from './world.json';
import { TextInput } from 'react-native-gesture-handler';
import { Converter } from 'currency-converter-for-all';
// import CountryPicker from 'react-native-country-picker-modal'

const fromCountry =  {
      "name": "United Arab Emirates",
      "native": "United Arab Emirates",
      "phone": "971",
      "continent": "UAE",
      "capital": "Abu Dhabi",
      "currency": "AED",
      "languages": [
        "arabic"
      ],
      "emoji": "🇦🇪",
      "emojiU": "U+1F1EC U+1F1E7"
    }
const toCountry =  {
    "name": "India",
    "native": "भारत",
    "phone": "91",
    "continent": "AS",
    "capital": "New Delhi",
    "currency": "INR",
    "languages": [
      "hi",
      "en"
    ],
    "emoji": "🇮🇳",
    "emojiU": "U+1F1EE U+1F1F3"
      }

  

export default class SwiftCode extends Component {
    constructor(props){
        super(props)
        this.state={
            fromCountry:  fromCountry,
            toCountry:toCountry,
            searchResult:'',
            amount:'1',
            result:''
            
        }
    }  
    onCheckPressed(){
        this.props.navigation.navigate("CreditScore")
      }
      onCreditScoreClicked(){
        this.props.navigation.navigate("CreditScore")
      }
      onMyRewardsClicked(){
        this.props.navigation.navigate("Rewards")
      }
      onMyMoneyPressed(){
        this.props.navigation.navigate("MyMoney")
      }
      onAmountChanged(value){
          this.setState({amount:value})
          
          if(this.state.fromCountry !== "" && this.state.toCountry !== ""){
              console.log('====================================');
              console.log(`https://free.currconv.com/api/v7/convert?q=${this.state.fromCountry.currency}_${this.state.toCountry.currency}&compact=ultra&apiKey=2702adf993c8691b4693`);
              console.log('====================================');
            axios.get(`https://free.currconv.com/api/v7/convert?q=${this.state.fromCountry.currency}_${this.state.toCountry.currency}&compact=ultra&apiKey=2702adf993c8691b4693`).then((response) =>{
            
                var rate = parseFloat(response.data[Object.keys(response.data)[0]]);  
            this.setState({result:rate * parseInt(value)})
            })
          }
   
      }

      componentDidMount(){
        axios.get(`http://data.fixer.io/api/latest?access_key=a59706039559caaab0443fe064a798a3&base=USD&symbol=GBP`).then((response) =>{
            
           console.log('====================================');
           console.log("cccccccccc",this.state.fromCountry,this.state.toCountry);
           console.log('====================================');
                if(this.state.fromCountry !== "" && this.state.toCountry !== ""){
          axios.get(`https://free.currconv.com/api/v7/convert?q=${this.state.fromCountry.currency}_${this.state.toCountry.currency}&compact=ultra&apiKey=2702adf993c8691b4693`).then((response) =>{
          
              var rate = parseFloat(response.data[Object.keys(response.data)[0]]);  
              console.log("Rateeeeeee",rate);
              
          this.setState({result:rate * parseInt(this.state.amount)})
          })
        }
        })
      }

    onFromCountryChanged(itemValue){
       
        
       this.setState({fromCountry: itemValue})
       if( this.state.toCountry !== ""){
        
      axios.get(`https://free.currconv.com/api/v7/convert?q=${itemValue.currency}_${this.state.toCountry.currency}&compact=ultra&apiKey=2702adf993c8691b4693`).then((response) =>{
      
          var rate = parseFloat(response.data[Object.keys(response.data)[0]]);  
          console.log(this.state.fromCountry.currency);
          
      this.setState({result:rate * parseInt(this.state.amount)})
      })
    }
    }
    onToCountryChanged(itemValue){
        
        console.log(itemValue);
        this.setState({toCountry: itemValue})
        if(this.state.fromCountry !== ""){
            
          axios.get(`https://free.currconv.com/api/v7/convert?q=${this.state.fromCountry.currency}_${itemValue.currency}&compact=ultra&apiKey=2702adf993c8691b4693`).then((response) =>{
          
              var rate = parseFloat(response.data[Object.keys(response.data)[0]]);  
          this.setState({result:rate * parseInt(this.state.amount)})
          })
        }
     }
     static navigationOptions = ({ navigation  }) => {
        return{
            headerRight:null,
           
           headerTitle:
        
        <TouchableOpacity style={{width:width(80),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
        <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",width:height(6),width:height(6),borderRadius: height(6)/2}}>
            <Image source={require("../image/rateicon.png")}  width={width(40)} height={height(10)} resizeMode="contain"/>
        </View>
        <View style={{marginLeft:width(2)}}>
        <Text style={{fontSize: 18,
    fontFamily:"Ubuntu-Bold"}}>
          Currency Rate
        </Text>
        </View>
        </TouchableOpacity>,
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
        <Image style={{marginLeft:width(3)}} source={require("../image/headerback.png")} width={width(7)} height={height(6)} resizeMode="contain"/>
        </TouchableOpacity>,
        headerStyle:{
          borderBottomWidth:0,
          height:height(15),
          elevation: 0,
          shadowOpacity: 0
        }}
      };
    render() {
        var branchIdx = -1;
        return (
            <View style={styles.container}>
                <View style={{height:height(73)}}>
                <View style={{width:width(90)}}>
                    <Text style={{fontSize:20,fontWeight:"bold"}}>
                        Amount
                    </Text>
               </View>
               {/* <View style={{height:height(8),width:width(88),marginTop:height(3),marginBottom:height(3), shadowOffset: { width: 10, height: 10 },
                    shadowColor: 'black',
                    
                    alignItems:"center",
                    justifyContent:"center",
                    shadowOpacity: 1,
                    elevation: 8,
                    borderRadius:12,
                    backgroundColor : "#f7faff"}}> */}
                     <View  style={{height:height(8),width:width(88),marginTop:height(3),marginBottom:height(3),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                        <TextInput style={{width:width(88),height:height(6)}} 
                        onChangeText={this.onAmountChanged.bind(this)}
                        value={this.state.amount}
                        placeholder="Enter Amount to Convert"/>
                </View>
                <View style={{width:width(90)}}>
                    <Text style={{fontSize:20,fontWeight:"bold"}}>
                        From
                    </Text>
               </View>
               {/* <View style={{height:height(8),width:width(88),marginTop:height(3),marginBottom:height(3), shadowOffset: { width: 10, height: 10 },
                    shadowColor: 'black',
                    
                    alignItems:"center",
                    justifyContent:"center",
                    shadowOpacity: 1,
                    elevation: 8,
                    borderRadius:12,
                    backgroundColor : "#f7faff"}}> */}
                      <View  style={{height:height(8),width:width(88),marginTop:height(3),marginBottom:height(3),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                           <Picker
                    style={{height:height(6),width:width(88)}}
                    selectedValue={this.state.fromCountry}
                    
                    onValueChange={this.onFromCountryChanged.bind(this)}
                    >
                           <Picker.Item label={fromCountry.emoji+"  "+fromCountry.currency+"      "+fromCountry.name} value={fromCountry} />
                           
                        {Object.keys(worldlist).map((key,idx)=>{
                            return(
                             <Picker.Item label={worldlist[key].emoji+"  "+worldlist[key].currency+"      "+worldlist[key].name} value={worldlist[key]} key={idx} />
                            )
                        }
                        )}
                    </Picker>  
  
                 </View>

                 <View style={{width:width(90)}}>
                    <Text style={{fontSize:20,fontWeight:"bold"}}>
                        To
                    </Text>
               </View>
               {/* <View style={{height:height(8),width:width(88),marginTop:height(3),marginBottom:height(3), shadowOffset: { width: 10, height: 10 },
                    shadowColor: 'black',
                    
                    alignItems:"center",
                    justifyContent:"center",
                    shadowOpacity: 1,
                    elevation: 8,
                    borderRadius:12,
                    backgroundColor : "#f7faff"}}> */}
                     <View  style={{height:height(8),width:width(88),marginTop:height(3),marginBottom:height(3),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                           <Picker
                    style={{height:height(6),width:width(88)}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onToCountryChanged.bind(this)}>
                               <Picker.Item label={toCountry.emoji+"  "+toCountry.currency+"      "+toCountry.name} value={toCountry} />
                    
                        {Object.keys(worldlist).map((key,idx)=>{
                            return(
                             <Picker.Item label={worldlist[key].emoji+"  "+worldlist[key].currency+"      "+worldlist[key].name} value={worldlist[key]} key={idx} />
                            )
                        }
                        )}
                    </Picker>  
  
                 </View>
                 <View style={{height:height(10),alignItems:"center",justifyContent:"center"}}>
                    {this.state.amount !== "" && this.state.result !== "" && this.state.fromCountry !== "" && this.state.toCountry !== ""
                    ?
                    <View>
                        <View style={{height:height(4),marginBottom:height(2),width:width(90),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:16}}>
                   {this.state.amount}  {this.state.fromCountry.currency} =  
                </Text>
                </View>
                <View style={{height:height(4),width:width(90),alignItems:"center",justifyContent:"center"}}>
                <Text  style={{fontSize:24,fontWeight:"bold",color:"#035AF1"}}>
                {this.state.result.toFixed(2)} {this.state.toCountry.currency}
                </Text>
                </View>
                </View>
                :
                <View/>
                    }
                </View>
                </View>

                <View style={{width:width(100),height:height(10),flexDirection: 'row',shadowOpacity: 0.75,borderTopLeftRadius: 25,
      borderTopRightRadius:25,
      shadowRadius: 15,
      elevation:8,
      shadowColor: '#707070',
      backgroundColor:"#fff",
      shadowOffset: { height: 10, width: 10 }}}>
        <TouchableOpacity style={{width:width(20),height:height(10),alignItems: 'center',borderTopLeftRadius: 25}} onPress={() => this.props.navigation.navigate("Home")}>
        <Image  source={require("../image/moneymallbottom.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Home
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyMoneyPressed.bind(this)}>
        <Image  source={require("../image/mymoneybottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
             My Cards
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onCreditScoreClicked.bind(this)}>
        <Image  source={require("../image/creditbottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <View style={{width:width(20),alignItems:"center"}}>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Credit
        </Text>
        </View>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}}>
        <Image  source={require("../image/mygoalbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Goals
        </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyRewardsClicked.bind(this)}>
        <Image  source={require("../image/myrewardbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Magnates
        </Text>
        </TouchableOpacity>
      
      </View>
         </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        height:height(90),
    },
});
