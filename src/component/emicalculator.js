import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import Slider from '@react-native-community/slider';

import {width,height} from 'react-native-dimension';
import NumberFormat from 'react-number-format';
export default class EmiCalculator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loanamount:10000,
            tenure:4,
            rate:8
        };
    }

    static navigationOptions = ({ navigation  }) => {
        return{
            headerRight:null,
           
           headerTitle:
        
        <TouchableOpacity style={{width:width(80),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
        <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",width:height(6),width:height(6),borderRadius: height(6)/2}}>
            <Image source={require("../image/calculatoricon.png")}  style={{width:width(40),height:height(10)}} resizeMode="contain"/>
        </View>
        <View style={{marginLeft:width(2)}}>
        <Text style={{fontSize: 18,
    fontFamily:"Ubuntu-Bold"}}>
          EMI Calculator
        </Text>
        </View>
        </TouchableOpacity>,
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
        <Image style={{marginLeft:width(3)}} source={require("../image/headerback.png")} width={width(7)} height={height(6)} resizeMode="contain"/>
        </TouchableOpacity>,
        headerStyle:{
          borderBottomWidth:0,
          height:height(15),
          elevation: 0,
          shadowOpacity: 0
        }}
      };
    
    componentWillReceiveProps(nextProps) {
        
    }
    onCheckPressed(){
        this.props.navigation.navigate("CreditScore")
      }
      onCreditScoreClicked(){
        this.props.navigation.navigate("CreditScore")
      }
      onMyRewardsClicked(){
        this.props.navigation.navigate("Rewards")
      }
      onMyMoneyPressed(){
        this.props.navigation.navigate("MyMoney")
      }
    render() {
        return (
            <View style={styles.container}>
            <View style={{width:width(100),height:height(10),marginTop:height(4),alignItems:"center",justifyContent:"center"}}>
            <View style={{flexDirection:"row",width:width(90),justifyContent:"center",alignItems:"center"}}>
                <View style={{width:width(25)}}>
                    <Text style={{fontSize:16,
    fontFamily:"Ubuntu-Bold"}}>
                        Loan Amount
                    </Text>
                </View>
                <View style={{width:width(55),alignItems:"flex-end",justifyContent:"flex-end",marginLeft:width(2)}}>
                     <NumberFormat
    value={this.state.loanamount}
    displayType={'text'}
    thousandSeparator={true}
    prefix={'AED '}
    renderText={value => <Text style={{fontSize:20,
        fontFamily:"Ubuntu-Bold"}}>{value}</Text>} 
/> 
                </View>
            </View>
          
            <View style={{width:width(90),height:height(3),justifyContent:"center",alignItems:"center"}}>
                <Slider
                    style={{width: width(90), height: height(6),transform: [{ scaleX: 1 }, { scaleY: 1 }]}}
                    minimumValue={10000}
                    
                    maximumValue={1000000}
                    value={1000}
                    step={1000}
                    minimumTrackTintColor="#4A90E2"
                    maximumTrackTintColor="#000000"
                    thumbTintColor='#4A90E2'
                    onSlidingComplete={value =>
                      this.setState({
                        loanamount: value,
                      })
                    }
                  />
                </View>
                <View style={{height:height(4),flexDirection:"row"}}>
                    <View style={{width:width(65)}}>
                        
                    <NumberFormat
    value={10000}
    displayType={'text'}
    thousandSeparator={true}
    prefix={'AED '}
    renderText={value => <Text style={{fontSize:11,
        fontFamily:"Ubuntu-Regular"}}>{value}</Text>} 
/> 
                    </View>
                    <View>
                    <NumberFormat
    value={1000000}
    displayType={'text'}
    thousandSeparator={true}
    prefix={'AED '}
    renderText={value => <Text style={{fontSize:11,
        fontFamily:"Ubuntu-Regular"}}>{value}</Text>} 
/> 
                    </View>
                </View>
                </View>

                <View style={{width:width(100),height:height(15),alignItems:"center",justifyContent:"center"}}>
            <View style={{flexDirection:"row",width:width(90),justifyContent:"center",alignItems:"center"}}>
                <View style={{width:width(75)}}>
                    <Text style={{fontSize:16,
    fontFamily:"Ubuntu-Bold"}}>
                        Repayment Tenor (Years)
                    </Text>
                </View>
                <View style={{fontSize:16,width:width(10),marginLeft:width(2),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:20,
    fontFamily:"Ubuntu-Bold"}}>
                         {this.state.tenure}
                    </Text>
                </View>
            </View>
          
            <View style={{width:width(90),height:height(3),justifyContent:"center",alignItems:"center"}}>
                <Slider
                    style={{width: width(90), height: height(6),transform: [{ scaleX: 1 }, { scaleY: 1 }]}}
                    minimumValue={0}
                    
                    maximumValue={30}
                    value={4}
                    step={1}
                    minimumTrackTintColor="#4A90E2"
                    maximumTrackTintColor="#000000"
                    thumbTintColor='#4A90E2'
                    onSlidingComplete={value =>
                      this.setState({
                        tenure: value,
                      })
                    }
                  />
                </View>
                <View style={{height:height(4),width:width(90),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(75)}}>
                        <Text style={{fontSize:11,fontFamily:"OpenSans-Regular"}}>
                            0
                        </Text>
                    </View>
                    <View>
                        <Text style={{fontSize:11,marginLeft:width(4),fontFamily:"OpenSans-Regular"}}>
                            30
                        </Text>
                    </View>
                </View>
                </View>

                <View style={{width:width(100),height:height(15),alignItems:"center",justifyContent:"center"}}>
            <View style={{flexDirection:"row",width:width(90),justifyContent:"center",alignItems:"center"}}>
                <View style={{width:width(70),paddingLeft:width(5)}}>
                    <Text style={{fontFamily:"Ubuntu-Bold",fontSize:16}}>
                        Interest Rate
                    </Text>
                </View>
                <View style={{width:width(25),alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:20,
    fontFamily:"Ubuntu-Bold"}}>
                         {this.state.rate} %
                    </Text>
                </View>
            </View>
          
            <View style={{width:width(90),height:height(3),justifyContent:"center",alignItems:"center"}}>
                <Slider
                    style={{width: width(90), height: height(6),transform: [{ scaleX: 1 }, { scaleY: 1 }]}}
                    minimumValue={0}
                    
                    maximumValue={25}
                    value={8.50}
                    step={0.25}
                    minimumTrackTintColor="#4A90E2"
                    maximumTrackTintColor="#000000"
                    thumbTintColor='#4A90E2'
                    onSlidingComplete={value =>
                      this.setState({
                        rate: value,
                      })
                    }
                  />
                </View>
                <View style={{height:height(4),width:width(90),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(75)}}>
                        <Text style={{fontSize:11}}>
                            0%
                        </Text>
                    </View>
                    <View>
                        <Text style={{fontSize:11,marginLeft:width(4)}}>
                            25%
                        </Text>
                    </View>
                </View>
                </View>
            
            <View>
                <View style={{width:width(95),height:height(10),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                        <View style={{width:width(80),alignItems:"center",justifyContent:"center"}}>
                            <Text style={{fontSize:20,
    fontFamily:"Ubuntu-Bold"}}>
                                Monthly Instalment ( EMI )
                            </Text>
                            <NumberFormat
                            value={(((this.state.loanamount*this.state.tenure*this.state.rate)/100 + this.state.loanamount) / this.state.tenure.toFixed(2)).toFixed(2)}
                            displayType={'text'}
                            thousandSeparator={true}
                            prefix={'AED '}
                            renderText={value => <Text style={{fontSize:20,
                                fontFamily:"Ubuntu-Bold",color:"#035AF1"}}>{value}</Text>} 
                        /> 
                            
                        </View>
                      
                </View>
                <View style={{width:width(90),height:height(10),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                <View style={{width:width(40),alignItems:"center",justifyContent:"center"}}>
                            <Text style={{fontSize:16,
    fontFamily:"Ubuntu-Bold"}}>
                               Interest Amount
                            </Text>
                            <NumberFormat
                            value={((this.state.loanamount*this.state.tenure*this.state.rate)/100).toFixed(2)}
                            displayType={'text'}
                            thousandSeparator={true}
                            prefix={'AED '}
                            renderText={value => <Text style={{color:"#035AF1",fontSize:16}}>{value}</Text>} 
                        /> 
                            {/* <Text style={{color:"#035AF1",fontSize:16}}>
                                 {(this.state.loanamount*this.state.tenure*this.state.rate)/100}
                            </Text> */}
                            
                        </View>
                        <View style={{width:width(40),alignItems:"center",justifyContent:"center"}}>
                        
                            <Text style={{fontSize:16,
    fontFamily:"Ubuntu-Bold"}}>
                                 Principal Amount
                            </Text>
                            <NumberFormat
                            value={this.state.loanamount}
                            displayType={'text'}
                            thousandSeparator={true}
                            prefix={'AED '}
                            renderText={value => <Text style={{color:"#035AF1",fontSize:16}}>{value}</Text>} 
                        /> 
                            {/* <Text style={{color:"#035AF1",fontSize:16}}>
                                AED {this.state.loanamount}
                            </Text> */}
                            </View>
                </View>

                <View style={{width:width(90),height:height(10),alignItems:"center",justifyContent:"center"}}>
                            <Text style={{fontSize:16,
    fontFamily:"Ubuntu-Bold"}}>
                                Total Amount Payable
                            </Text>
                            <NumberFormat
                            value={((this.state.loanamount*this.state.tenure*this.state.rate)/100 + this.state.loanamount).toFixed(2)}
                            displayType={'text'}
                            thousandSeparator={true}
                            prefix={'AED '}
                            renderText={value => <Text style={{color:"#035AF1",fontSize:16}}>{value}</Text>} 
                        /> 
                            {/* <Text style={{color:"#035AF1",fontSize:16}}>
                                 {(this.state.loanamount*this.state.tenure*this.state.rate)/100 + this.state.loanamount}
                            </Text> */}
                        </View>

            </View>
            <View style={{width:width(100),height:height(10),flexDirection: 'row',shadowOpacity: 0.75,borderTopLeftRadius: 25,
      borderTopRightRadius:25,
      shadowRadius: 15,
      elevation:8,
      shadowColor: '#707070',
      backgroundColor:"#fff",
      shadowOffset: { height: 10, width: 10 }}}>
        <TouchableOpacity style={{width:width(20),height:height(10),alignItems: 'center',borderTopLeftRadius: 25}} onPress={() => this.props.navigation.navigate("Home")}>
        <Image  source={require("../image/moneymallbottom.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Home
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyMoneyPressed.bind(this)}>
        <Image  source={require("../image/mymoneybottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
             My Cards
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onCreditScoreClicked.bind(this)}>
        <Image  source={require("../image/creditbottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <View style={{width:width(20),alignItems:"center"}}>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Credit
        </Text>
        </View>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}}>
        <Image  source={require("../image/mygoalbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Goals
        </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyRewardsClicked.bind(this)}>
        <Image  source={require("../image/myrewardbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Magnates
        </Text>
        </TouchableOpacity>
      
      </View>
        </View>
        );
    }
    
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
