import React, { Component ,createRef} from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,CheckBox,Picker,PickerIOS} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import CodeInput from 'react-native-confirmation-code-field';

export default class emiratesidsample extends Component {
    constructor(props) {
        super(props);
        this.state = {    
            frontdata:'',
            backdata:''
        };
      }
      static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
           <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       
        </TouchableOpacity>
       ,
        headerStyle:{
          height:height(8),
        }
        }
      };   
    onFrontPressed(){

    }
    onNextClicked(){
        this.props.navigation.navigate("UploadFile")
    }


  render() {
    return (
      <View style={{height:height(90),alignItems:"center"}}>
          <View style={{width:width(85)}}>
        <Text style={{fontSize:18}}> Emirates ID scan </Text>
        </View>
        <View style={{height:height(60),width:width(99)}}>
                <TouchableOpacity style={{width:width(99),height:height(30)}} onPress={this.onFrontPressed.bind(this)}>
                    {this.state.frontdata !== undefined && this.state.frontdata.length > 0
                    ?
                    <Image source={{uri:this.state.frontdata}} resizeMode="cover" style={{width:width(95),height:height(27)}}/>
                    :
                    <Image source={require("../image/eidfrontsample.png")} resizeMode="cover" style={{width:width(95),height:height(27)}}/>
                    }
                </TouchableOpacity>
                <TouchableOpacity style={{width:width(99),height:height(30)}}>
                {this.state.backdata !== undefined && this.state.backdata.length > 0
                    ?
                    <Image source={{uri:this.state.backdata}} resizeMode="cover" style={{width:width(95),height:height(27)}}/>
                    :
                    <Image source={require("../image/eidbacksample.png")} resizeMode="cover" style={{width:width(95),height:height(27)}}/>
                    }
                </TouchableOpacity>
                </View>

            <View style={{width:width(65)}}>
                    <Text style={{fontSize:16}}>Name:</Text>
            </View>
            <View style={{height:height(10),width:width(55),alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:18}}>
                    Sucessfully verified
                    </Text>
            </View>
            <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6),marginTop:10}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18}}>
                        Apply
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
      </View>
    );
  }
}
