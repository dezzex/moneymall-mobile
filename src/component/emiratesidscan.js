import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,Alert,Picker,PermissionsAndroid, ToastAndroid, ImageBackground} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import RNFetchBlob from 'react-native-fetch-blob';
var RNGRP = require('react-native-get-real-path');
export default class emiratesidscan extends Component {

    constructor(props){
        super(props)
        this.state={
            frontdata:'',
            backdata:'',
            salarydoc:'',
            frontpassport:'',
            backpassport:''
        }
    }
    static navigationOptions = ({ navigation  }) => {
      return{
      headerRight:null,
      
      headerLeft:
      <TouchableOpacity onPress={() => navigation.navigate("Home")}>
         <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
     
      </TouchableOpacity>
     ,
      headerStyle:{
        height:height(8),
      }
      }
    };
    openCamera(index){
      console.log('You can use the Camera');
      var self = this;
            const options = {
              title: 'Select File',
              // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
              storageOptions: {
                cameraRoll: true,
                skipBackup: true,
                path: 'images',
              },
            };
            // this.setState({ isUploadDocModal: true })
            ImagePicker.launchCamera(options, async (response) => {
              console.log('Response = ', response);
              if(!response.didCancel){
              if(response.data.length > 0){
                if(index === 0)
                  this.setState({frontdata:`data:image/gif;base64,${response.data}`})
                  else if(index  === 1)
                  self.setState({backdata:`data:image/gif;base64,${response.data}`}) 
          else if(index === 2)
                  self.setState({frontpassport:`data:image/gif;base64,${response.data}`})
            else if(index  === 3)
                    self.setState({backpassport:`data:image/gif;base64,${response.data}`})
                 else if(index  === 4)
                self.setState({salarydoc:`data:image/gif;base64,${response.data}`})
              }
            }
            else{
              ToastAndroid.show("Uploading Canceled....",ToastAndroid.SHORT);
            }
              })
    }
      onNextClicked(){
        this.props.navigation.navigate("BankScan")
      }
      onHomePressed(){
        this.props.navigation.navigate("Home")  
      }
      async  requestCameraPermission(index) {
        
        try {
          const granted = await  PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: 'Lisec Doccenter App Camera Permission',
              message:
                'Lisec Doccenter App needs access to your Camera ',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            this.openCamera(index);
          } else {
            console.log('Camera permission denied');
          }
        } catch (err) {
          console.warn(err);
        }
      }
      displayFromGallery = async (path,index) => {
        var self = this;
        try {
          const granted = await  PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            {
              title: 'Lisec Doccenter App Camera Permission',
              message:
                'Lisec Doccenter App needs access to your Camera ',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            // this.openCamera();
            const fs = RNFetchBlob.fs;
                fs.readFile(path, 'base64').then(imageBase64 =>{
                  if(index === 0)
                        self.setState({frontdata:`data:image/gif;base64,${imageBase64}`})
                  else if(index  === 1)
                          self.setState({backdata:`data:image/gif;base64,${imageBase64}`}) 
                  else if(index === 2)
                          self.setState({frontpassport:`data:image/gif;base64,${imageBase64}`})
                    else if(index  === 3)
                            self.setState({backpassport:`data:image/gif;base64,${imageBase64}`})
                         else if(index  === 4)
                        self.setState({salarydoc:`data:image/gif;base64,${imageBase64}`})
                    }
                      
                        )
          } else {
            console.log('Camera permission denied');
          }
        } catch (err) {
          console.warn(err);
        }
      }
      onFrontPressed = async (index) => {


        Alert.alert(
          'Money Mall Chooser',
          'Choose the way you want to upload document',
          [
            {text: 'Cancel', onPress: () => console.log('Ask me later pressed')},
            {
              text: 'Camera',
              onPress: () => this.requestCameraPermission(index),
              style: 'cancel',
            },
            {text: 'Gallery',
             onPress: () => {
              DocumentPicker.show({
                filetype: [DocumentPickerUtil.allFiles()],
              }, (error, response) => {
                if (error) {
                  this.setState({ isUploadDocModal: false })
                  console.log('FILE Picker Error: ', error);
                }
                if (response) {
                  if(response.fileName !== ""){
                   
                    RNGRP.getRealPathFromURI(response.uri).then(path =>{
                      this.displayFromGallery(path,index);
                    }
                  
                    )
                  }
                }
              })
             }},
          ],
          {cancelable: false},
        );
      }
    render() { 
        return (
          <View style={{flex:1,alignItems:"center"}}>
            <View style={{width:width(90),flexDirection:"row",height:height(10),marginTop:height(2),justifyContent:"center",borderBottomColor:"#1972D7",borderBottomWidth:1}}>
                <View style={{width:width(15),height:height(10),alignItems:"center",justifyContent:"center"}}>
                <Image  source={require("../image/check.png")} style={{width:width(12),height:height(6)}} resizeMode="contain"/>
                       
                </View>
                
                <View style={{width:width(60),height:height(12),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold",marginBottom:height(1)}}>You Have succesfully completed</Text>
                </View>
                </View>


          <View style={{width:width(90),height:height(8),justifyContent:"center",alignItems:"center"}}>
          <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Would you like to upload supporting sdocuments to speedup your process ?</Text>
       
          </View>
          
          <View style={{height:height(55),alignItems:"center",justifyContent:"center"}}>
          <View style={{width:width(90),alignItems:"flex-start"}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Emirates Id</Text>
          </View>
          <View style={{width:width(90),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
      
              <TouchableOpacity style={{height:height(15),width:width(45),alignItems:"center",justifyContent:"center"}} onPress={this.onFrontPressed.bind(this,0)}>
              {this.state.frontdata !== undefined && this.state.frontdata.length > 0
                ?
                  <Image source={{uri:this.state.frontdata}} resizeMode="contain" style={{width:width(40),height:height(15)}}/>
                :
                  <ImageBackground source={require("../image/eidfront.png")} blurRadius={1} style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:20,fontFamily:"Ubuntu-Bold"}}>Front</Text>
                  </ImageBackground>
              }
              </TouchableOpacity>
              <TouchableOpacity style={{height:height(15),width:width(45),alignItems:"center",justifyContent:"center"}} onPress={this.onFrontPressed.bind(this,1)}>
              {this.state.backdata !== undefined && this.state.backdata.length > 0
                ?
                  <Image source={{uri:this.state.backdata}} resizeMode="contain" style={{width:width(40),height:height(15)}}/>
                :
                <ImageBackground source={require("../image/eidback.png")} blurRadius={1} style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:20,fontFamily:"Ubuntu-Bold"}}>Back</Text>
                </ImageBackground>
              }
              </TouchableOpacity>
          </View>
           <View style={{width:width(90),alignItems:"flex-start"}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>PassPort</Text>
          </View>
          <View style={{height:height(15),width:width(90),alignItems:"center",justifyContent:"center",flexDirection:"row"}} >
      
              <TouchableOpacity style={{height:height(15),width:width(45),alignItems:"center",justifyContent:"center"}} onPress={this.onFrontPressed.bind(this,2)}>
              {this.state.frontpassport !== undefined && this.state.frontpassport.length > 0
                ?
                  <Image source={{uri:this.state.frontpassport}} resizeMode="contain" style={{width:width(40),height:height(15)}}/>
                :
                <ImageBackground source={require("../image/eidfront.png")} blurRadius={1} style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:20,fontFamily:"Ubuntu-Bold"}}>Front</Text>
              </ImageBackground>  
              }
              </TouchableOpacity>
              <TouchableOpacity style={{height:height(12),width:width(45),alignItems:"center",justifyContent:"center"}} onPress={this.onFrontPressed.bind(this,3)}>
              {this.state.backpassport !== undefined && this.state.backpassport.length > 0
                ?
                  <Image source={{uri:this.state.backpassport}} resizeMode="contain" style={{width:width(40),height:height(15)}}/>
                :
                <ImageBackground source={require("../image/eidback.png")} blurRadius={1} style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:20,fontFamily:"Ubuntu-Bold"}}>Back</Text>
              </ImageBackground>
              }
              </TouchableOpacity>
          </View>
          <View style={{width:width(90),alignItems:"flex-start"}}>
                    <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>Salary Certificate</Text>
          </View>
          <View style={{height:height(15),width:width(90),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
      
              <TouchableOpacity style={{height:height(15),width:width(45),alignItems:"center",justifyContent:"center"}} onPress={this.onFrontPressed.bind(this,4)}>
              {this.state.salarydoc !== undefined && this.state.salarydoc.length > 0
                ?
                  <Image source={{uri:this.state.salarydoc}} resizeMode="contain" style={{width:width(70),height:height(15)}}/>
                :
                <ImageBackground source={require("../image/eidfront.png")} blurRadius={1} style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                
              </ImageBackground> 
             }
              </TouchableOpacity>
              {/* <TouchableOpacity style={{height:height(15),width:width(45),alignItems:"center",justifyContent:"center"}}>
              {this.state.frontdata !== undefined && this.state.frontdata.length > 0
                ?
                  <Image source={{uri:this.state.frontdata}} resizeMode="contain" style={{width:width(70),height:height(10)}}/>
                :
                  <Image source={require("../image/eidback.png")} resizeMode="contain" style={{width:width(70),height:height(15)}}/>
              }
              </TouchableOpacity> */}
          </View>
          </View>
          <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(4),alignItems:"center",justifyContent:"center"}}  onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                        Continue
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
                   <TouchableOpacity style={{width:width(90),height:height(4),marginTop:height(2),alignItems:"center",justifyContent:"center"}}  onPress={this.onHomePressed.bind(this)}>
                        <Text style={{color:"#0D9CF8",fontSize:14,fontFamily:"Ubuntu-Bold",textDecorationLine:"underline"}}>
                        I will do it Later - Send me Link
                      </Text>
                   </TouchableOpacity>
              
          </View>
        //     <View style={{flex:1,alignItems:"center"}}>
        //         <View style={{height:height(6),width:width(90)}}>
        //         <Text style={{fontSize:22}}> Emirates ID scan </Text>
        //         </View>
        //         <View style={{height:height(60),width:width(99)}}>
        //         <TouchableOpacity style={{width:width(99),height:height(30)}} onPress={this.onFrontPressed.bind(this)}>
        //             {this.state.frontdata !== undefined && this.state.frontdata.length > 0
        //             ?
        //             <Image source={{uri:this.state.frontdata}} resizeMode="cover" style={{width:width(95),height:height(27)}}/>
        //             :
        //             <Image source={require("../image/eidfront.png")} resizeMode="cover" style={{width:width(95),height:height(27)}}/>
        //             }
        //         </TouchableOpacity>
        //         <TouchableOpacity style={{width:width(99),height:height(30)}}>
        //         {this.state.backdata !== undefined && this.state.backdata.length > 0
        //             ?
        //             <Image source={{uri:this.state.backdata}} resizeMode="cover" style={{width:width(95),height:height(27)}}/>
        //             :
        //             <Image source={require("../image/eidback.png")} resizeMode="cover" style={{width:width(95),height:height(27)}}/>
        //             }
        //         </TouchableOpacity>
        //         </View>
        //         {this.state.frontdata !== undefined && this.state.backdata !== undefined && this.state.frontdata.length > 0 && this.state.backdata.length > 0
        //         ?
        //         <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6),marginTop:height(6)}}
        //          colors={["#0D9CF8","#0354F1"]} >
        //            <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
        //                 <Text style={{color:"#fff",fontSize:18}}>
        //                 Upload & Verfiy
        //               </Text>
        //            </TouchableOpacity>
              
        //       </LinearGradient>
        //       :
        //       <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center",backgroundColor:"#E6E6E6"}} onPress={this.onNextClicked.bind(this)}>
        //       <Text style={{color:"#fff",fontSize:18}}>
        //       Upload & Verfiy
        //     </Text>
            
        //  </TouchableOpacity>
    
        //         }
        //       <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6),marginTop:10}}
        //          colors={["#0D9CF8","#0354F1"]} >
        //            <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
        //                 <Text style={{color:"#fff",fontSize:18}}>
        //                 Next
        //               </Text>
        //            </TouchableOpacity>
              
        //       </LinearGradient>
        //     </View>
        )
    }
}
