import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

var faqList = [
    {title:"How to earn more points?",
     content:"As soon as your creditor reports your lower balance, the better utilization will be reflected in your scores. If you have a bunch of maxed-out credit cards, you could elevate your scores by nearly 100 points by paying them all off, says John Ulzheimer, a credit score expert who has worked at FICO and Equifax."},
     {title:"How to earn more points?",
     content:"As soon as your creditor reports your lower balance, the better utilization will be reflected in your scores. If you have a bunch of maxed-out credit cards, you could elevate your scores by nearly 100 points by paying them all off, says John Ulzheimer, a credit score expert who has worked at FICO and Equifax."},
     {title:"How to earn more points?",
     content:"As soon as your creditor reports your lower balance, the better utilization will be reflected in your scores. If you have a bunch of maxed-out credit cards, you could elevate your scores by nearly 100 points by paying them all off, says John Ulzheimer, a credit score expert who has worked at FICO and Equifax."},
     {title:"How to earn more points?",
     content:"As soon as your creditor reports your lower balance, the better utilization will be reflected in your scores. If you have a bunch of maxed-out credit cards, you could elevate your scores by nearly 100 points by paying them all off, says John Ulzheimer, a credit score expert who has worked at FICO and Equifax."},
     {title:"How to earn more points?",
     content:"As soon as your creditor reports your lower balance, the better utilization will be reflected in your scores. If you have a bunch of maxed-out credit cards, you could elevate your scores by nearly 100 points by paying them all off, says John Ulzheimer, a credit score expert who has worked at FICO and Equifax."},


    ]

export default class FaqScreen extends Component {
    static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
           <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       
        </TouchableOpacity>
       ,
        headerStyle:{
          height:height(8),
        }
        }
      };
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
          <View style={{width:width(75)}}>
          <Text style={{fontSize:24}}> Frequenty Asked Questions </Text>
          </View>
        <View style={{height:height(80),marginTop:height(5)}}>
        <ScrollView
            showsVerticalScrollIndicator={false}>
                {faqList.map((val,idx) => {
                    return(
                        <TouchableOpacity key={idx} style={{width:width(95),height:height(15),marginBottom:height(5),alignItems:"center",justifyContent:"center"}}>
                            <View style={{width:width(75),height:height(2)}}>
                            <Text style={{fontSize:14,color:"#035AF1"}}>
                                {val.title}
                            </Text>
                            </View>
                        
                            <View style={{marginTop:height(1),width:width(95),height:height(13),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                <Image source={require("../image/sideline.png")} style={{width:width(2),height:height(5),marginRight:width(5)}} resizeMode="contain"/>
                                <View style={{width:width(80),height:height(13),flexDirection:"row"}}>
                                <Text style={{flex: 1, flexWrap: 'wrap'}}>
                                    {val.content}
                                </Text>
                                </View>
                                
                            </View>
                        </TouchableOpacity>
                    )
                })
                }
            </ScrollView>
            </View>
      </View>
    );
  }
}
