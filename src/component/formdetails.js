//import liraries
import React, { Component } from 'react'
import { View, Text ,Image,TouchableOpacity,Keyboard,Picker,ActivityIndicator} from 'react-native';
import {width,height} from 'react-native-dimension';
import Slideshow from 'react-native-slideshow';
import LinearGradient from 'react-native-linear-gradient';
import CreditCard from './CreditCard';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Slider from '@react-native-community/slider';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { Header } from 'react-navigation-stack';

// create a component
class FormDetails extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            mobileno:''
        }
    }
    onMobileNoChanged(text){
        if(this.state.mobileno.length === 0){
          text = "+971"+text;
        }
        
        if(text.length === 13){
          
        console.log("Dssssss",text.length);
        
        Keyboard.dismiss();
          this.salaryinput.focus();
        }
        this.setState({mobileno:text})
      }
    onFindClicked(){
        console.log(this.state);
        
        var url = this.props.navigation.getParam('url', undefined);
        var index = this.props.navigation.getParam('index', undefined);
        var categorylist = this.props.navigation.getParam('categorylist', undefined);
        
        this.props.navigation.navigate("CreditCardList",{url:url,categorylist:categorylist,index:index})
  
      }
      static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
           <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       
        </TouchableOpacity>
       ,
        headerStyle:{
          height:height(8),
        }
        }
      };
    
    render() {
        var index = this.props.navigation.getParam('index', undefined);
      
        return (
            <KeyboardAwareScrollView>
            {index === 0
            ?

            <View style={{flex:1,width:width(100),alignItems:"center"}}>
            <View style={{width:width(95),flexDirection:"row",height:height(12),marginTop:height(2),marginBottom:height(8),justifyContent:"center"}}>
            <View style={{width:width(15),alignItems:"center",marginRight:width(2)}}>
            <Image  source={require("../image/cardtitle.png")} style={{width:width(12),height:height(10)}} resizeMode="contain"/>
                   
            </View>
            
            <View style={{width:width(80),marginBottom:height(5)}}>
            <Text style={{fontSize:20,fontFamily:"Ubuntu-Bold",marginBottom:height(1)}}>Let's find the right Credit Card for you</Text>
            <View style={{width:width(80)}}>
            <Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}>Getting A Credit Card Has Never Been Easier!</Text>
            <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Get Your Pre-Qualified Offer Now</Text>
            </View>
            </View>
            </View>
            <View style={{width:width(90)}}>
           <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> MOBILE NO </Text>
           </View>
           <View style={{flexDirection:"row",marginTop:height(3),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             <TextInput
             textAlign={"center"}
           placeholder="+971-XXXXXXX"
           maxLength={13}
           
           style={{width:width(80),alignItems:"center",justifyContent:"center"}}
           />
           </TouchableOpacity>
           </View>

           <View style={{width:width(90)}}>
           <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> MONTHLY SALARY </Text>
           </View>
           <View style={{flexDirection:"row",marginTop:height(3),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             <TextInput
             textAlign={"center"}
           placeholder="AED 5000"
           maxLength={13}
           
           style={{width:width(80),alignItems:"center",justifyContent:"center"}}
           />
           </TouchableOpacity>
           </View>

           <View style={{width:width(90)}}>
           <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> EMPLOYER COMPANY NAME </Text>
           </View>
           <View style={{flexDirection:"row",marginTop:height(3),marginBottom:height(10),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             <TextInput
             textAlign={"center"}
           placeholder="ABCD Construction"
           maxLength={13}
           
           style={{width:width(80),alignItems:"center",justifyContent:"center"}}
           />
           </TouchableOpacity>
           </View>

           

           <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(88),marginTop:height(1),marginBottom:height(1),height:height(6),
                borderRadius:12}}
             colors={["#0D9CF8","#0354F1"]} >
               <TouchableOpacity style={{width:width(88),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onFindClicked.bind(this)}>
                    <Text style={{color:"#fff",fontSize:18,fontFamily:"Ubuntu-Bold"}}>
                    FIND MY CREDIT CARD
                  </Text>
               </TouchableOpacity>
          
          </LinearGradient>

            </View>
            :
            <View style={{flex:1,width:width(100),height:height(90),alignItems:"center",justifyContent:"center"}}>
               
            <View style={{width:width(95),flexDirection:"row",alignItems:"center",height:height(12),marginBottom:height(2),marginTop:height(2),marginBottom:height(5),justifyContent:"center"}}>
            <View style={{width:width(15),alignItems:"center",marginRight:width(2)}}>
            <Image  source={require("../image/loanicon.png")} style={{width:width(12),height:height(10)}} resizeMode="contain"/>
                   
            </View>
            
            <View style={{width:width(80)}}>
            <Text style={{fontSize:20,fontFamily:"Ubuntu-Bold",marginBottom:height(1)}}>Let's find the Right Loan for you</Text>
            <View style={{width:width(80)}}>
            <Text style={{fontSize:14,fontFamily:"OpenSans-Regular"}}>Getting A Credit Card Has Never Been Easier!</Text>
            <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold"}}>Get Your Pre-Qualified Offer Now</Text>
            </View>
            </View>
            </View>
             <View style={{height:height(60)}}>               
           <View style={{width:width(90)}}>
           <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> MOBILE NO </Text>
           </View> 
           <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6",marginBottom:height(2)}}>
            
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             <TextInput
              textAlign={"center"}
              placeholder="+971 -5X - XXX -XXXX"
              maxLength={13}
              value={this.state.mobileno}
              keyboardType="number-pad"
              ref={(input) => { this.firstTextInput = input; }}
              onChangeText={this.onMobileNoChanged.bind(this)}
              style={{width:width(80),alignItems:"center",justifyContent:"center"}}
           />
           </TouchableOpacity>
           </View>
           <View style={{width:width(90)}}>
           <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> MONTHLY SALARY </Text>
           </View>
           <View style={{flexDirection:"row",marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             <TextInput
             textAlign={"center"}
           placeholder="AED 5000"
           maxLength={13}
           
           keyboardType="number-pad"
           ref={(input) => { this.salaryinput = input; }}
           value={this.state.salary}
           
           onChange={(event) => this.setState({salary:event.target.value})}
           style={{width:width(80),height:height(6),alignItems:"center",justifyContent:"center"}}
           />
           </TouchableOpacity>
           </View>

           <View style={{width:width(90)}}>
           <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> EMPLOYER COMPANY NAME </Text>
           </View>
           <View style={{flexDirection:"row",marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             <TextInput
             textAlign={"center"}
           placeholder="ABCD Construction"
           maxLength={13}
           
           value={this.state.employername}
           onChange={(event) => this.setState({employername:event.target.value})}
           style={{width:width(80),alignItems:"center",justifyContent:"center"}}
           />
           </TouchableOpacity>
           </View>

           <View style={{width:width(90)}}>
           <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> LOAN AMOUNT REQUIRED </Text>
           </View>
           <View style={{flexDirection:"row",marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             <TextInput
             textAlign={"center"}
           placeholder="AED 10000"
           maxLength={13}
           
           keyboardType="number-pad"
           value={this.state.loan}
           onChangeText={(text) => this.setState({loan:text})}
           style={{width:width(80),height:height(6),alignItems:"center",justifyContent:"center"}}
           />
           </TouchableOpacity>
           </View>

           <View style={{width:width(90)}}>
           <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> REPAYMENT TENURE  </Text>
           </View>
           <View style={{flexDirection:"row",marginBottom:height(6),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             <TextInput
             textAlign={"center"}
           placeholder="4 years"
           maxLength={13}
           keyboardType="number-pad"
           value={this.state.tenure}
           onChangeText={(text) => this.setState({tenure:text})}
           style={{width:width(80),height:height(6),alignItems:"center",justifyContent:"center"}}
           />
           </TouchableOpacity>
           </View>
           </View>
           <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(88),height:height(6),
                borderRadius:12}}
             colors={["#0D9CF8","#0354F1"]} >
               <TouchableOpacity style={{width:width(88),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onFindClicked.bind(this)}>
                    <Text style={{color:"#fff",fontSize:20,fontFamily:"Ubuntu-Bold"}}>
                    FIND MY LOAN
                  </Text>
               </TouchableOpacity>
          
          </LinearGradient>
          
      
            </View>
            }
            </KeyboardAwareScrollView>
        );
    }
}

export default FormDetails;
