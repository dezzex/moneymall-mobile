import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    Picker,
    ActivityIndicator,
    Clipboard,
    ToastAndroid,
    AsyncStorage
} from 'react-native'; 
import LinearGradient from 'react-native-linear-gradient';
import {width,height} from 'react-native-dimension';

export default class goldconvertor extends Component {
    static navigationOptions = ({ navigation  }) => {
        return{
            headerRight:
            <View style={{flexDirection:"row",width:width(20)}}>
            <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
            <Image source={require("../image/avatar.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
            </View>
            <View style={{width:width(10),height:height(10),alignItems:"center",justifyContent:"center"}}>
            
            <Image style={{marginRight:width(3)}} source={require("../image/notification.png")} style={{width:width(4),height:height(4)}} resizeMode="contain"/>
           </View>
           </View>,
           
           headerTitle:
        
        <TouchableOpacity style={{width:width(80),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
        <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",width:height(6),width:height(6),borderRadius: height(6)/2}}>
            <Image source={require("../image/goldrate.png")}  width={width(40)} height={height(10)} resizeMode="contain"/>
        </View>
        <View style={{marginLeft:width(2)}}>
        <Text style={{fontSize: 18,
    fontFamily:"Ubuntu-Bold"}}>
          Gold rates
        </Text>
        </View>
        </TouchableOpacity>,
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
        <Image style={{marginLeft:width(3)}} source={require("../image/headerback.png")} width={width(7)} height={height(6)} resizeMode="contain"/>
        </TouchableOpacity>,
        headerStyle:{
          borderBottomWidth:0,
          height:height(15),
          elevation: 0,
          shadowOpacity: 0
        }}
      };
  
    render() {
        return (
            <View>
                <Text> Coming Soon </Text>
            </View>
        )
    }
}
