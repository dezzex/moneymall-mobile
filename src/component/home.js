import React, { Component } from 'react';
import { View, Text ,Image,StyleSheet,TouchableOpacity,ScrollView,ImageBackground,AsyncStorage,ActivityIndicator,StatusBar,InteractionManager} from 'react-native';
import {width,height} from 'react-native-dimension';
import ProgressCircle from 'react-native-progress-circle'
import LinearGradient from 'react-native-linear-gradient';
import branch, { BranchEvent } from 'react-native-branch'

const value = 66;

export default class home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading:true,
      ispreInitialize:false,
      score:620,
      tempScore:620
    };
  }
  onTimeOutCalled(){
    var score = this.state.score;
    var temp = this.state.tempScore;
    score = score + 1;
    

    if(score > 651){
      temp = 0;
      score = 0;
    }
    else if(score < 10)
      temp = 0;
    else
        temp = score/10;

    temp = Math.trunc(temp);

    
    console.log("dddddddddd",temp,score);
    
    
    this.setState({tempScore:temp,score:score})
  }
  async componentDidMount(){
    var that = this;
    branch.subscribe(({ error, params }) => {
      if (error) {
        console.error('Error from Branch: ' + error)
        return
      }
    
      // params will never be null if error is null
      if (params['+non_branch_link']) {
        const nonBranchUrl = params['+non_branch_link']
        // Route non-Branch URL if appropriate.
        return
      }
    
      if (!params['+clicked_branch_link']) {
        // Indicates initialization success and some other conditions.
        // No link was opened.
        return
      }
    
      // A Branch link was opened.
      // Route link based on data in params, e.g.
    
      // Get title and url for route
      const title = params.$og_title
      const url = params.$canonical_url
      const image = params.$og_image_url

    
      console.log(title,url,image,"Ddddddddddddd");
      
      // Now push the view for this URL
       that.props.navigation.navigate("Refer");
    })
    
    let lastParams = await branch.getLatestReferringParams() 
    let installParams = await branch.getFirstReferringParams() 

    console.log(lastParams,installParams);
    
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      AsyncStorage.getItem("com.moneymall.userdetails").then((value) => {
        if(value === null){
            that.props.navigation.navigate("Login");
        }
        else{
          that.setState({ispreInitialize:false})
          // that.props.navigation.navigate("FingerprintPopup",{loggedIn:true})
        }
      })
    });

  
  }
  onOptionsSelected(index){

    if(index === 0){
      var categorylist =[
        { label:"All Personal Loans", value:"all_personalloan" },
        { label:"Salary Transfer Loans", value:"salary_transferloan" },
        { label:"Non-Salary Transfer Loans" ,value:"nonsalary_transferloan" },
        { label:"Buy Out Loans", value:"buy_outloans" },
        { label:"Debt Settlement" ,value:"debt_settlement" },
        { label:"Islamic" ,value:"islmaic_financeloan" }
      ]
      this.props.navigation.navigate("FormDetails",{url:"http://3.89.11.165:3000/personalloan",categorylist:categorylist,index:1})
    }
    else if(index === 1){
      var categorylist =[
        { label:"All Cards", value:"all_creditcards" },
        { label:"Balance Transfer", value:"creditcardbalance_transfer" },
        { label:"Shopping" ,value:"creditcard_shopping" },
        { label:"CashBack", value:"creditcard_cashback" },
        { label:"Rewards" ,value:"creditcard_rewards" },
        { label:"Credit Builder" ,value:"credit_builder" }
      ]
      this.props.navigation.navigate("FormDetails",{url:"http://3.89.11.165:3000/creditcardloan",categorylist:categorylist,index:0})
    }
    else if(index === 3){   
      this.props.navigation.navigate("OtherLoans")
    }

  }
    static navigationOptions = ({ navigation  }) => {
      return{
    headerRight:
    <View style={{flexDirection:"row",width:width(20)}}>
    <TouchableOpacity style={{width:width(10),height:height(8),alignItems:"center",justifyContent:"center"}} onPress={() => navigation.navigate("Account")}>
    <Image source={require("../image/avatar.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
    </TouchableOpacity>
    <View style={{width:width(10),height:height(8),alignItems:"center",justifyContent:"center"}}>
    
    <Image style={{marginRight:width(3)}} source={require("../image/notification.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
   </View>
   </View>,
    
    headerLeft:<Image source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   ,
    headerStyle:{
      height:height(8),
    }
  }
  };
  onCheckPressed(){
    this.props.navigation.navigate("CheckNow")
  }
  onCreditScoreClicked(){
    this.props.navigation.navigate("CreditScore")
  }
  onMyRewardsClicked(){
    this.props.navigation.navigate("Rewards")
  }
  onMyMoneyPressed(){
    this.props.navigation.navigate("MyMoney")
  }
  render() {
    return (
      <View style={{flex:1,backgroundColor:"#fff"}}>
          <StatusBar backgroundColor="#0354F1" barStyle="light-content" />
       
        {this.state.ispreInitialize
        ?
        <View style={{alignItems:"center",justifyContent:"center",width:width(99),height:height(80)}}>
          <ActivityIndicator size="large" color="#0D9CF8"/>
        </View>
        
        :
        <View style={{backgroundColor:"#fff"}}>
          <View style={{height:height(78),marginTop:height(2),backgroundColor:"#fff"}}>
          <ScrollView showsVerticalScrollIndicator={false} >
      <View style={{alignItems: 'center',height:height(55)}}>
      <View style={{width:width(80),height:height(8),alignItems:"center"}}>
      <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
        Do you know your
        </Text> 
        <View>
        <Text style={{fontWeight: 'bold',fontSize: 18,fontFamily:"Ubuntu-Bold"}}>
        Credit Score ?
        </Text>
        </View>
      </View>
      <View style={{width:width(80),height:height(40),alignItems: 'center',justifyContent:"center",
      shadowOpacity: 0.75,
      shadowRadius: 5,
      }}
      >
        <ProgressCircle
        percent={62}
        radius={height(20)}
        borderWidth={height(3)}
        color={this.state.tempScore < 40 ? "#ff6a00" : "#3399FF"}
        backgroundColor="#ac9f99"
        // shadowColor="#999"
        bgColor="#fff"
    >
       <View style={{flexDirection: 'column',alignItems: 'center',justifyContent:"center",height:height(20),width:width(35)}}>
            {/* <Text style={{fontSize: 10}}>
            Your credit score 
            </Text> */}
            <Text style={{fontSize: height(9),color:this.state.tempScore < 40 ? "#ff6a00" :"#035AF1",fontFamily:"Ubuntu-Bold"}} allowFontScaling={true}>
            {this.state.tempScore}
            </Text>
            <Text style={{fontSize: 22,fontFamily:"OpenSans-Regular"}}>
            Out of 900
            </Text>
        </View>
        <Text style={{fontSize: 16,fontFamily:"OpenSans-Regular"}}>
           Provided By
        </Text>
        <Image source={require("../image/aecb.png")} resizeMode="contain" style={{width:width(25),height:height(5)}}/>
      
    </ProgressCircle>
    {/* <View style={{width:width(40),height:height(4),marginTop:-20,backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}}>
      <Text style={{fontSize: 10,fontFamily:"OpenSans-Regular"}}>
      Next update in 10 days
      </Text>
    </View>
    <TouchableOpacity style={styles.apply} onPress={this.onCheckPressed.bind(this)}>
                    <Text style={styles.applytext}>
                            Check Now
                    </Text>
                </TouchableOpacity> */}
      </View>
      <TouchableOpacity style={styles.apply} onPress={this.onCheckPressed.bind(this)}>
                    <Text style={styles.applytext}>
                            Check my Score
                    </Text>
                </TouchableOpacity>
      </View>

     
     
      <View style={{height:height(3),width:width(99),alignItems: 'center',justifyContent:"center",marginTop:height(2)}}>
      <Text style={{fontSize: 16,fontWeight: 'bold',fontFamily:"Ubuntu-Bold"}}>
      Find the Right Credit Card & Loans for you
      </Text>
      </View>


 
      <View style={{width:width(100),alignItems: 'center',justifyContent:"center",flexDirection: 'column',}}>
        <View style={{height:height(18),width:width(95),alignItems: 'center',justifyContent:"space-between",flexDirection: 'row',}}>
            <TouchableOpacity style={{height:height(15),width:width(30),alignItems: 'center',justifyContent:"center",
            shadowOpacity: 0.25,
            shadowRadius: 3,
            shadowColor: '#707070',
            backgroundColor:"#fff",
            elevation:8,
            shadowOffset: { height: 0, width: 0 }}}
            onPress={this.onOptionsSelected.bind(this,0)}>
            <Image source={require("../image/loan.png")} resizeMode="contain" style={{height:height(5),width:width(15)}}/>
            
            <View style={{alignItems: 'center',marginTop:height(2)}}>
            <Text style={{fontSize: 14,fontFamily:"Ubuntu-Medium"}}>
              Personal Loan
            </Text>
            </View>
            </TouchableOpacity>

            <TouchableOpacity style={{height:height(15),width:width(30),alignItems: 'center',justifyContent:"center",
            shadowOpacity: 0.25,
            shadowRadius: 3,
            shadowColor: '#707070',
            backgroundColor:"#fff",
             elevation:8,
            shadowOffset: { height: 0, width: 0 }}}
            onPress={this.onOptionsSelected.bind(this,1)}>
            <Image source={require("../image/cards.png")} resizeMode="contain" style={{height:height(5),width:width(15)}}/>
            
            <View style={{alignItems: 'center',marginTop:height(2)}}>
            <Text style={{fontSize: 14,fontFamily:"Ubuntu-Medium"}}>
              Credit Card
            </Text>
            </View>
            </TouchableOpacity>

            <TouchableOpacity style={{height:height(15),width:width(30),alignItems: 'center',justifyContent:"center",
        shadowOpacity: 0.25,
        shadowRadius: 3,
        shadowColor: '#707070',
        backgroundColor:"#fff",
        elevation:8,
        shadowOffset: { height: 0, width: 0 }}}
        onPress={this.onOptionsSelected.bind(this,3)}>
        <Image source={require("../image/mortgage.png")} resizeMode="contain" style={{height:height(5),width:width(15)}}/>
        
        <View style={{alignItems: 'center',marginTop:height(2)}}>
        <Text style={{fontSize: 14,fontFamily:"Ubuntu-Medium"}}>
         Other Loans
        </Text>
        </View>
        </TouchableOpacity>

        </View>

        {/* <View style={{width:width(95),height:height(18),alignItems: 'center',justifyContent:"space-between",flexDirection: 'row',}}> */}
     
{/* 
        <TouchableOpacity style={{height:height(15),width:width(30),alignItems: 'center',justifyContent:"center",
        shadowOpacity: 0.25,
        shadowRadius: 3,
        shadowColor: '#707070',
        elevation:8,
        backgroundColor:"#fff",
        shadowOffset: { height: 0, width: 0 }}}
        onPress={this.onOptionsSelected.bind(this,4)}>
        <Image source={require("../image/OtherLoans.png")} resizeMode="contain" style={{height:height(5),width:width(15)}}/>
        
        <View style={{alignItems: 'center',marginTop:height(2)}}>
        <Text style={{fontSize: 14,fontFamily:"Ubuntu-Medium"}}>
          Other Loan
        </Text>
        </View>
        </TouchableOpacity> */}

        {/* <TouchableOpacity style={{height:height(15),width:width(30),alignItems: 'center',justifyContent:"center",
        shadowOpacity: 0.25,
        shadowRadius: 3,
        shadowColor: '#707070',
        backgroundColor:"#fff",
        elevation:8,
        shadowOffset: { height: 0, width: 0 }}}
        onPress={this.onOptionsSelected.bind(this,5)}>
        <Image source={require("../image/insurance.png")} resizeMode="contain" style={{height:height(5),width:width(15)}}/>
        
        <View style={{alignItems: 'center',marginTop:height(2)}}>
        <Text style={{fontSize: 14,fontFamily:"Ubuntu-Medium"}}>
          Insurance
        </Text>
        </View>
        </TouchableOpacity> */}

        
        {/* <TouchableOpacity style={{height:height(15),width:width(30),alignItems: 'center',justifyContent:"center",
            shadowOpacity: 0.25,
            shadowRadius: 3,
            shadowColor: '#707070',
            elevation:8,
            backgroundColor:"#fff",
            shadowOffset: { height: 0, width: 0 }}}
            onPress={this.onOptionsSelected.bind(this,2)}>
            <Image source={require("../image/star.png")} resizeMode="contain" style={{height:height(5),width:width(15)}}/>
            
            <View style={{alignItems: 'center',marginTop:height(2)}}>
            <Text style={{fontSize: 14,fontFamily:"Ubuntu-Medium"}}>
              Islamic
            </Text>
            </View>
            </TouchableOpacity> */}
    {/* </View> */}

    <LinearGradient colors={['#0D9CF8', '#035AF1']} style={{height:height(9),width:width(100),marginTop: height(2),flexDirection: 'row',alignItems: 'center',justifyContent:"center"}}>
      
      <View style={{width:width(95)}}>
      <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
      
      <TouchableOpacity style={{alignItems: 'center',justifyContent:"center",flexDirection: 'row',width:width(33)}} onPress={() => this.props.navigation.navigate("EMICalculator")}>
      <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",width:height(6),width:height(6),borderRadius: height(6)/2}}>
      <Image source={require("../image/calculatoricon.png")} resizeMode="contain" style={{width:height(6),height:height(6)}}/>
      </View>
      <View style={{width:width(18),marginLeft:width(2)}}>
      <Text style={{color:"#fff",fontSize: 12,fontFamily:"Ubuntu-Medium"}}>
          EMI
      </Text>
      <Text style={{color:"#fff",fontSize: 12,fontFamily:"Ubuntu-Medium"}}>
      Calculator
      </Text>
      </View>
      
  </TouchableOpacity>

  <TouchableOpacity style={{alignItems: 'center',justifyContent:"center",flexDirection: 'row',width:width(33)}} onPress={() => this.props.navigation.navigate("SwiftCode")}>
      <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",width:height(6),width:height(6),borderRadius: height(6)/2}}>
      <Image source={require("../image/swifticon.png")} resizeMode="contain" style={{width:height(6),height:height(6)}}/>
      </View>
      <View style={{width:width(14),marginLeft:width(2),alignItems: 'center',}}>
      <Text style={{color:"#fff",fontSize: 12,fontFamily:"Ubuntu-Medium"}}>
      Swift Code
      </Text>
      </View>
      
  </TouchableOpacity>

  <TouchableOpacity style={{alignItems: 'center',justifyContent:"center",flexDirection: 'row',width:width(33)}} onPress={() => this.props.navigation.navigate("CurrencyConvert")}>
      <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",width:height(6),width:height(6),borderRadius: height(6)/2}}>
      <Image source={require("../image/rateicon.png")} resizeMode="contain" style={{width:height(6),height:height(6)}}/>
      </View>
      <View style={{width:width(18),marginLeft:width(2),alignItems: 'center',}}>
      <Text style={{color:"#fff",fontSize: 12,fontFamily:"Ubuntu-Medium"}}>
      Currency Rate
      </Text>
      </View>
      
  </TouchableOpacity>

  <TouchableOpacity style={{alignItems: 'center',justifyContent:"center",flexDirection: 'row',width:width(33)}} onPress={() => this.props.navigation.navigate("GoldConvertor")}>
      <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",width:height(6),width:height(6),borderRadius: height(6)/2}}>
      <Image source={require("../image/goldrate.png")} resizeMode="contain" style={{width:height(6),height:height(6)}}/>
      </View>
      <View style={{width:width(14),marginLeft:width(2),alignItems: 'center',}}>
      <Text style={{color:"#fff",fontSize: 12,fontFamily:"Ubuntu-Medium"}}>
      Gold Rate
      </Text>
      </View>
      
  </TouchableOpacity>




    </ScrollView>

    </View>
      
    </LinearGradient>
      
    <ImageBackground style={{height:height(14),width:width(99),alignItems: 'center',justifyContent:"center",
    shadowRadius: 5,
      shadowColor: '#707070',
      backgroundColor:"#fff",
      shadowOffset: { height: 0, width: 0 }}} source={require("../image/bg1.png")}>
        <Text style={{fontSize: 16,fontFamily:"Ubuntu-Medium"}}>
        Get Rewarded for Being aGood Borrower
        </Text>
        <TouchableOpacity style={styles.applyresponsable}>
                    <Text style={styles.applytextresponsable}>
                    #ResponsibleBorrowing 
                    </Text>
                </TouchableOpacity>
    </ImageBackground> 
    <View style={{flex:1,width:width(99),height:height(25),alignItems:"center",justifyContent:"center"}}> 
     <ScrollView horizontal={true}    showsHorizontalScrollIndicator={false}> 
     <TouchableOpacity style={{width:width(33),alignItems:"center",justifyContent:"center"}}>
    <Image  source={require("../image/reward6.png")} style={{width:width(30),height:height(58)}} resizeMode="contain"/>
    </TouchableOpacity>
    <TouchableOpacity style={{width:width(33),alignItems:"center",justifyContent:"center"}}>
    <Image  source={require("../image/reward7.png")} style={{width:width(30),height:height(58)}} resizeMode="contain"/>
    </TouchableOpacity>
    <TouchableOpacity style={{width:width(33),alignItems:"center",justifyContent:"center"}}>
    <Image  source={require("../image/reward1.png")} style={{width:width(30),height:height(58)}} resizeMode="contain"/>
    
    </TouchableOpacity>
    <TouchableOpacity style={{width:width(33),alignItems:"center",justifyContent:"center"}}>
    <Image  source={require("../image/reward7.png")} style={{width:width(30),height:height(58)}} resizeMode="contain"/>
    </TouchableOpacity>
    <TouchableOpacity style={{width:width(33),alignItems:"center",justifyContent:"center"}}>
    <Image  source={require("../image/reward1.png")} style={{width:width(30),height:height(58)}} resizeMode="contain"/>
    </TouchableOpacity>
    <TouchableOpacity style={{width:width(33),alignItems:"center",justifyContent:"center"}}>
    <Image  source={require("../image/reward7.png")} style={{width:width(30),height:height(58)}} resizeMode="contain"/>
    </TouchableOpacity>
    </ScrollView>
   </View>
      </View>


   
    </ScrollView>
            </View>
            <View style={{width:width(100),height:height(12),flexDirection: 'row',shadowOpacity: 0.75,borderTopLeftRadius: 25,
      borderTopRightRadius:25,
      shadowColor: '#707070',
      backgroundColor:"#fff"}}>
        <TouchableOpacity style={{width:width(20),height:height(10),alignItems: 'center',borderTopLeftRadius: 25}} onPress={() => this.props.navigation.navigate("Home")}>
        <Image  source={require("../image/moneymallbottom.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Home
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyMoneyPressed.bind(this)}>
        <Image  source={require("../image/mymoneybottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
             My Cards
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onCreditScoreClicked.bind(this)}>
        <Image  source={require("../image/creditbottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <View style={{width:width(20),alignItems:"center"}}>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Credit
        </Text>
        </View>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}}  onPress={() => 
    this.props.navigation.navigate("MyGoals")}>
        <Image  source={require("../image/mygoalbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Goals
        </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyRewardsClicked.bind(this)}>
        <Image  source={require("../image/myrewardbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Magnates
        </Text>
        </TouchableOpacity>
      
      </View>
       </View>
        }
    </View>
    );
  }
}
const styles = StyleSheet.create({ 
  container:{
      flex:1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent:'space-between'
  },
  applyresponsable:{
    backgroundColor: "#097FF5",
    width:width(65),
    height:height(4),
    alignItems: 'center',
    justifyContent:'center',
    marginTop: height(4),
    borderRadius: 25,

},
  apply:{
    backgroundColor: "#097FF5",
    width:width(80),
    height:height(6),
    alignItems: 'center',
    justifyContent:'center',
    marginTop: height(1),
    borderRadius: 25,

},
applytextresponsable:{
  fontSize: 18,
  color:"#fff",
  fontWeight: 'bold',
  
  fontFamily:"Ubuntu-Medium"
},
applytext:{
    fontSize: 22,
    color:"#fff",
    fontFamily:"Ubuntu-Bold"
},
})
