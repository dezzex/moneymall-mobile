import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage
  } from 'react-native';
  import {width,height} from 'react-native-dimension';
import { TextInput } from 'react-native-gesture-handler';
  
export default class componentName extends Component {
    constructor(props){
        super(props);
        this.state={
            onSignInPressed:true,
            mobileno:'',
            password:''
        }
    }
    static navigationOptions = {
        headerTitle: <Image source={require("../image/appicon.png")} style={{width:width(60),height:height(7)}} resizeMode="contain"/> ,
        header:null
    };
    onSignInPressed(){
        var that = this;
        if(this.state.mobileno.length > 0){
            var user={
                mobileno:this.state.mobileno
            }
            AsyncStorage.setItem("com.moneymall.userdetails",JSON.stringify(user)).then(() => {
                that.props.navigation.navigate("Home",{loggedIn:true})
                
            })
        }
       
    }

    onNextPressed(){
        var that = this;
        console.log('====================================');
        console.log(this.state.mobileno.length);
        console.log('====================================');
        
        that.setState({onSignInPressed:!this.state.onSignInPressed})
        
    }
    onAuthenticatePressed(){
        if(this.state.mobileno.length)
                this.props.navigation.navigate("OTPVerification")
    }
    onMobileNoEntered(text){
            this.setState({mobileno:text})
    }
    onPasswordEntered(text){
        this.setState({password:text})
    }
    render() {
        return (
            <View style={styles.container}>
            <View style={styles.imagecontainer}>
            <Image source={require("../image/appicon.png")} style={{width:width(50),height:height(7)}} resizeMode="contain"/>
            
            </View>
            {this.state.onSignInPressed
                ?
            <View>
           <TouchableOpacity style={styles.input}>
                    <TextInput value={this.state.mobileno}  onChangeText={(text) => this.setState({mobileno:text})}   placeholder="Email or Mobile No"/>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.password}>
            <TextInput value={this.state.password}  onChangeText={(text) => this.setState({password:text})}   placeholder="password" secureTextEntry={true}/>
            </TouchableOpacity> */}
            <TouchableOpacity style={styles.signin} onPress={this.onSignInPressed.bind(this)}>
                <Text style={styles.signintext}>
                        Sign In
                </Text>
            </TouchableOpacity>
            <View style={{marginTop:height(2),alignItems: 'center',justifyContent:"center"}}>
            <Text style={styles.signintext}>
            OR
                 </Text>
            </View>
           
            {/* <TouchableOpacity style={styles.signup} onPress={this.onNextPressed.bind(this)}>
            <Text style={styles.signuptext}>
                    Sign Up
            </Text>
        </TouchableOpacity> */}
        {/* <TouchableOpacity style={styles.forgot}>
        <Text style={styles.forgottext}>
                Forgot Password ?
        </Text>
    </TouchableOpacity> */}

    </View>
    :
    <View>
    <TouchableOpacity style={styles.password}>
            <TextInput onChangeText={(text) => this.setState({mobileno:text})} placeholder="Enter Email or Phone No" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.signupnext} onPress={this.onAuthenticatePressed.bind(this)}>
            <Text style={styles.signuptext}>
                   Next
            </Text>
        </TouchableOpacity>
        <View style={{marginTop:height(4),alignItems: 'center',justifyContent:"center"}}>
        <Text style={styles.signintext}>
        OR
             </Text>
        </View>
    </View>
            }
    <TouchableOpacity style={styles.socialcontainer}>
    <TouchableOpacity>
    <Image source={require("../image/googleicon.png")} style={styles.social}   resizeMode="contain"/>
    </TouchableOpacity>
    <TouchableOpacity>

    <Image source={require("../image/fbicon.png")} style={styles.social}  resizeMode="contain"/>
    </TouchableOpacity>
                      
    </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({ 
    container:{
        flex:1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center'
    },
    forgot:{
            width:width(80),
            height:height(3),
            marginLeft: 15,
            marginTop:8
    },
    socialcontainer:{
        width:width(30),
        height:height(7),
        alignItems: 'center',
        justifyContent:"space-between",
        flexDirection: 'row',
        backgroundColor: "#fff",
        borderColor: "#707070",
        shadowOffset:{   height: 2,width:2  },
        shadowColor: '#707070',
    },
    social:{
        height:25,
        width:25,
        borderRadius:12
    },
    forgottext:{
        fontSize: 12,
        color:"#707070"
    },
    signin:{
        borderWidth: 1,
        borderColor: "#097FF5",
        width:width(80),
        height:height(7),
        alignItems: 'center',
        justifyContent:'center',
        marginTop: height(4),
        borderRadius: 4,

    },
    input:{
            width:width(80),
            height:height(7),
            // borderColor: "#707070",
            alignItems: 'center',
            justifyContent:'center',
            marginTop: height(4),

    },
    password:{
        width:width(80),
        height:height(7),
        // borderWidth:0.5,
        backgroundColor: "#fff",
        borderColor: "#707070",
        shadowOffset:{   height: 2,width:2  },
        shadowColor: '#707070',
        shadowOpacity: 0.5,
        alignItems: 'center',
        justifyContent:'center',
        marginTop: height(2),

},
signupnext:{
    borderWidth: 1,
    backgroundColor: "#097FF5",

    borderColor: "#097FF5",
    width:width(80),
    height:height(7),
    alignItems: 'center',
    justifyContent:'center',
    marginTop: height(5),
    borderRadius: 4,
},
    signup:{
        borderWidth: 1,
        backgroundColor: "#097FF5",

        borderColor: "#097FF5",
        width:width(80),
        height:height(7),
        alignItems: 'center',
        justifyContent:'center',
        marginTop: height(2),
        borderRadius: 4,

    },
    signuptext:{
        fontSize: 16,
        color:"#fff"
    },
    signintext:{
        fontSize: 16,
    },
    imagecontainer:{
        width:width(90),
        height:height(20),
        alignItems: 'center',
        justifyContent:"center"
    }
});
