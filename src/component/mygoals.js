import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Button,
    TouchableOpacity,
    Image,
    Picker,
    Clipboard,
    ToastAndroid,
    AsyncStorage
} from 'react-native'; 
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import Dialogflow from "react-native-dialogflow";
import { ScrollView, TextInput } from 'react-native-gesture-handler';
// import { WebView } from 'react-native-webview';


export default class MyGoals extends Component {
    constructor(props){
        super(props)
        this.state={
            value:"",
            list:[]
        }
        Dialogflow.setConfiguration(
            "235e85f7e7f14144b10d97ccc071953c", Dialogflow.LANG_GERMAN
          );
    }
    onCheckPressed(){
        this.props.navigation.navigate("CreditScore")
      }
      onCreditScoreClicked(){
        this.props.navigation.navigate("CreditScore")
      }
      onMyRewardsClicked(){
        this.props.navigation.navigate("Rewards")
      }
      onMyMoneyPressed(){
        this.props.navigation.navigate("MyMoney")
      }
      static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
           <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       
        </TouchableOpacity>
       ,
        headerStyle:{
          height:height(8),
        }
        }
      };
      onSendPressed(){
        if(this.state.value.length <= 0) return;
        var temp = this.state.list;
        temp.push(this.state.value);
        var self= this;
        this.setState({value:"",list:temp})
        Dialogflow.requestQuery(this.state.value, result=>{
            if(result!== undefined && result.status.code !== 400){
                console.log('====================================');
                console.log(result);
                console.log('====================================');
                var text = result.result.fulfillment.speech;
               if(text === "")
               text = "Try some other Text..."

                temp.push(text);
                self.setState({list:temp})
            }
        });
      }
      onCardPressed(){

      }
      onStartClicked(){

      }
      componentDidMount(){
        Dialogflow.requestEvent(
            "wel.switch - home",
            {param1: "yo mr. white!"},
            result=>{console.log(result);},
            error=>{console.log(error);}
        );
      }
    render() {
        return (
            <View>
                <View style={{height:height(80),alignItems:"center",justifyContent:"center",width:width(99)}}>
                <View style={{width:width(90),height:height(8),justifyContent:"center",alignItems:"center"}}>
                  <View style={{width:width(100),alignItems:"center",justifyContent:"center",height:height(8),backgroundColor:"#ffff"}}>
                  <Text style={{fontSize:34,fontFamily:"Ubuntu-Bold",color:"#0e4cb0"}}>
                  Achieve your Goals
                  </Text>
                  </View>
                 
                </View>
                <View style={{width:width(100),alignItems:"center",justifyContent:"center",height:height(6)}}>
                  <Text style={{fontSize:14,fontFamily:"Ubuntu-Regular",color:"#000"}}>
                  Select a Goal,Get a Personalised Coaching Plan.      
                  </Text>
                  <Text style={{fontSize:14,fontFamily:"Ubuntu-Regular",color:"#000"}}>
                  Credit Coach will help you Achieve it .
                  </Text>
                  </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                  <View style={{width:width(100),height:height(35),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                  <TouchableOpacity style={{width:width(42),height:height(30),marginLeft:width(5),marginRight:width(5),marginTop:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",borderRadius:4,borderWidth:1,borderColor:"grey"}} onPress={this.onCardPressed.bind(this,1)}>
                  <View style={{height:height(26),width:width(65),alignItems:"center",justifyContent:"center"}}>
                  <Image source={require("../image/improve.png")} resizeMode="contain" style={{width:width(30),height:height(10)}}/>
                  <View style={{width:width(42),height:height(6),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>
                    Improve Your Score
                  </Text>
                  
                  </View>
                  <View style={{width:width(42),height:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  Improve Your Credit Score -
                  </Text>
                  </View>
                  <View style={{width:width(42),height:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  Better Scores, Better Deals
                  </Text>
                  </View>
                  <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(35),height:height(4),borderRadius:25}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onStartClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        Start Now
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
      </View>

    
    </TouchableOpacity>
    <TouchableOpacity style={{width:width(42),height:height(30),marginLeft:width(5),marginRight:width(5),marginTop:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",borderRadius:4,borderWidth:1,borderColor:"grey"}} onPress={this.onCardPressed.bind(this,1)}>
                  <View style={{height:height(26),width:width(65),alignItems:"center",justifyContent:"center"}}>
                  <Image source={require("../image/home.png")} resizeMode="contain" style={{width:width(30),height:height(10)}}/>
                  <View style={{width:width(42),height:height(6),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>
                    Buy a Home
                  </Text>
                  
                  </View>
                  <View style={{width:width(42),height:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  Step by Step guide
                  </Text>
                  </View>
                  <View style={{width:width(42),height:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  to buy your home
                  </Text>
                  </View>
                  <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(35),height:height(4),borderRadius:25}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onStartClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        Start Now
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
      </View>

    
    </TouchableOpacity>
                  </View>
                  <View style={{width:width(100),height:height(35),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                  <TouchableOpacity style={{width:width(42),height:height(30),marginLeft:width(5),marginRight:width(5),marginTop:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",borderRadius:4,borderWidth:1,borderColor:"grey"}} onPress={this.onCardPressed.bind(this,1)}>
                  <View style={{height:height(26),width:width(65),alignItems:"center",justifyContent:"center"}}>
                  <Image source={require("../image/brushes.png")} resizeMode="contain" style={{width:width(30),height:height(10)}}/>
                  <View style={{width:width(42),height:height(6),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>
                  Clear your Debt 
                  </Text>
                  
                  </View>
                  <View style={{width:width(42),height:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  Step by Step Guide to 
                  </Text>
                  </View>
                  <View style={{width:width(42),height:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  Achieve your Financial Freedom
                  </Text>
                  </View>
                  <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(35),height:height(4),borderRadius:25}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onStartClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        Start Now
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
      </View>

    
    </TouchableOpacity>
    <TouchableOpacity style={{width:width(42),height:height(30),marginLeft:width(5),marginRight:width(5),marginTop:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",borderRadius:4,borderWidth:1,borderColor:"grey"}} onPress={this.onCardPressed.bind(this,1)}>
                  <View style={{height:height(26),width:width(65),alignItems:"center",justifyContent:"center"}}>
                  <Image source={require("../image/homeplan.png")} resizeMode="contain" style={{width:width(30),height:height(10)}}/>
                  <View style={{width:width(42),height:height(6),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>
                  Build Credit Score
                  </Text>
                  
                  </View>
                  <View style={{width:width(42),height:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  Step-wise guide to Build 
                  </Text>
                  </View>
                  <View style={{width:width(42),height:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  a Credit from Scratch
                  </Text>
                  </View>
                  <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(35),height:height(4),borderRadius:25}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onStartClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        Start Now
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
      </View>

    
    </TouchableOpacity>
                  </View>

                  <View style={{width:width(100),height:height(35),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                  <TouchableOpacity style={{width:width(42),height:height(30),marginLeft:width(5),marginRight:width(5),marginTop:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",borderRadius:4,borderWidth:1,borderColor:"grey"}} onPress={this.onCardPressed.bind(this,1)}>
                  <View style={{height:height(26),width:width(65),alignItems:"center",justifyContent:"center"}}>
                  <Image source={require("../image/coins.png")} resizeMode="contain" style={{width:width(30),height:height(10)}}/>
                  <View style={{width:width(42),height:height(6),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>
                  Save for Rainy Day
                  </Text>
                  
                  </View>
                  <View style={{width:width(42),height:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  Get Organised and 
                  </Text>
                  </View>
                  <View style={{width:width(42),height:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  save money for future
                  </Text>
                  </View>
                  <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(35),height:height(4),borderRadius:25}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onStartClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        Start Now
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
      </View>

    
    </TouchableOpacity>
    <TouchableOpacity style={{width:width(42),height:height(30),marginLeft:width(5),marginRight:width(5),marginTop:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",borderRadius:4,borderWidth:1,borderColor:"grey"}} onPress={this.onCardPressed.bind(this,1)}>
                  <View style={{height:height(26),width:width(65),alignItems:"center",justifyContent:"center"}}>
                  <Image source={require("../image/repaircredit.png")} resizeMode="contain" style={{width:width(30),height:height(10)}}/>
                  <View style={{width:width(42),height:height(6),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}>
                  Repair Credit Score
                  </Text>
                  
                  </View>
                  <View style={{width:width(42),height:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                  Repair Credit Profile and Get
                  </Text>
                  </View>
                  <View style={{width:width(42),height:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center"}}>
                  <Text style={{fontSize:11,fontFamily:"Ubuntu-Regular"}}>
                   Access to Better Financial Deals
                  </Text>
                  </View>
                  <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(35),height:height(4),borderRadius:25}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onStartClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
                        Start Now
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
      </View>

    
    </TouchableOpacity>
                  </View>

             
                
                </ScrollView>
                {/* <WebView
          scalesPageToFit={true}
          bounces={false}
          javaScriptEnabled
          style={{ height: 500, width: 300 }}
          source={{
            html: `
                  <!DOCTYPE html>
                  <html>
                    <head></head>
                    <body>
                      <div id="baseDiv"><iframe
                        allow="microphone;"
                        width="350"
                        height="430"
                        src="https://console.dialogflow.com/api-client/demo/embedded/759d73eb-56ff-46d4-95c8-946721124751">
                    </iframe></div> 
                    </body>
                  </html>
            `,
          }}
          automaticallyAdjustContentInsets={false}
        /> */}
                {/* <ScrollView>
                    {this.state.list.map((val,idx)=>{
                        return(
                            <View key={idx}  style={{width:width(99),height:height(8),margin:height(1),alignItems:"center",justifyContent:"center"}}>
                            {idx === 0 || idx%2 === 0
                            ?
                            <TouchableOpacity style={{height:height(8),alignItems:"flex-start",justifyContent:"flex-start",width:width(99)}}>
                            <TouchableOpacity style={{backgroundColor:"grey",height:height(5),alignItems:"center",justifyContent:"flex-start",borderRadius:12}}>
                            <Text  style={{fontSize: 14,fontFamily:"Ubuntu-Medium",margin:height(1)}}>
                                {val}
                            </Text>
                            </TouchableOpacity>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={{height:height(8),width:width(99),marginRight:width(2),alignItems:"center",alignItems:"flex-end"}}>
                            <TouchableOpacity style={{backgroundColor:"#1972D7",justifyContent:"center",borderRadius:12}}>
                             <Text style={{color:"#fff",fontSize: 14,fontFamily:"Ubuntu-Medium",margin:height(1)}}>
                                             {val}
                            </Text>
                            </TouchableOpacity>
                            </TouchableOpacity>
                            }
                            </View>
                        )
                    })}
                </ScrollView> */}
                </View>
                {/* <View style={{height:height(12),marginBottom:height(2),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                    <TextInput
                    placeholder="Start your chart here...."
                    style={{borderWidth:1,width:width(90),height:height(10)}}
                    onChangeText={text => this.setState({value:text})}
                    value={this.state.value}
                    />
                   <TouchableOpacity style={{width:width(10),height:height(10),backgroundColor:"#1972D7",alignItems: 'center',justifyContent: 'center'}}
                             onPress={this.onSendPressed.bind(this)}>
                    <Text>
                        Send
                    </Text>
                    </TouchableOpacity> 
                </View> */}
             <View style={{width:width(100),height:height(10),flexDirection: 'row',shadowOpacity: 0.75,borderTopLeftRadius: 25,
      borderTopRightRadius:25,
      shadowRadius: 15,
      elevation:8,
      shadowColor: '#707070',
      backgroundColor:"#fff",
      shadowOffset: { height: 10, width: 10 }}}>
        <TouchableOpacity style={{width:width(20),height:height(10),alignItems: 'center',borderTopLeftRadius: 25}} onPress={() => this.props.navigation.navigate("Home")}>
        <Image  source={require("../image/moneymallbottom.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Home
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyMoneyPressed.bind(this)}>
        <Image  source={require("../image/mymoneybottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
             My Cards
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onCreditScoreClicked.bind(this)}>
        <Image  source={require("../image/creditbottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <View style={{width:width(20),alignItems:"center"}}>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Credit
        </Text>
        </View>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}}  onPress={() => 
    this.props.navigation.navigate("MyGoals")}>
        <Image  source={require("../image/mygoalbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Goals
        </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyRewardsClicked.bind(this)}>
        <Image  source={require("../image/myrewardbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Magnates
        </Text>
        </TouchableOpacity>
      
      </View>
      </View>
        )
    }
}
