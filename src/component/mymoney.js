
import React, { Component } from 'react';
import { View, Text ,Image,PermissionsAndroid,Alert,ActivityIndicator,TouchableOpacity} from 'react-native';
import {width,height} from 'react-native-dimension';
// import Carousel from 'react-native-snap-carousel';
import SmsAndroid  from 'react-native-get-sms-android';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import RadioForm from 'react-native-simple-radio-button';
import StepIndicator from 'react-native-step-indicator';

// import Swiper from 'react-native-swiper';
var filter = {
  box: 'inbox', // 'inbox' (default), 'sent', 'draft', 'outbox', 'failed', 'queued', and '' for all
}
const _MS_PER_DAY = 1000 * 60 * 60 * 24;

var entries =[
  {title:"Title1"},
  {title:"Title2"},
  {title:"Title3"},
  {title:"Title4"},
  
]
export default class MyCarousel extends Component {
  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };

  constructor(props){
    super(props)
    this.state={
      data:[],
      currentCard:'',
      value: 0,
      
     currentPosition: 0
    }
  }

  getIcon(bankname){
      
    if(bankname.toLowerCase() === "emiratesnbd"){
      return require("../image/bank/emiratesnbd.png");
    }
    else if(bankname.toLowerCase() === "citibank"){
      return require("../image/bank/citibank.png");
    }
    else if(bankname.toLowerCase() === "cbi"){
      return require("../image/bank/cbi.png");
    }
    else if(bankname.toLowerCase() === "rakbank"){
      return require("../image/bank/rakbank.png");
    }
    else if(bankname.toLowerCase() === "najmbank"){
      return require("../image/bank/najm.png");
    }
    else if(bankname.toLowerCase() === "samba bank"){
      return require("../image/bank/samba.png");
    }
    else if(bankname.toLowerCase() === "fab"){
      return require("../image/bank/fab.png");
    }
    else if(bankname.toLowerCase() === "citibank"){
      return require("../image/bank/citibank.png");
    }
    else if(bankname.toLowerCase() === "simplylife"){
      return require("../image/bank/adcb.png");
    }
    else if(bankname.toLowerCase() === "stanchart"){
      return require("../image/bank/standardcharted.png");
    }
    else if(bankname.toLowerCase() === "eisms"){
      return require("../image/bank/emiratesislamic.png");
    }

    else if(bankname.toLowerCase() === "dunia"){
      return require("../image/bank/dunia.png");
    }
  }
  isValidDate(val){
    var date = Date.parse(val);
    if(!isNaN(date)){
         date = new Date(date);
         if(date.getFullYear() > 2000 && date.getFullYear() < 2100){
           return true;
         }
         else{
           return false;
         }
        }
  }
  
  onNextClicked(){
    this.props.navigation.navigate("ApplySecondLevel");
}
  componentDidMount(){
        this.readSms();
}
removeExtraCharacter(cardDetail){
  var temp = [];
  cardDetail.map((val,idx) => {
    val = val.replace(/[a-z]/gi, '') ;
    if(val.charAt(val.length-1) === "." || val.charAt(val.length-1) === ";" || val.charAt(val.length-1) === ",")
      val = val.substring(0,val.length-1);

    temp.push(val)
  })
  return temp;
}
getIntegersFromSMS(smsdetails){
  var smsdetails = smsdetails.split(" ");
  var cardDetail = [];
    smsdetails.map((val,idx) => {
    var firstChar = val.charAt(0);
    var isCardDetails = parseInt(firstChar);
    if(!isNaN(isCardDetails)){
       cardDetail.push(val);
    }
  })
  return cardDetail;
}
getParsedDate(dateString){
  var list = dateString.split("/");
  if(list.length === 3){
    var date = list[0];
    var month = list[1];
    var year = list[2];
    if(year.length === 2){
        year = "20"+year;
    }

    return new Date(year+"-"+month+"-"+date);
  }
}
getMonthtoNumber(month){
  switch(month.toLocaleLowerCase()){
    case "jan":
      return "01"
      case "feb":
      return "02"
      case "mar":
      return "03"
      case "apr":
      return "04"
      case "may":
      return "05"
      case "jun":
      return "06"
      case "jul":
      return "07"
      case "aug":
      return "08"
      case "sep":
      return "09"
      case "oct":
      return "10"
      case "nov":
      return "11"
      case "dec":
      return "12"
    
  }
}
getParsedDateforFab(dateString){
  // var list = dateString.split("/");
  // if(list.length === 3){
    var date = dateString[0]+dateString[1];
    var month = dateString[2]+dateString[3]+dateString[4];
    var year = dateString[5]+dateString[6]; 
    if(year.length === 2){
      year = "20"+year;
  }
  month = this.getMonthtoNumber(month);
  return new Date(year+"-"+month+"-"+date)
  // }
}
getRakSMSDetails(smsdetails,address){

  var card = {};
  if(smsdetails.length > 0){
    smsdetails = this.getIntegersFromSMS(smsdetails);
    smsdetails = this.removeExtraCharacter(smsdetails)
  
    var date = this.getParsedDate(smsdetails[1]);

  card.cardno = smsdetails[0];
  card.duedate = date;
  card.icon = this.getIcon(address);
  card.mindue = smsdetails[2];
  card.totaldue = smsdetails[3];
    card.bankname = address;
  card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)

  return card;
  }

}
getfabSMSDetails(smsdetails,address){
  var card = {};
  if(smsdetails.includes("Your statement of the card")){
    var splitteddetails = this.getIntegersFromSMS(smsdetails);
    splitteddetails = this.removeExtraCharacter(splitteddetails)

    var date = this.getParsedDateforFab(smsdetails.split(" ")[smsdetails.split(" ").length - 1]);

    card.cardno = splitteddetails[0];
    card.duedate = date;
    card.icon = this.getIcon(address);
    card.mindue = splitteddetails[3];
    card.totaldue = splitteddetails[2];
    card.bankname = address;


    card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)

    return card;
  }
}
getEmiratesIslamicDetails(smsdetails,address){
  var card = {};
  if(smsdetails.includes("Your Emirates Islamic")){
    var splitteddetails = this.getIntegersFromSMS(smsdetails);
    // splitteddetails = this.removeExtraCharacter(splitteddetails)

    // var date = this.getParsedDateforFab(smsdetails.split(" ")[smsdetails.split(" ").length - 1]);

    card.cardno = splitteddetails[0];
    card.duedate = new Date(splitteddetails[4].substring(0,splitteddetails[4].length - 1));
    card.icon = this.getIcon(address);
    card.mindue = splitteddetails[2];
    card.totaldue = splitteddetails[3];
    card.bankname = address;


    card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)

    return card;
  }
}
getNajmBankDetails(smsdetails,address){
  var card = {};
  var splitteddetails = this.getIntegersFromSMS(smsdetails);
  splitteddetails = this.removeExtraCharacter(splitteddetails)


  card.cardno = "XXXX XXXX XXXX XXXX";
  card.duedate = new Date(splitteddetails[2]);
  card.icon = this.getIcon(address);
  card.mindue = splitteddetails[1];
  card.totaldue = splitteddetails[0];
    card.bankname = address;


    card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)

    return card;
}
getAedParsedString(smsdetails){
  var cardno = '';
  var mindue = '';
  var totaldue = '';
  var index = 0;
  var card = {};
  smsdetails.split(" ").map((val,idx) => {


  
    if(val.includes("AED") || val.includes("XXX")){
    
      if(val.indexOf("XXX") !== -1){
          cardno = val;
      }
      else if(val.indexOf("AED") !== -1){
          var splittedAed = val.split("AED");
          if(splittedAed.length === 2 && parseInt(splittedAed[1])){
    
            if(index === 0){
              mindue = parseInt(splittedAed[1])
              index = index+1;
            }
            else if(index === 1){
              totaldue = parseInt(splittedAed[1])
              index = index+1;
            }
          }
      }
    }
  })
  if(totaldue !== ""){
    var dateString = smsdetails.split("due by")[1].split(".")[0];
    dateString = dateString.replace("  "," ");
    dateString = dateString.split(" ");
    card.duedate = new Date(dateString[3]+"-"+dateString[1]+"-"+dateString[2]);
    card.cardno = cardno;
    card.totaldue = totaldue;
    card.mindue = mindue;

    return card;
  }

}
getStandardSMSDetails(smsdetails,address){
  var card = {};
  var splitteddetails = this.getIntegersFromSMS(smsdetails);
  splitteddetails = this.removeExtraCharacter(splitteddetails);
        
  var dateString = splitteddetails[1].split("/");
  card.cardno = splitteddetails[0];
  card.duedate = new Date("20"+dateString[2]+"-"+dateString[1]+"-"+dateString[0]);
  card.icon = this.getIcon(address);
  card.mindue = splitteddetails[2];
  card.totaldue = splitteddetails[3];
  card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16);
  card.bankname = address;

  return card;
}
getSamabaSMSDetails(smsdetails,address){
  
}
getSimplyLifeSMSDetails(smsdetails,address){
  var card = {};
  var card = this.getAedParsedString(smsdetails);
  if(card !== undefined){
    card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16);
    card.icon = this.getIcon(address);
    card.bankname = address;
    return card;
  }
}
getCitiBankSMSDetails(smsdetails,address){
        var card = {};
  var splitteddetails = this.getIntegersFromSMS(smsdetails);
  splitteddetails = this.removeExtraCharacter(splitteddetails);
    
    var dateString = splitteddetails[2].split("/");

    card.cardno = splitteddetails[0];
    card.duedate = new Date(dateString[2]+"-"+dateString[1]+"-"+dateString[0]);
    card.icon = this.getIcon(address);
    card.mindue = splitteddetails[1];
    card.totaldue = splitteddetails[3];
    card.bankname = address;


    card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)

    return card;
}
getDuniaBankSMSDetails(smsdetails,address){
           var card = {};
  var splitteddetails = this.getIntegersFromSMS(smsdetails);
  // splitteddetails = this.removeExtraCharacter(splitteddetails);

           var card = {};
  var splitteddetails = this.getIntegersFromSMS(smsdetails);
  splitteddetails = this.removeExtraCharacter(splitteddetails);
    
    var dateString = smsdetails.split("due on")[1];
    dateString = dateString.split("and")[0];

   

   dateString = dateString.split("-");

    card.cardno = splitteddetails[0];
    card.duedate = new Date("20"+dateString[2].replace(/\s/g, '')+"-"+dateString[1].replace(/\s/g, '')+"-"+dateString[0].replace(/\s/g, ''));
    card.icon = this.getIcon(address);
    card.mindue = splitteddetails[1];
    card.totaldue = splitteddetails[3];
    card.bankname = address;
    card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)

    console.log('====================================');
    console.log("ddddddddx",card);
    console.log('====================================');

    return card;

}
getCardDetails(cardDetails,address){
  var card;
  if(cardDetails.search("Statement") !== -1 || cardDetails.search("Due") !== -1 || cardDetails.search("due") !== -1 ){
    
    if(typeof address === "string"){

      if(address.toLocaleLowerCase() === "rakbank"){
       card = this.getRakSMSDetails(cardDetails,address);
      }
      else if(address.toLocaleLowerCase() === "fab"){
        card = this.getfabSMSDetails(cardDetails,address);
       }   
      else if(address.toLocaleLowerCase() === "najmbank"){
        card = this.getNajmBankDetails(cardDetails,address);
       }  
       else if(address.toLocaleLowerCase() === "simplylife"){
         card = this.getSimplyLifeSMSDetails(cardDetails,address);
        }
       else if(address.replace(/\s/g, '').toLocaleLowerCase() === "eisms"){
        card = this.getEmiratesIslamicDetails(cardDetails,address);
       }    
       else if(address.replace(/\s/g, '').toLocaleLowerCase() === "sambabank"){
        card = this.getNajmBankDetails(cardDetails,address);
       }
         
         else if(address.replace(/\s/g, '').toLocaleLowerCase() === "stanchart"){
        card = this.getStandardSMSDetails(cardDetails,address);
       } 
        else if(address.replace(/\s/g, '').toLocaleLowerCase() === "citibank"){
        card = this.getCitiBankSMSDetails(cardDetails,address);
       }
        else if(address.replace(/\s/g, '').toLocaleLowerCase() === "dunia"){
        card = this.getDuniaBankSMSDetails(cardDetails,address);
       }
    }
    return card;
  }
}
    _renderItem = ({item, index}) => {
      console.log('====================================');
      console.log(item,"ddddddddddddd");
      console.log('====================================');
        return (
          <View style={{width:width(90),height:height(25),backgroundColor:item.bgcolor,alignItems:"center",justifyContent:"center",borderRadius:12}}>
          <View style={{height:height(10),width:width(90),justifyContent:"space-evenly",alignItems:"center",flexDirection:"row"}}>
                  <View style={{width:width(35)}}>
              <Image source={item.icon} style={{width:width(30),height:height(6)}} resizeMode="cover"/>
              </View> 
              <View style={{width:width(45),height:height(6),alignItems:"center",flexDirection:"row"}}>
                <View style={{marginRight:width(2)}}>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Bold",color:"#fff"}  }>Available Limit</Text>
                </View>
                <TextInput placeholder="Your Limit..." style={{fontSize:12,alignItems:"center",justifyContent:"center",borderBottomColor:"#1972D7",color:"#1972D7",width:width(20),height:height(6)}}/>

                </View>
              </View>
              <View style={{height:height(7),width:width(90)}}>
              <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold",color:"#fff",marginLeft:width(5)}}>{ item.cardno }</Text>
              </View>
              <View style={{height:height(7),width:width(90),alignItems:"flex-end",justifyContent:"center"}}>
              <Image source={(require("../image/bank/visa.png"))} style={{width:width(40),height:height(7)}} resizeMode="contain"/>
              </View>
            </View>
        );
    }

    async readSms(){
      var self = this;
      var mainList = []
     
      try {
          let granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_SMS, {
              title: 'Send SMS',
              message: 'Need access to send sms',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('SEND_SMS permissions granted', granted);
            SmsAndroid.list(JSON.stringify(filter), (fail) => {
              console.log("Failed with this error: " + fail)
          },
          (count, smsList) => {
             
              var tempList = [];
              var arr = JSON.parse(smsList);
              // console.log('Count: ', arr,smsList);
              // // if(arr.length <= 0){
                arr.push(
                  {"address":"Najmbank","body":"Your Najm statement has been sent to your registered email. Total payment due is AED 3880.48.Min Due is AED 843.42, payment due before 04/11/2019"},
                  {"address":"EISMS","body":"Your Emirates Islamic Credit Card Mini Stmt: Card No starting 413840*  Statement date 10-NOV-19. Total Amt Due AED 4555.8 . Min Amt Due AED 272.32 .  Due Date 05-DEC-19."},
                  {"address":"SAMBA Bank","body":"SAMBA Alert: Your Credit Card Statement Outstanding is AED 4317. Minimum payment of AED 1033  is due by 04/11/2019. Please disregard if payment is made. www.samba.ae/pay"},
                  {"address":"StanChart","body":"The statement for Manhattan Visa Platinum credit card ending with 7744 has been dispatched. Payment Due Date: 22/10/19; Min. Payment Due: AED 110.00; Outstanding Balance: AED 150.45."},
                   {"address":"CitiBank","body":"Your Citi Card ending 3182 has a min amount AED 192.11 due on 30/11/2019. Total due AED 689.87. Check your statement on the go www.citibank.ae/estmt"},
                   {"address":"DUNIA","body":"Your dunia Credit Card ending 4739 has a min amount of AED 491.52 due on 23-DEC-19 and total due of AED 9830.42. Your December 2019 statement has been emailed to you."},
                   {"address":"EmiratesNBD","body":"Emirates NBD Credit Card Mini Stmt for Card ending 0952: Statement date 16/11/19. Total Amt Due AED 3625.90, Due Date 11/12/19. Min Amt Due AED 1192.33"},
                   {"address":"FAB","body":"Your statement of the card ending with 8962 dated 22Nov19 has been sent to you and can also be viewed in the new FAB mobile banking app, download it from the App Store goo.gl/FB7qEZ or Google Play goo.gl/7dXnNc. The total amount due is AED 7,477.00. Minimum due is AED 373.85. Due date is 17Dec19"},
                 
                  {"address":"SIMPLYLIFE","body":" Min. payment of AED715.77 on Cr.Card XXX3565 due by Nov  4 2019.To avoid finance charges, pay AED4787.10.Total billed amt. AED11016.37.Pay AED715.77 by Nov  4 2019 to avoid late payment fees of AED 241.50. Please ignore if already paid."})
             
                  //  }
              // arr = JSON.parse(tempList);
              console.log('List: ', arr.length);
              var banklist = [];
              arr.forEach(function(object){
                  var address = object["address"].toString();
                  var body =  object["body"].toString();
                  
                  if((body.toLowerCase().search("due") !== -1 ||body.toLowerCase().search("total") !== -1 || address.toLocaleLowerCase().includes("simplelife"))){
                    
                     console.log('====================================');
                     console.log(object);
                     console.log('====================================');
                      banklist.push(object);
                      
                      var card = self.getCardDetails(body,address);
                      if(card !== undefined){
                        var isCardExist = false;
                          mainList.map((val,idx) => {
                            if(val.bankname === card.bankname){
                                  isCardExist = true;
                            }
                          })
                        if(!isCardExist){
                             mainList.push(card);
                        }
                      }
              }
              })

              console.log('====================================');
              console.log(mainList);
              console.log('====================================');

              if(mainList.length > 0){

                self.setState({data:mainList,currentCard:mainList[0]})
              }

              
          });
          } else {
            // Alert.alert('SEND_SMS permissions denied');
            console.log('SEND_SMS permissions denied');
          }
        } catch (err) {
          console.log('====================================');
          console.log(err);
          console.log('====================================');
          // Alert.alert(err);
        }

   }
   dateDiffInDays(a, b) {
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
  
    return Math.floor((utc1 - utc2) / _MS_PER_DAY);
  }
   onCreditScoreClicked(){
    this.props.navigation.navigate("CreditScore")
  }
  onMyRewardsClicked(){
    this.props.navigation.navigate("Rewards")
  }
  onMyMoneyPressed(){
    this.props.navigation.navigate("MyMoney")
  }
   onNextClicked(){
    this.props.navigation.navigate("ApplySecondLevel");
}
    render () {
      console.log('====================================');
      console.log(this.state.data.length);
      console.log('====================================');
        return (
          <View style={{flex:1}}>
            <View style={{flex:1,height:height(50),width:width(100),alignItems:"center",justifyContent:"center",backgroundColor:"green"}}>
            <View style={{width:width(80),alignItems:"center",justifyContent:"center"}}>
             
            {this.state.data.length > 0
            ?
            <View/>
            // <Carousel
            //   ref={(c) => { this._carousel = c; }}
            //   data={this.state.data}
            //   renderItem={this._renderItem}
            //   sliderWidth={width(99)}
            //   itemWidth={width(90)}
            //   onSnapToItem={(index) => {this.setState({currentCard:this.state.data[index]})}}
            // />
            :
            <ActivityIndicator/>
            }
            </View>
            </View>
            <View style={{width:width(99),alignItems:"center",justifyContent:"center"}}>
            <View style={{width:width(90),height:height(25),marginTop:height(2),flexDirection:"row",alignItems:"center",justifyContent:"space-evenly"}}>
              <View style={{width:width(58),alignItems:"center",justifyContent:"center",borderWidth:1,borderColor:"#E6E6E6",height:height(20),borderRadius:12}}>
                <View style={{height:height(5),width:width(50),marginTop:height(2)}}>
                  <View style={{height:height(5),width:width(20),justifyContent:"center",alignItems:"center",backgroundColor:"#E6E6E6",borderRadius:8}}>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Medium",color:"#D71922"}}>
                  Alerts
                </Text>
                </View>
                </View>
                <View style={{height:height(14),width:width(60),paddingTop:width(3),alignItems:"center",justifyContent:"center"}}>
                {/* <Swiper style={{height:height(14),width:width(50)}}>
                  <View style={{height:height(10),width:width(40)}}>
                    <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
                  </View>
                   </Swiper> */}
                </View>
              </View>
              <View style={{width:width(28),alignItems:"center",justifyContent:"center",height:height(20),borderRadius:12,borderWidth:1,borderColor:"#E6E6E6"}}>
              
              <View style={{height:height(4),width:width(23),borderRadius:12,alignItems:"center",justifyContent:"center",backgroundColor:"#E6E6E6"}}>
                <Text style={{fontSize:15,color:"#F99E1A",fontFamily:"Ubuntu-Medium"}}>Payment In</Text>
              </View>

              <View style={{height:height(10),width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                  <Text style={{fontSize:20,fontFamily:"Ubuntu-Bold"}}>
                      {this.state.currentCard !== "" ? this.dateDiffInDays(new Date(this.state.currentCard.duedate),new Date()) < 0 ? "0" :
                      this.dateDiffInDays(new Date(this.state.currentCard.duedate),new Date()) : "0"} 
                  </Text>
              </View>
              
              <View style={{width:width(30),height:height(3),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(10),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold",color:"#F99E1A"}}>
                  By
                </Text> 
                </View>
                <View style={{width:width(18),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                <Text style={{fontSize:14,fontFamily:"Ubuntu-Light"}}>{this.state.currentCard !== "" ? 
                    new Date(this.state.currentCard.duedate).getDate()+"/"+(new Date(this.state.currentCard.duedate).getMonth()+1)+"/"+new Date(this.state.currentCard.duedate).getFullYear(): "0"} 
                </Text>
                </View>
              </View>

              </View>

            

            </View>
            <View style={{width:width(99),height:height(10),marginBottom:height(2),alignItems:"center",justifyContent:"space-evenly",elevation:4}}>
              <View style={{width:width(90),backgroundColor:"#E6E6E6",alignItems:"center",justifyContent:"center",height:height(4),borderTopRightRadius:12,borderTopLeftRadius:12,flexDirection:"row"}}>
                <View style={{width:width(46),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Light"}}>
                    Minimum Due
                  </Text>
                </View>
                <View style={{width:width(46),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Light"}}>
                    Total Due
                  </Text>
                </View>
              </View>

              <View style={{width:width(90),alignItems:"center",justifyContent:"center",height:height(7),borderRadius:12,flexDirection:"row",borderColor:"#E6E6E6",borderBottomRightRadius:12,borderBottomLeftRadius:12,borderBottomWidth:1,borderLeftWidth:1,borderRightWidth:1}}>
                <View style={{width:width(85),alignItems:"center",justifyContent:"center",height:height(8),flexDirection:"row"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Light"}}>
                   
                  </Text>
                    
                  <RadioForm 
                  formHorizontal={true}
                  animation={true}
                  radioStyle={{paddingRight: 30,marginLeft:40}}
                  // labelColor={'#0ECC4D'}
                  radio_props={[{label: this.state.currentCard !== "" ?  "AED "+this.state.currentCard.mindue : "0 AED"},
                                {label:this.state.currentCard !== "" ? "AED "+this.state.currentCard.totaldue : "0 AED"}]}
                  initial={0}
                  onPress={(value) => {this.setState({value:value})}}
                >
                </RadioForm>
                </View>
              </View>

         

              </View>
              <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(5),borderRadius:8}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:20,fontFamily:"Ubuntu-Bold"}}>
                        Pay Now
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
             
            </View>
            </View>
        );
    }
}