import React, { Component } from 'react';
import { View, Text ,Image,PermissionsAndroid,Alert,ActivityIndicator,TouchableOpacity, ImageBackground} from 'react-native';
import {width,height} from 'react-native-dimension';
// import Carousel from 'react-native-snap-carousel';
import SmsAndroid  from 'react-native-get-sms-android';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import RadioForm from 'react-native-simple-radio-button';
// import Slider from "react-native-slider";
 
import Swiper from 'react-native-swiper';
const labels = ["Minimum","Total","Other"];
var entries =[
  {title:"Title1"},
  {title:"Title2"},
  {title:"Title3"},
  {title:"Title4"},
  
]
var filter = {
    box: 'inbox', // 'inbox' (default), 'sent', 'draft', 'outbox', 'failed', 'queued', and '' for all
}
const _MS_PER_DAY = 1000 * 60 * 60 * 24;
var radio_props = [
  {label: 'param1', value: 0 },
  {label: 'param2', value: 1 }
];
export default class mymoneycard extends Component {
  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };
      onNextClicked(){
        // this.props.navigation.navigate("ApplySecondLevel");
  }
      componentDidMount(){
            this.readSms();
    }
    getIcon(bankname){

      var icon={};
      
      if(bankname.replace(/\s/g, '').toLowerCase() === "emiratesnbd"){
        return {icon:require("../image/bank/emiratesnbd.png"),bg:require("../image/bank/emiratesnbd.png")};
      }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "citibank"){
        return {icon:require("../image/bank/citibank.png"),bg:require("../image/cardbg/citi.png")};
      }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "cbi"){
        return {icon:require("../image/bank/cbi.png"),bg:require("../image/cardbg/cbj.jpg")};
      }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "cbd"){
        return {icon:require("../image/bank/cbd.png"),bg:require("../image/cardbg/cbj.jpg")};
      }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "rakbank"){
        return {icon:require("../image/bank/rakbank.png"),bg:require("../image/cardbg/rakbank.png")};
      }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "najmbank"){
        return {icon:require("../image/bank/najm.png"),bg:require("../image/cardbg/najm.png")};
       }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "samba"){
        return {icon:require("../image/bank/samba.png"),bg:require("../image/cardbg/samba.png")};
      }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "fab"){
        return {icon:require("../image/bank/fab.png"),bg:require("../image/cardbg/fab.png")};
      }
      // else if(bankname.replace(/\s/g, '').toLowerCase() === "citibank"){
      //   return {icon:require("../image/bank/citibank.png"),bg:require("../image/bank/citi.png")};
      // }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "simplylife"){
        return {icon:require("../image/bank/adcb.png"),bg:require("../image/cardbg/adcb.png")};
      }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "stanchart"){
        
        return {icon:require("../image/bank/standardcharted.png"),bg:require("../image/cardbg/scb.png")};
      }
      else if(bankname.replace(/\s/g, '').toLowerCase() === "eisms"){
        return {icon:require("../image/bank/emiratesislamic.png"),bg:require("../image/cardbg/emiratesislamic.png")};
     }

      else if(bankname.replace(/\s/g, '').toLowerCase() === "dunia"){
        return {icon:require("../image/bank/dunia.png"),bg:require("../image/cardbg/dunia.png")};
     
      }   
      else if(bankname.replace(/\s/g, '').toLowerCase() === "mashreqbank"){
        return {icon:require("../image/bank/mashreq.png"),bg:require("../image/cardbg/mashreq.png")};
     
      }  
      console.log('====================================');
      console.log("dddddddddddd",bankname);
      console.log('====================================');
    }
    isValidDate(val){
      var date = Date.parse(val);
      if(!isNaN(date)){
           date = new Date(date);
           if(date.getFullYear() > 2000 && date.getFullYear() < 2100){
             return true;
           }
           else{
             return false;
           }
          }
    }
    getDueDate(cardDetail){
      var dueDate = undefined;
        cardDetail.map((val,idx) => {
     
          
          var date = Date.parse(val);
          if(!isNaN(date)){
               date = new Date(date);
               if(date.getFullYear() > 2000 && date.getFullYear() < 2025){
              if(dueDate === undefined)
                  dueDate = date;
              else if(dueDate < date){
                dueDate = date;
              }
            }
          }
        })
        return dueDate;
    }
    getTotalDue(cardDetail){
      var date="";
      cardDetail.map((val,idx) =>{
        val = val.replace(';','');
        val = val.replace('.','');
        if(val.indexOf("/") !== -1  && !this.isValidDate(val)){
          for (var i = 0; i < val.length; i++) {
            if(!isNaN(parseInt(val.charAt(i)))){
                date = date+val.charAt(i);
            }
            else if( (val.charAt(i) === "/")){
              date = date+"/";
            }
          }
         

          cardDetail.splice(idx,1);
         
        }
       
         
     
        
      })
      return [date,cardDetail];
    }
    getCardNo(cardDetails){
      var cardNo = '';
      cardDetails.map((val,idx) => {
       
        if(val.indexOf(".") === -1 && val.indexOf("/") === -1 && val.indexOf(":") === -1 && val.indexOf("x") === -1 &&  val.length >= 4 && !this.isValidDate(val)){
                cardNo = val;
        }
      })
      return cardNo;
    }
    getTotalDueAmount(cardDetails){
      var totalDue = 0;

      cardDetails.map((val,idx) => {
       
        if( val.indexOf("/") === -1 && val.indexOf("*") === -1 && val.indexOf("X") === -1 && val.indexOf("x") === -1 && !this.isValidDate(val)){

                var temp = parseFloat(val);
                if(totalDue <= temp){
                  totalDue = temp;
                }
        }
      })
      return totalDue;
    }
    getMinDueAmount(cardDetails){
      var minDue = 0;

      cardDetails.map((val,idx) => {

        if( val.indexOf("/") === -1 && !this.isValidDate(val)  && val.indexOf("*") === -1 && val.indexOf("X") === -1){

                var temp = parseFloat(val);
                if(minDue === 0)
                    minDue = temp;
                else if(minDue >= temp){
                  minDue = temp;
                }
        }
    })
      return minDue;
    }
    removeExtraCharacter(cardDetail){
      var temp = [];
      cardDetail.map((val,idx) => {
        val = val.replace(/[a-z]/gi, '') ;
        if(val.charAt(val.length-1) === "." || val.charAt(val.length-1) === ";" || val.charAt(val.length-1) === ",")
          val = val.substring(0,val.length-1);

        temp.push(val)
      })
      return temp;
    }
    getIntegersFromSMS(smsdetails){
      var smsdetails = smsdetails.split(" ");
      var cardDetail = [];
        smsdetails.map((val,idx) => {
        var firstChar = val.charAt(0);
        var isCardDetails = parseInt(firstChar);
        if(!isNaN(isCardDetails)){
           cardDetail.push(val);
        }
      })
      return cardDetail;
    }
    getParsedDate(dateString){
      if(dateString === undefined){
        console.log('====================================');
        console.log("1111111111",dateString);
        console.log('====================================');
        return;
      }
      var list = dateString.split("/");
      if(list.length === 3){
        var date = list[0];
        var month = list[1];
        var year = list[2];
        if(year.length === 2){
            year = "20"+year;
        }

        return new Date(year+"-"+month+"-"+date);
      }
    }
    getMonthtoNumber(month){
      switch(month.toLocaleLowerCase()){
        case "jan":
          return "01"
          case "feb":
          return "02"
          case "mar":
          return "03"
          case "apr":
          return "04"
          case "may":
          return "05"
          case "jun":
          return "06"
          case "jul":
          return "07"
          case "aug":
          return "08"
          case "sep":
          return "09"
          case "oct":
          return "10"
          case "nov":
          return "11"
          case "dec":
          return "12"
        
      }
    }
    getParsedDateforFab(dateString){
      // var list = dateString.split("/");
      // if(list.length === 3){
        var date = dateString[0]+dateString[1];
        var month = dateString[2]+dateString[3]+dateString[4];
        var year = dateString[5]+dateString[6]; 
        if(year.length === 2){
          year = "20"+year;
      }
      month = this.getMonthtoNumber(month);
      return new Date(year+"-"+month+"-"+date)
      // }
    }
    getRakSMSDetails(smsdetails,address){

      var card = {};
    
     
      if(smsdetails.length > 0 ){
        smsdetails = this.getIntegersFromSMS(smsdetails);
        smsdetails = this.removeExtraCharacter(smsdetails)

       

       
      if(smsdetails.length === 4){
       
      var dateString = smsdetails[1].split("/");
      if(dateString !== undefined && dateString.length === 3){
        
      var year = dateString[2];
      var date =  dateString[0].replace(/\s/g, '');
      var month = dateString[1].replace(/\s/g, '');

      card.cardno = smsdetails[0].substring(6,11);
      card.duedate = new Date("20"+year+"-"+month+"-"+date);
      card.icon = this.getIcon(address).icon;
      card.bg = this.getIcon(address).bg;
      card.mindue = smsdetails[2];
      card.totaldue =smsdetails[3];
        card.bankname = address;
      card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)

      console.log('====================================');
      console.log("RAKKKKKKKKKKKK",card);
      console.log('====================================');


      return card;
      }
      }
    }

    }
    getfabSMSDetails(smsdetails,address){
      var card = {};
      if(smsdetails.includes("Your statement of the card")){
        var splitteddetails = this.getIntegersFromSMS(smsdetails);
        splitteddetails = this.removeExtraCharacter(splitteddetails)

        var date = this.getParsedDateforFab(smsdetails.split(" ")[smsdetails.split(" ").length - 1]);

        card.cardno = splitteddetails[0];
        card.duedate = date;
        card.icon = this.getIcon(address).icon;
        card.bg = this.getIcon(address).bg;
        card.mindue = splitteddetails[3];
        card.totaldue = splitteddetails[2].replace(",","");
        card.bankname = address;


        card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)
  
        return card;
      }
    }
    getEmiratesIslamicDetails(smsdetails,address){
      var card = {};

      if(smsdetails.includes("Your Emirates Islamic")){
        var splitteddetails = this.getIntegersFromSMS(smsdetails);

       
       
        if(splitteddetails.length === 5){
         
          var dateString = splitteddetails[4].split("-");
          var year = dateString[2].split(".")[0].replace(/\s/g, '');
          var date =  dateString[0].replace(/\s/g, '');
          var month = dateString[1].replace(/\s/g, '');

        card.cardno = splitteddetails[0].substring(0,  splitteddetails[0].length - 1);
        card.duedate = new Date("20"+year+"-"+this.getMonthtoNumber(month)+"-"+date);
        card.icon = this.getIcon(address).icon;
        card.bg = this.getIcon(address).bg;
        card.mindue = splitteddetails[3];
        card.totaldue = splitteddetails[2];
        card.bankname = address;



        card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)
        return card;
        }
        else{
          console.log("wente rnong in islamic",splitteddetails,smsdetails);
          
        }
      }
    }
    getNajmBankDetails(smsdetails,address){
      var card = {};
      var splitteddetails = this.getIntegersFromSMS(smsdetails);
      splitteddetails = this.removeExtraCharacter(splitteddetails)


      card.cardno = "XXXX XXXX XXXX XXXX";
      card.duedate = new Date(splitteddetails[2]);
      card.icon = this.getIcon(address);
      card.bg = this.getIcon(address).bg;
      card.mindue = splitteddetails[1];
      card.totaldue = splitteddetails[0];
        card.bankname = address;


        card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)
  
        return card;
    }
    getAedParsedString(smsdetails){
      var cardno = '';
      var mindue = '';
      var totaldue = '';
      var index = 0;
      var card = {};
      smsdetails.split(" ").map((val,idx) => {

    
      
        if(val.includes("AED") || val.includes("XXX")){
        
          if(val.indexOf("XXX") !== -1){
              cardno = val;
          }
          else if(val.indexOf("AED") !== -1){
              var splittedAed = val.split("AED");
              if(splittedAed.length === 2 && parseInt(splittedAed[1])){
        
                if(index === 0){
                  mindue = parseInt(splittedAed[1])
                  index = index+1;
                }
                else if(index === 1){
                  totaldue = parseInt(splittedAed[1])
                  index = index+1;
                }
              }
          }
        }
      })
      if(totaldue !== ""){
        var dateString = smsdetails.split("due by")[1].split(".")[0];
        dateString = dateString.replace("  "," ");
        
        dateString = dateString.split(" ");
        card.duedate = new Date(dateString[3]+"-"+dateString[1]+"-"+dateString[2]);
        card.cardno = cardno.substring(3,6);;
        card.totaldue = totaldue;
        card.mindue = mindue;

        return card;
      }

    }
    getStandardSMSDetails(smsdetails,address){
      var card = {};
      var splitteddetails = this.getIntegersFromSMS(smsdetails);
      splitteddetails = this.removeExtraCharacter(splitteddetails);
            
      var dateString = splitteddetails[1].split("/");
      card.cardno = splitteddetails[0];
      card.duedate = new Date("20"+dateString[2]+"-"+dateString[1]+"-"+dateString[0]);
      card.icon = this.getIcon(address).icon;
      card.bg = this.getIcon(address).bg;
      card.mindue = splitteddetails[2];
      card.totaldue = splitteddetails[3];
      card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16);
      card.bankname = address;

      return card;
    }
    getSamabaSMSDetails(smsdetails,address){
      
    }
    getSimplyLifeSMSDetails(smsdetails,address){
      var card = {};
      var card = this.getAedParsedString(smsdetails);
      if(card !== undefined){
        card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16);
        card.icon = this.getIcon(address).icon;
        card.bg = this.getIcon(address).bg;
        card.bankname = address;
        return card;
      }
    }
    getCitiBankSMSDetails(smsdetails,address){
            var card = {};
      var splitteddetails = this.getIntegersFromSMS(smsdetails);
      splitteddetails = this.removeExtraCharacter(splitteddetails);


        
        var dateString = splitteddetails[1].split("/");

        console.log('====================================');
        console.log(dateString);
        console.log('====================================');

        if(dateString.length !== 3){
          dateString = splitteddetails[2].split("/");
          card.cardno = splitteddetails[0];
          card.duedate = new Date(dateString[2]+"-"+dateString[1]+"-"+dateString[0]);
          card.icon = this.getIcon(address).icon;
          card.bg = this.getIcon(address).bg;
          card.mindue = splitteddetails[1];
          card.totaldue = splitteddetails[3];
          card.bankname = address;
  
  
          card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)
    
          return card
        }
        else{
         

          card.cardno = splitteddetails[0];
        card.duedate = new Date(dateString[2]+"-"+dateString[1]+"-"+dateString[0]);
        card.icon = this.getIcon(address).icon;
        card.bg = this.getIcon(address).bg;
        card.mindue = splitteddetails[2];
        card.totaldue = splitteddetails[3];
        card.bankname = address;


        card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)
  
        return card
        }

        ;
    }
    getDuniaBankSMSDetails(smsdetails,address){
      var card = {};
      var splitteddetails = this.getIntegersFromSMS(smsdetails);
      splitteddetails = this.removeExtraCharacter(splitteddetails);
        
        var dateString = smsdetails.split("due on")[1];
        dateString = dateString.split("and")[0];

       

       dateString = dateString.split("-");

        card.cardno = splitteddetails[0];
        card.duedate = new Date("20"+dateString[2].replace(/\s/g, '')+"-"+dateString[1].replace(/\s/g, '')+"-"+dateString[0].replace(/\s/g, ''));
        card.icon = this.getIcon(address).icon;
        card.bg = this.getIcon(address).bg;
        card.mindue = splitteddetails[1];
        card.totaldue = splitteddetails[3];
        card.bankname = address;
        card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)
  
        return card;

    }
    getMashreqCardDetails(smsdetails,address){
      var card = {};
      var splitteddetails = this.getIntegersFromSMS(smsdetails);
      splitteddetails = this.removeExtraCharacter(splitteddetails);
        console.log('====================================');
        console.log(splitteddetails);
        console.log('====================================');

        var dateString = splitteddetails[0].split("/");

        card.cardno = splitteddetails[1];
        card.duedate = new Date(dateString[2]+"-"+dateString[1]+"-"+dateString[0]);
        card.icon = this.getIcon(address).icon;
        card.bg = this.getIcon(address).bg;
        card.mindue = splitteddetails[3];
        card.totaldue = splitteddetails[2];
        card.bankname = address;


        card.bgcolor = '#'+Math.floor(Math.random()*16777215).toString(16)
  
        return card;
    }
    getCBDSMSDetials(smsdetails,address){
      var splitteddetails = this.getIntegersFromSMS(smsdetails);
      splitteddetails = this.removeExtraCharacter(splitteddetails);
               var card = {};
     
    }
    checkForNAN(card){
      // if(card.duedate !== undefined && Object.prototype.toString.call(card.duedate) === "[object Date]")
      // if (isNaN(card.duedate.getTime())) {  // d.valueOf() could also work
      //    return false;
      // } else {
        return true;
      // }
      // return false;
    }
    getCardDetails(cardDetails,address){
      var card;
      if(cardDetails.search("Statement") !== -1 || cardDetails.search("Due") !== -1 || cardDetails.search("due") !== -1 ){
        if(typeof address === "string"){

          if(address.replace(/\s/g, '').toLocaleLowerCase() === "rakbank"){
        
           card = this.getRakSMSDetails(cardDetails,address);
          }
          else if(address.replace(/\s/g, '').toLocaleLowerCase() === "fab"){
            card = this.getfabSMSDetails(cardDetails,address);
           }   
          else if(address.replace(/\s/g, '').toLocaleLowerCase() === "najmbank"){
            card = this.getNajmBankDetails(cardDetails,address);
           }  
           else if(address.replace(/\s/g, '').toLocaleLowerCase() === "simplylife"){
             card = this.getSimplyLifeSMSDetails(cardDetails,address);
            }
           else if(address.replace(/\s/g, '').toLocaleLowerCase() === "eisms"){
            card = this.getEmiratesIslamicDetails(cardDetails,address);
           }    
           else if(address.replace(/\s/g, '').toLocaleLowerCase() === "sambabank"){
            card = this.getNajmBankDetails(cardDetails,address);
           }
             
             else if(address.replace(/\s/g, '').toLocaleLowerCase() === "stanchart"){
            card = this.getStandardSMSDetails(cardDetails,address);
           } 
            else if(address.replace(/\s/g, '').toLocaleLowerCase() === "citibank"){
            card = this.getCitiBankSMSDetails(cardDetails,address);
           }
            else if(address.replace(/\s/g, '').toLocaleLowerCase() === "dunia"){
            card = this.getDuniaBankSMSDetails(cardDetails,address);
           }
           else if(address.replace(/\s/g, '').toLocaleLowerCase() === "cbd"){
           card = this.getCBDSMSDetials(cardDetails,address);
          }
          else if(address.replace(/\s/g, '').toLocaleLowerCase() === "mashreqbank"){
            card = this.getMashreqCardDetails(cardDetails,address);

         }
        }
    
        return card;
    //   var splittedDetails = cardDetails.split(" ");
    //   var cardDetail = [];
    //   var card = undefined;

    //   splittedDetails.map((val,idx) => {
    //     var firstChar = val.charAt(0);
    //     var isCardDetails = parseInt(firstChar);
    //     if(!isNaN(isCardDetails)){
    //        cardDetail.push(val);
    //     }
    //   })

    //   console.log('====================================');
    //   console.log(cardDetail,cardDetails);
    //   console.log('====================================');

 
    //   var tempDetails = [];
    //   cardDetail.map((val,idx) => {
    //     // val = val.replace('.','');
    //     val = val.replace("-","/");
    //     val = val.replace("-","/");

    //     tempDetails.push(val)
    //   })
      
    //   cardDetail = tempDetails;

    //   cardDetail = this.removeExtraCharacter(cardDetail)
    //   console.log('====================================');
    //   console.log(cardDetail);
    //   console.log('====================================');
     

    //   if(cardDetail.length === 4){
    //     var returnVal  = this.getTotalDue(cardDetail);
    //     if(returnVal.length === 2){
    //     var DueDate = returnVal[0];
    //     var MinDue = this.getMinDueAmount(returnVal[1]);
    //     var TotalDue = this.getTotalDueAmount(returnVal[1]);
    //     var CardNo = this.getCardNo(returnVal[1]);
    
    //     if(CardNo.toString().length > 0 && DueDate.toString().length > 0 && TotalDue.toString().length > 0 && MinDue.toString().length > 0){
    //       card ={cardno:CardNo.toString(), icon:this.getIcon(address),bankname:address,duedate:DueDate.toString(),mindue:MinDue.toString(),totaldue:TotalDue.toString(),bgcolor:'#'+Math.floor(Math.random()*16777215).toString(16),}

         
    //     }
    //   }
    //   }
    //   else if(cardDetail.length === 3){
    //     var dueDate  = this.getDueDate(cardDetail);
    //     var mindue = this.getMinDueAmount(cardDetail);
    //     var totaldue = this.getTotalDueAmount(cardDetail);

    //     if(dueDate === undefined || mindue === undefined || totaldue === undefined) return;
      
    //       if(dueDate.toString().length > 0 && totaldue.toString().length > 0 && mindue.toString().length > 0){
    //         card ={cardno:"XXXX XXXX XXXX XXXX", icon:this.getIcon(address),bankname:address,duedate:dueDate.toString(),mindue:mindue.toString(),totaldue:totaldue.toString(),bgcolor:'#'+Math.floor(Math.random()*16777215).toString(16)}
    //     }
    //   }
    //   else if(cardDetail.length === 5){
       
    //     var dueDate  = this.getDueDate(cardDetail);
    //     var mindue = this.getMinDueAmount(cardDetail);
    //     var totaldue = this.getTotalDueAmount(cardDetail);
    //     var cardNo = this.getCardNo(cardDetail);

        
    //     if(dueDate === undefined || mindue === undefined || totaldue === undefined) return;

    //       if(dueDate.toString().length > 0 && mindue.toString().length > 0 && totaldue.toString().length > 0 && cardNo.toString().length > 0){
    //         card ={cardno:cardNo,duedate:dueDate.toString(), icon:this.getIcon(address),bankname:address,mindue:mindue.toString(),totaldue:totaldue.toString(),bgcolor:'#'+Math.floor(Math.random()*16777215).toString(16)}
            
           
    //       }
    //   }
    // }
    //   return card;
    }
  }
  
    async readSms(){
        var self = this;
        var mainList = []
       
        try {
            let granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.READ_SMS, {
                title: 'Send SMS',
                message: 'Need access to send sms',
              },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              console.log('SEND_SMS permissions granted', granted);
              SmsAndroid.list(JSON.stringify(filter), (fail) => {
                console.log("Failed with this error: " + fail)
            },
            (count, smsList) => {
               if(smsList === undefined) return;
                var tempList = [];
                var arr = JSON.parse(smsList);
                // var arr = [];
                console.log('Count: ', arr,smsList);
                // if(arr.length <= 0){
                  // arr.push(
                  //   {"address":"Najmbank","body":"Your Najm statement has been sent to your registered email. Total payment due is AED 3880.48.Min Due is AED 843.42, payment due before 04/11/2019"},
                  //   {"address":"EI SMS","body":"Your Emirates Islamic Credit Card Mini Stmt: Card No starting 413840*  Statement date 10-NOV-19. Total Amt Due AED 4555.8 . Min Amt Due AED 272.32 .  Due Date 05-DEC-19."},
                  //   {"address":"SAMBA","body":"SAMBA Alert: Your Credit Card Statement Outstanding is AED 4317. Minimum payment of AED 1033  is due by 04/11/2019. Please disregard if payment is made. www.samba.ae/pay"},
                  //   {"address":"StanChart","body":"The statement for Manhattan Visa Platinum credit card ending with 7744 has been dispatched. Payment Due Date: 22/10/19; Min. Payment Due: AED 110.00; Outstanding Balance: AED 150.45."},
                  //    {"address":"CitiBank","body":"Your Citi Card ending 3182 has a min amount AED 192.11 due on 30/11/2019. Total due AED 689.87. Check your statement on the go www.citibank.ae/estmt"},
                  //   // {"address":"CitiBank","body":"Please make a payment on your Citi Card ending with 3182 by 30/12/2019. The min due is AED 199.11 & total due is AED 3822.00."},
                   
                    
                  //   {"address":"DUNIA","body":"Your dunia Credit Card ending 4739 has a min amount of AED 491.52 due on 23-DEC-19 and total due of AED 9830.42. Your December 2019 statement has been emailed to you."},
                  //    {"address":"EmiratesNBD","body":"Emirates NBD Credit Card Mini Stmt for Card ending 0952: Statement date 16/11/19. Total Amt Due AED 3625.90, Due Date 11/12/19. Min Amt Due AED 1192.33"},
                  //    {"address":"FAB","body":"Your statement of the card ending with 8962 dated 22Nov19 has been sent to you and can also be viewed in the new FAB mobile banking app, download it from the App Store goo.gl/FB7qEZ or Google Play goo.gl/7dXnNc. The total amount due is AED 7,477.00. Minimum due is AED 373.85. Due date is 17Dec19"},
                  //    {"address":"CBD","body":"Statement for CC ending 8566, dated Dec 08 2019 can be viewed on CBD Mobile http://bit.ly/CBDapp. Min payment due 418.78 on Dec 30 2019, total outstanding balance is 8375.50. If any discrepancy, notify the bank within 30 days of the statement."},
                  //    {"address":"RAKBANK","body":"Dear Customer, please note that a minimum payment amount of AED 581.39 is due on your Credit Card 5260-XXXX-XXXX-7009 by 08-Jan-20. Please ignore this message if already paid. Regards, RAKBANK."},
               
                  //   {"address":"MashreqBank","body":" Dear Customer,Your payment due date is 11/01/2020 for your Credit Card ending with 1279, total amount due is 35726.62, Min amount due is AED 1786.34, log into Mashreq.com/Login to view your Card statement, please ignore if already paid."},
                  //   {"address":"SIMPLYLIFE","body":" Min. payment of AED715.77 on Cr.Card XXX3565 due by Nov  4 2019.To avoid finance charges, pay AED4787.10.Total billed amt. AED11016.37.Pay AED715.77 by Nov  4 2019 to avoid late payment fees of AED 241.50. Please ignore if already paid."})
               
                // arr = JSON.parse(tempList);
                console.log('List: ', arr.length);
                var banklist = [];
                arr.forEach(function(object){
                    var address = object["address"].toString();
                    var body =  object["body"].toString();
                    
                    if((body.toLowerCase().search("due") !== -1 ||body.toLowerCase().search("total") !== -1 || address.toLocaleLowerCase().includes("simplelife"))){
                      
                       
                        banklist.push(object);
                        
                        var card = self.getCardDetails(body,address);
                        if(card !== undefined){
                          var isCardExist = false;
                            mainList.map((val,idx) => {
                              if(val.bankname.toLocaleLowerCase() === card.bankname.toLocaleLowerCase()){
                                    isCardExist = true;
                              }
                            })
                          if(!isCardExist && self.checkForNAN(card)){
                               mainList.push(card);
                          }
                        }
                }
                })

                console.log('====================================');
                console.log(mainList);
                console.log('====================================');

                if(mainList.length > 0){

                  self.setState({data:mainList,currentCard:mainList[0],sliderval:parseInt(mainList[0].mindue)})
                }

                
            });
            } else {
              Alert.alert('SEND_SMS permissions denied');
              console.log('SEND_SMS permissions denied');
            }
          } catch (err) {
            Alert.alert(err);
          }

     }
     constructor(props){
       super(props)
       this.state={
         data:[],
         currentCard:'',
         value: 0,
         sliderval:0,
         
        currentPosition: 0
       }
     }
     onCardSwiped(index){
       console.log('====================================');
       console.log(index);
       console.log('====================================');
     }
     _renderItem = ({item, index}) => {
      // console.log('=========77777777777=====================');
      // console.log(item,item === undefined);
      // console.log('====================================');
      //  if(item === undefined) return;
      
       return(
        <View style={{width:width(90),height:height(15),alignItems:"center",justifyContent:"center"}}>
        <Text style={{fontSize:24}}>{ item.title }</Text>
    </View>
//         <View style={{width:width(90),height:height(25),marginLeft:width(4),marginRight:width(4),backgroundColor:item.bgcolor,alignItems:"center",justifyContent:"center",borderRadius:12}}>
//         <View style={{height:height(10),width:width(90),justifyContent:"space-evenly",alignItems:"center",flexDirection:"row"}}>
//    <View style={{width:width(35)}}>
// <Image source={item.icon} style={{width:width(30),height:height(6)}} resizeMode="cover"/>
// </View> 
// <View style={{width:width(45),height:height(6),alignItems:"center",flexDirection:"row"}}>
// <View style={{marginRight:width(2)}}>
// <Text style={{fontSize:12,fontFamily:"Ubuntu-Bold",color:"#fff"}  }>Available Limit</Text>
// </View>
// <TextInput placeholder="Your Limit..." style={{fontSize:12,alignItems:"center",justifyContent:"center",borderBottomColor:"#1972D7",color:"#1972D7",width:width(20),height:height(6)}}/>

// </View>
// </View>
// <View style={{height:height(7),width:width(90)}}>
// <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold",color:"#fff",marginLeft:width(5)}}>{ item.cardno }</Text>
// </View>
// <View style={{height:height(7),width:width(90),alignItems:"flex-end",justifyContent:"center"}}>
// <Image source={(require("../image/bank/visa.png"))} style={{width:width(40),height:height(7)}} resizeMode="contain"/>
// </View>
//           </View>
      )
  }
  change(value) {
    this.setState(() => {
      return {
        sliderval: parseFloat(value),
      };
    });
  }

  dateDiffInDays(a, b) {
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    return Math.floor((utc1 - utc2) / _MS_PER_DAY);
  }

  onCreditScoreClicked(){
    this.props.navigation.navigate("CreditScore")
  }
  onMyRewardsClicked(){
    this.props.navigation.navigate("Rewards")
  }
  onMyMoneyPressed(){
    this.props.navigation.navigate("MyMoney")
  }
  getColor(){
    console.log('====================================');
    console.log(this.state.sliderval);
    console.log('====================================');
    
    return "green";
  }
  handleScroll(event) {
    console.log(event.nativeEvent.contentOffset.y);
   }
   getDueDays(){
     var date = "0";
     if(this.state.currentCard !== undefined && this.state.currentCard.duedate !== undefined &&this.state.currentCard.duedate !== ""){
          date = this.dateDiffInDays(new Date(this.state.currentCard.duedate),new Date());
     }
     return date;
   }
   _onMomentumScrollEnd = ({ nativeEvent }) => {
    const index = Math.round(nativeEvent.contentOffset.x / width(99));
    
    
    if (this.state.currentCard !== this.state.data[index]) {
      this.setState({currentCard:this.state.data[index],sliderval:parseInt(this.state.data[index].mindue)})
    }
  };
    render() {
      const left = this.state.sliderval * (width(100)-60)/100 - 15;
      console.log('====================================');
      console.log(left);
      console.log('====================================');
        return (
            <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
              <View style={{height:height(82),width:width(100)}}>
              <View style={{marginTop:height(2),alignItems:"center",justifyContent:"center"}}>
                {this.state.data.length === 0
                ?
                <ActivityIndicator/>
                :
                <ScrollView  onScroll={this.handleScroll} 
                showsHorizontalScrollIndicator={false} 
                horizontal={true} 
                onMomentumScrollEnd={this._onMomentumScrollEnd}
                  pagingEnabled={true}
                >
                    {this.state.data.map((item,idx) => {
                      return(
                      <View key={idx}  style={{width:width(100),height:height(30),alignItems:"center",justifyContent:"center",borderRadius:12}}>
                        <ImageBackground key={idx} source={item.bg} style={{flex:1 , width: undefined, height: undefined,borderRadius:12,alignItems:"center",justifyContent:"center"}}>
                      <View style={{height:height(20),width:width(90),justifyContent:"flex-end",alignItems:"center",flexDirection:"row"}}> 
                                    
                                    <View style={{width:width(45),height:height(6),alignItems:"center",flexDirection:"row"}}>
                                     <View style={{marginRight:width(2)}}>
                                     <Text style={{fontSize:12,fontFamily:"Ubuntu-Bold",color:"#fff"}  }>Available Limit</Text>
                                     </View>
                                     <TextInput placeholder="Your Limit..." style={{fontSize:12,alignItems:"center",justifyContent:"center",borderBottomColor:"#1972D7",color:"#1972D7",width:width(20),height:height(6)}}/>

                                     </View>
                             </View>
                             <View style={{height:height(7),width:width(90)}}>
                                   <Text style={{fontSize:18,fontFamily:"Ubuntu-Bold",color:"#fff",marginLeft:width(5)}}>{item.bankname === "EI SMS" ? "XXXX XXXX XX"+ item.cardno[0]+item.cardno[1]+" "+item.cardno.substring(2): item.cardno.length === 4 ? "XXXX   XXXX   XXXX   "+item.cardno : item.cardno }</Text>
                             </View>
                        </ImageBackground>
                        </View>
                        
                      )
                       
                      
                    })}
                </ScrollView>
                }
            </View>
            <View style={{width:width(99),alignItems:"center",justifyContent:"center"}}>
            <View style={{width:width(90),height:height(25),marginTop:height(2),flexDirection:"row",alignItems:"center",justifyContent:"space-evenly"}}>
              <View style={{width:width(58),alignItems:"center",justifyContent:"center",borderWidth:1,borderColor:"#E6E6E6",height:height(20),borderRadius:12}}>
                <View style={{height:height(5),width:width(50),marginTop:height(2)}}>
                  <View style={{height:height(5),width:width(20),justifyContent:"center",alignItems:"center",backgroundColor:"#E6E6E6",borderRadius:8}}>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Medium",color:"#D71922"}}>
                  Alerts
                </Text>
                </View>
                </View>
                <View style={{height:height(14),width:width(60),paddingTop:width(3),alignItems:"center",justifyContent:"center"}}>
                {/* <Swiper style={{height:height(14),width:width(50)}}>
                  <View style={{height:height(10),width:width(40)}}>
                    <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
                  </View>
                   </Swiper> */}
                   <View style={{height:height(10),width:width(40)}}>
                    <Text style={{fontSize:10,fontFamily:"OpenSans-Regular"}}>You utilized 90% of your credit card  limit, Make payment to bring it under 50% to improve your Credit Score.</Text>
                  </View>
                </View>
              </View>
              <View style={{width:width(28),alignItems:"center",justifyContent:"center",height:height(20),borderRadius:12,borderWidth:1,borderColor:"#E6E6E6"}}>
              
              <View style={{height:height(4),width:width(23),borderRadius:12,alignItems:"center",justifyContent:"center",backgroundColor:"#E6E6E6"}}>
                <Text style={{fontSize:15,color:"#F99E1A",fontFamily:"Ubuntu-Medium"}}>Payment In</Text>
              </View>

              <View style={{height:height(10),width:width(30),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                  <Text style={{fontSize:20,fontFamily:"Ubuntu-Bold"}}>
                      {this.state.currentCard !== "" ? this.dateDiffInDays(new Date(this.state.currentCard.duedate),new Date()) < 0 ? "0" :
                      this.dateDiffInDays(new Date(this.state.currentCard.duedate),new Date()) : "0"} 
                  </Text>
              </View>
              
              <View style={{width:width(30),height:height(3),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                <View style={{width:width(10),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold",color:"#F99E1A"}}>
                  By
                </Text> 
                </View>
                <View style={{width:width(18),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Light"}}>{this.state.currentCard !== "" ? 
                    new Date(this.state.currentCard.duedate).getDate()+"/"+(new Date(this.state.currentCard.duedate).getMonth()+1)+"/"+new Date(this.state.currentCard.duedate).getFullYear(): "0"} 
                </Text>
                </View>
              </View>

              </View>

            

            </View>
            <View style={{width:width(99),height:height(16),alignItems:"center",justifyContent:"space-evenly",elevation:4}}>
              <View style={{width:width(90),backgroundColor:"#E6E6E6",alignItems:"center",justifyContent:"center",height:height(4),borderTopRightRadius:12,borderTopLeftRadius:12,flexDirection:"row"}}>
                <View style={{width:width(46),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Light"}}>
                    Minimum Due
                  </Text>
                </View>
                <View style={{width:width(46),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Light"}}>
                    Total Due
                  </Text>
                </View>
              </View>

              <View style={{width:width(90),alignItems:"center",justifyContent:"center",height:height(7),borderRadius:12,flexDirection:"row",borderColor:"#E6E6E6",borderBottomRightRadius:12,borderBottomLeftRadius:12,borderBottomWidth:1,borderLeftWidth:1,borderRightWidth:1}}>
                <View style={{width:width(85),alignItems:"center",justifyContent:"center",height:height(8),flexDirection:"row"}}>
                  <Text style={{fontSize:16,fontFamily:"Ubuntu-Light"}}>
                   
                  </Text>
                    
                  <RadioForm 
                  formHorizontal={true}
                  animation={true}
                  radioStyle={{paddingRight: 30,marginLeft:40}}
                  // labelColor={'#0ECC4D'}
                  radio_props={[{label: this.state.currentCard !== "" ?  "AED "+this.state.currentCard.mindue : "0 AED"},
                                {label:this.state.currentCard !== "" ? "AED "+this.state.currentCard.totaldue : "0 AED"}]}
                  initial={0}
                  onPress={(value) => {this.setState({value:value})}}
                >
                </RadioForm>
                </View>
              </View>
              {/* {this.state.data.length !== 0
                ?
              <View style={{width:width(99),height:height(5),alignItems:"center",justifyContent:"center"}}>
              <Slider
                  value={this.state.sliderval}
                  maximumValue={parseInt(this.state.currentCard.totaldue)}
                  style={{width:width(88),height:height(8)}}
                  onValueChange={value => this.setState({ value })}
                  trackStyle={{height:height(3)}}
                  minimumTrackTintColor={"yellow"}
                />
              </View>
              :
              <View/>
              } */}
              </View>
              <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(5),borderRadius:8}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:20,fontFamily:"Ubuntu-Bold"}}>
                        Get Free Alerts & Save
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
             
            </View>
            </View>


           
            <View style={{width:width(100),height:height(10),flexDirection: 'row',shadowOpacity: 0.75,borderTopLeftRadius: 25,
      borderTopRightRadius:25,
      shadowRadius: 15,
      elevation:8,
      shadowColor: '#707070',
      backgroundColor:"#fff",
      shadowOffset: { height: 10, width: 10 }}}>
        <TouchableOpacity style={{width:width(20),height:height(10),alignItems: 'center',borderTopLeftRadius: 25}} onPress={() => this.props.navigation.navigate("Home")}>
        <Image  source={require("../image/moneymallbottom.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Home
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyMoneyPressed.bind(this)}>
        <Image  source={require("../image/mymoneybottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
             My Cards
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onCreditScoreClicked.bind(this)}>
        <Image  source={require("../image/creditbottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <View style={{width:width(20),alignItems:"center"}}>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Credit
        </Text>
        </View>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}}  onPress={() => 
    this.props.navigation.navigate("MyGoals")}>
        <Image  source={require("../image/mygoalbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Goals
        </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyRewardsClicked.bind(this)}>
        <Image  source={require("../image/myrewardbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Magnates
        </Text>
        </TouchableOpacity>
      
      </View>
           </View>
        )
    }
}
