import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';


var categoryList =[
    {image:require("../image/category2.png")},
    {image:require("../image/category1.png")},
    {image:require("../image/category3.png")},
    {image:require("../image/category4.png")},
    {image:require("../image/category5.png")},
    
    
]




export default class MyRewards extends Component {
    static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
           <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       
        </TouchableOpacity>
       ,
        headerStyle:{
          height:height(8),
        }
        }
      };
  constructor(props) {
    super(props);
    this.state = {
        rewardType:0,
        idx:0,
         rewardlist:[
            {image:require("../image/reward1.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
            {image:require("../image/reward2.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
            {image:require("../image/reward3.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
            {image:require("../image/reward4.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
            {image:require("../image/reward1.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
            
            
        ]
    };
  }
  onRewardTypePressed(index){
      this.setState({rewardType:index})
  }

  onCategoryPressed(idx){
    var rewardlist ;
      if(idx%2 === 0){
       rewardlist =  [
            {image:require("../image/reward1.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
            {image:require("../image/reward2.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
            {image:require("../image/reward3.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
            {image:require("../image/reward4.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
            {image:require("../image/reward1.png"),text:"Scape Restaurant",location:"Arabian luxury",place:"Burj Al Arab"},
        ]
      }
      else{
        rewardlist =  [
            {image:require("../image/reward5.png"),text:"Fitzone Gym",location:"Fitness",place:"Karama"},
            {image:require("../image/reward6.png"),text:"Cosmetic Healing Center",location:"Arabian luxury",place:"Karama"},
            {image:require("../image/reward7.png"),text:"Cosmetic Healing Center",location:"Arabian luxury",place:"Karama"},
            {image:require("../image/reward5.png"),text:"Cosmetic Healing Center",location:"Arabian luxury",place:"Karama"},
            {image:require("../image/reward6.png"),text:"Cosmetic Healing Center",location:"Arabian luxury",place:"Karama"},
        ]
      }
      this.setState({idx:idx,rewardlist:rewardlist})
  }

  render() {
    return (
      <View style={styles.container}>
              <ImageBackground style={styles.container} source={require("../image/magnates.png")}>
                
                </ImageBackground>
          {/* <View style={{width:width(99),height:height(10),flexDirection:"row"}}>
          <Image  source={require("../image/backicon.png")} style={{width:width(10),height:height(10)}} resizeMode="contain"/>
          <View style={{width:width(70),alignItems:"center",justifyContent:"center"}}>
            <View style={{width:width(40),backgroundColor:"#F6F6F6",borderRadius:12,alignItems:"center",justifyContent:"center",height:height(5)}}>
                <Text style={{fontWeight:"bold",fontSize:20}}>
                    Magnates
                </Text>
            </View>
          </View>
          <View style={{width:width(20),alignItems:"center",justifyContent:"center"}}>
            <Text style={{fontSize:14}}>
                826 <Text style={{color:"#F99E1A"}}>Points</Text>
            </Text>
          </View>
          </View> */}
          {/* {this.state.rewardType === 0
          ?
          <View>
          <View style={{width:width(99),height:height(6),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
          <TouchableOpacity style={{width:width(45),backgroundColor:"#EEE9E9",alignItems:"center",justifyContent:"center",height:height(5)}} onPress={this.onRewardTypePressed.bind(this,0)}>
                <Text style={{fontSize:14}}>
                    Money Mall Rewards
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{width:width(45),backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(5)}} onPress={this.onRewardTypePressed.bind(this,1)}>
                <Text style={{fontSize:14}}>
                    Bank Rewards
                </Text>
            </TouchableOpacity>

          </View>
          <View style={{flexDirection:"row",height:height(5),alignItems:"center",justifyContent:"center"}}>
            <View style={{width:width(80)}}>
                <Text>
                Rewards from  <Text style={{fontWeight:"bold"}}>
                Moneymall
                    </Text>
                </Text>
            </View>
            <Text>
                View All
            </Text>
          </View>
          </View>
          :
          <View>
          <View style={{width:width(99),height:height(6),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
          <TouchableOpacity style={{width:width(45),backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(5)}} onPress={this.onRewardTypePressed.bind(this,0)}>
                <Text style={{fontSize:14}}>
                    Money Mall Rewards
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{width:width(45),backgroundColor:"#EEE9E9",alignItems:"center",justifyContent:"center",height:height(5)}} onPress={this.onRewardTypePressed.bind(this,1)}>
                <Text style={{fontSize:14}}>
                    Bank Rewards
                </Text>
            </TouchableOpacity>
            </View>
            <View style={{flexDirection:"row",height:height(5),alignItems:"center",justifyContent:"center"}}>
            <View style={{width:width(80)}}>
                <Text>
                Rewards from  <Text style={{fontWeight:"bold"}}>
                Bank
                    </Text>
                </Text>
            </View>
            <Text>
                View All
            </Text>
          </View>

          </View>
          } */}
 {/* <TouchableOpacity style={{width:width(90),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(5)}} onPress={this.onRewardTypePressed.bind(this,0)}>
               <TextInput
               placeholder="Search"
               style={{width:width(80),alignItems:"center",justifyContent:"center"}}
               />
               <View style={{width:width(10)}}>
               <Image  source={require("../image/searchicon.png")} style={{width:width(5),height:height(5)}} resizeMode="contain"/>

               </View>
            </TouchableOpacity>
            <View style={{height:height(15),marginTop:height(3),alignItems:"center",justifyContent:"center"}}> */}
          {/* <ScrollView
            showsHorizontalScrollIndicator={false}
            horizontal={true}>
                {categoryList.map((value,idx) => {
                        return(
                            this.state.idx === idx
                            ?
                            <TouchableOpacity onPress={this.onCategoryPressed.bind(this,idx)}>
                                       <Image key={idx} source={value.image} style={{width:height(10),height:height(10),marginLeft:width(5)}} resizeMode="contain"/>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={this.onCategoryPressed.bind(this,idx)}>
                            <Image key={idx} source={value.image} style={{opacity:0.5,width:height(10),height:height(10),marginLeft:width(5)}} resizeMode="contain"/>
                             </TouchableOpacity>
                         )
                })}
            </ScrollView>
         */}
              
            {/* </View> */}

            {/* <View style={{height:height(60),marginTop:height(3),alignItems:"center",justifyContent:"center"}}>
          <ScrollView
            showsVerticalScrollIndicator={false}>
                {this.state.rewardlist.map((value,idx) => {
                        return(
                            <TouchableOpacity key={idx} style={{flexDirection:"row",width:width(90),marginBottom:height(2),height:height(12),alignItems:"center",justifyContent:"center",backgroundColor:"#F3F3F3"}}>
                                       <Image  source={value.image} style={{width:height(25),height:height(10),marginLeft:width(5)}} resizeMode="contain"/>
                                    <View style={{width:width(70)}}>
                                        <View style={{height:height(8)}}>
                                        <Text style={{fontSize:14,fontWeight:"bold"}}>
                                            {value.text}
                                        </Text>
                                        <Text style={{color:"#707070",fontSize:12}}>
                                            {value.location}
                                        </Text>
                                        </View>
                                        <View style={{height:height(2),flexDirection:"row"}}>
                                        <Image  source={require("../image/locationicon.png")} style={{width:width(5),height:height(2)}} resizeMode="contain"/>
                                 
                                            <Text>
                                                {value.place}
                                            </Text>
                                        </View>
                                       

                                    </View>
                            </TouchableOpacity>
                         )
                })}
            </ScrollView>
        
              
            </View> */}

      </View>
    );
  }
}


const styles = StyleSheet.create({ 
    container:{
        flex:1,
        height:height(92),
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center'
    },
})