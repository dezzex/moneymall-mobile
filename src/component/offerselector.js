import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,Picker } from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Slider from '@react-native-community/slider';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DateTimePicker from '@react-native-community/datetimepicker';

import  worldlist from './world.json';
export default class OfferSelector extends Component {

  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };
  constructor(props) {
    super(props);
    this.state = {
        slideCompletionValue:1000,
        gender:"Male",
        show:false,
                fromCountry:'',
                toCountry:'',
                date: new Date("07/06/89"),
                mode: 'date',
    };
  }


  onNextClicked(){
        this.props.navigation.navigate("CheckNow2");
  }
    onCardPressed(index){
        if(index === 0){
            var categorylist =[
              { label:"All Personal Loans", value:"all_personalloan" },
              { label:"Salary Transfer Loans", value:"salary_transferloan" },
              { label:"Non-Salary Transfer Loans" ,value:"nonsalary_transferloan" },
              { label:"Buy Out Loans", value:"buy_outloans" },
              { label:"Debt Settlement" ,value:"debt_settlement" },
              { label:"Islamic" ,value:"islmaic_financeloan" }
            ]
            this.props.navigation.navigate("CreditCardList",{url:"http://3.89.11.165:3000/personalloan",categorylist:categorylist,index:1})
          }
          else if(index === 1){
            var categorylist =[
              { label:"All Cards", value:"all_creditcards" },
              { label:"Balance Transfer", value:"creditcardbalance_transfer" },
              { label:"Shopping" ,value:"creditcard_shopping" },
              { label:"CashBack", value:"creditcard_cashback" },
              { label:"Rewards" ,value:"creditcard_rewards" },
              { label:"Credit Builder" ,value:"credit_builder" }
            ]
            this.props.navigation.navigate("CreditCardList",{url:"http://3.89.11.165:3000/creditcardloan",categorylist:categorylist,index:0})
          }
          else if(index === 3){
            var categorylist =[
              { label:"All Mortage Loans", value:"all_mortgage" },
              { label:"Salary Transfer Loans" ,value:"salary_transfer" },
              { label:"Buy Out Loans", value:"buy_outloans" },
              { label:"Non Resident Mortage" ,value:"non_resident_mortgage" },
              { label:"Islamic" ,value:"islamic_finance" }
            ]
            this.props.navigation.navigate("CreditCardList",{url:"http://3.89.11.165:3000/homeloan",categorylist:categorylist,index:2})
          }
  
            
    }
    render() {
      
      var month = this.state.date.getMonth()+1;
      console.log('====================================');
      console.log(month);
      console.log('====================================');
        return (
            <KeyboardAwareScrollView>
            <View  style={{flex:1,alignItems:"center",justifyContent:"center",height:height(85)}}>

                <View style={{width:width(90),height:height(8),justifyContent:"center",alignItems:"center"}}>
                  <Text style={{fontSize:26,fontFamily:"Ubuntu-Bold",color:"#0e4cb0"}}>
                  What are you interested in ?
                  </Text>
                  <Text style={{fontSize:22,fontFamily:"Ubuntu-Regular",color:"#0e4cb0"}}>
                  Check your eligibility before you Apply
                  </Text>
                </View>
               <TouchableOpacity style={{width:width(80),height:height(20),marginLeft:width(5),marginRight:width(5),marginTop:height(2),marginBottom:height(2),alignItems:"center",justifyContent:"center",backgroundColor:"#0354F1",borderRadius:12}} onPress={this.onCardPressed.bind(this,1)}>
      <View style={{height:height(12),width:width(65),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
      <Image source={require("../image/creditcardselector.png")} resizeMode="contain" style={{width:width(30),height:height(10)}}/>
      <View style={{width:width(40)}}>
      <Text style={{fontSize:32,fontFamily:"Ubuntu-Bold",color:"#fff"}}>
        Credit Cards
      </Text>
      </View>
      </View>

    
    </TouchableOpacity>
    <TouchableOpacity style={{width:width(80),height:height(20),marginLeft:width(5),marginBottom:height(2),marginRight:width(5),alignItems:"center",justifyContent:"center",backgroundColor:"#0354F1",borderRadius:12}} onPress={this.onCardPressed.bind(this,0)}>
     
    <View style={{height:height(12),width:width(65),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
      <Image source={require("../image/personalselector.png")} resizeMode="contain" style={{width:width(30),height:height(10)}}/>
      <View style={{width:width(40)}}>
      <Text style={{fontSize:32,fontFamily:"Ubuntu-Bold",color:"#fff"}}>
        Personal Loans
      </Text>
      </View>
      </View>
      </TouchableOpacity>

      <TouchableOpacity style={{width:width(80),height:height(20),marginLeft:width(5),marginRight:width(5),alignItems:"center",justifyContent:"center",backgroundColor:"#0354F1",borderRadius:12}} onPress={this.onCardPressed.bind(this,2)}>
     
    <View style={{height:height(12),width:width(65),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
      <Image source={require("../image/islamicselector.png")} resizeMode="contain" style={{width:width(30),height:height(10)}}/>
      <View style={{width:width(40)}}>
      <Text style={{fontSize:32,fontFamily:"Ubuntu-Bold",color:"#fff"}}>
        Islamic Finance
      </Text>
      </View>
      </View>
      </TouchableOpacity>
            

            </View >
            </KeyboardAwareScrollView>
        )
    }
}
