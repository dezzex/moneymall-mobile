//import liraries
import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,Picker,ActivityIndicator,AsyncStorage, TextInput} from 'react-native';
import {width,height} from 'react-native-dimension';

import LinearGradient from 'react-native-linear-gradient';

// create a component

class OtherLoans extends Component {
    static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
           <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       
        </TouchableOpacity>
       ,
        headerStyle:{
          height:height(8),
        }
        }
      };
    render() {
        return (
            <View style={styles.container}>
                <View style={{flexDirection:"row",width:width(99),height:height(35),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(45),height:height(30),alignItems:"center",justifyContent:"space-between"}}>
                    
                    <TouchableOpacity style={{width:width(40),backgroundColor:"#fff",borderRadius:24,opacity:0.5,height:height(6),alignItems:"center",marginTop:height(1),marginBottom:height(1),justifyContent:"center",flexDirection:"row"}}>
                    <Image source={require("../image/search.png")} style={{width:width(6),height:height(3)}}/>
            
                        <TextInput placeholder="Search" style={{width:width(25),height:height(6),color:"#fff"}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                    <Image source={require("../image/criteria/savings.png")} style={{width:width(30),height:height(10)}} resizeMode="contain"/>
                    </View>
                    <View style={{height:height(10),width:width(40),alignItems:"center",justifyContent:"center"}}>
                        <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold",color:"#344046"}}>
                            ACCOUNTS & SAVINGS
                        </Text>
                        <View style={{height:height(8),alignItems:"center",justifyContent:"center",width:width(40)}}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={{borderRadius:25,alignItems:"center",justifyContent:"center",width:width(35),height:height(4)}}
                 colors={["#59a9c6","#5cb5d2","#65d8ef"]} >
                        <TouchableOpacity style={{borderRadius:25,width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={() => this.props.navigation.navigate("Apply")}>
                    <Text style={{fontSize:20,color:"#fff",fontFamily:"Ubuntu-Regular"}}>
                    Details
                    </Text>
                    </TouchableOpacity>
                    </LinearGradient>
                    </View>
                    </View>
                    </TouchableOpacity>
                    </View>
                    <View style={{width:width(45),height:height(30),alignItems:"center",justifyContent:"space-between",marginTop:height(12)}}>
                
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff"}}>

                    <View style={{height:height(25),width:width(40),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                    <Image source={require("../image/criteria/islamic.png")} style={{width:width(30),height:height(10)}} resizeMode="contain"/>
            
                        </View>

                        <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold",color:"#344046"}}>
                            ISLAMIC FINANCE
                        </Text>
                        <View style={{height:height(8),alignItems:"center",justifyContent:"center",width:width(40)}}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={{borderRadius:25,alignItems:"center",justifyContent:"center",width:width(35),height:height(4)}}
                 colors={["#59a9c6","#5cb5d2","#65d8ef"]} >
                        <TouchableOpacity style={{borderRadius:25,width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={() => this.props.navigation.navigate("Apply")}>
                    <Text style={{fontSize:20,color:"#fff",fontFamily:"Ubuntu-Regular"}}>
                    Details
                    </Text>
                    </TouchableOpacity>
                    </LinearGradient>
                    </View>
                    </View>
                    </TouchableOpacity>
                    </View>
                </View>

                <View style={{flexDirection:"row",width:width(99),height:height(30),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(45),height:height(30),marginTop:height(5),alignItems:"center",justifyContent:"space-between"}}>
                   
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff"}}>
                    <View style={{height:height(25),width:width(40),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                    <Image source={require("../image/criteria/mortage.png")} style={{width:width(30),height:height(8)}} resizeMode="contain"/>
            
                    </View>
                        <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold",color:"#344046"}}>
                            HOME LOANS
                        </Text>
                        <View style={{height:height(8),alignItems:"center",justifyContent:"center",width:width(40)}}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={{borderRadius:25,alignItems:"center",justifyContent:"center",width:width(35),height:height(4)}}
                 colors={["#59a9c6","#5cb5d2","#65d8ef"]} >
                        <TouchableOpacity style={{borderRadius:25,width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={() => this.props.navigation.navigate("Apply")}>
                    <Text style={{fontSize:20,color:"#fff",fontFamily:"Ubuntu-Regular"}}>
                    Details
                    </Text>
                    </TouchableOpacity>
                    </LinearGradient>
                    </View>
                    </View>
                    </TouchableOpacity>
                    </View>
                    <View style={{width:width(45),height:height(30),alignItems:"center",justifyContent:"space-between",marginTop:height(5)}}>
                
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff"}}>
                    <View style={{height:height(25),width:width(40),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                    <Image source={require("../image/criteria/car.png")} style={{width:width(30),height:height(10)}} resizeMode="contain"/>
            
</View>
                        <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold",color:"#344046"}}>
                            AUTO LOAN
                        </Text>
                        <View style={{height:height(8),alignItems:"center",justifyContent:"center",width:width(40)}}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={{borderRadius:25,alignItems:"center",justifyContent:"center",width:width(35),height:height(4)}}
                 colors={["#59a9c6","#5cb5d2","#65d8ef"]} >
                        <TouchableOpacity style={{borderRadius:25,width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={() => this.props.navigation.navigate("Apply")}>
                    <Text style={{fontSize:20,color:"#fff",fontFamily:"Ubuntu-Regular"}}>
                    Details
                    </Text>
                    </TouchableOpacity>
                    </LinearGradient>
                    </View>
                    </View>
                    </TouchableOpacity>
                    </View>
                </View>

                <View style={{flexDirection:"row",width:width(99),height:height(30),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(45),height:height(30),alignItems:"center",justifyContent:"space-between"}}>
                   
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff"}}>
                    <View style={{height:height(25),width:width(40),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                    <Image source={require("../image/criteria/secure.png")} style={{width:width(30),height:height(10)}} resizeMode="contain"/>

</View>
                        <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold",color:"#344046"}}>
                            BUSINESS FINANCE
                        </Text>
                        <View style={{height:height(8),alignItems:"center",justifyContent:"center",width:width(40)}}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={{borderRadius:25,alignItems:"center",justifyContent:"center",width:width(35),height:height(4)}}
                 colors={["#59a9c6","#5cb5d2","#65d8ef"]} >
                        <TouchableOpacity style={{borderRadius:25,width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={() => this.props.navigation.navigate("Apply")}>
                    <Text style={{fontSize:20,color:"#fff",fontFamily:"Ubuntu-Regular"}}>
                    Details
                    </Text>
                    </TouchableOpacity>
                    </LinearGradient>
                    </View>
                    </View>
                    </TouchableOpacity>
                    </View>
                    <View style={{width:width(45),height:height(30),alignItems:"center",justifyContent:"space-between"}}>
                
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff"}}>
                    <View style={{height:height(25),width:width(40),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(40),height:height(15),alignItems:"center",justifyContent:"center"}}>
                    <Image source={require("../image/criteria/safety.png")} style={{width:width(30),height:height(10)}} resizeMode="contain"/>

</View>
                        <Text style={{fontSize:14,fontFamily:"Ubuntu-Bold",color:"#344046"}}>
                            INSURANCES
                        </Text>
                        <View style={{height:height(8),alignItems:"center",justifyContent:"center",width:width(40)}}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={{borderRadius:25,alignItems:"center",justifyContent:"center",width:width(35),height:height(4)}}
                 colors={["#59a9c6","#5cb5d2","#65d8ef"]} >
                        <TouchableOpacity style={{borderRadius:25,width:width(35),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={() => this.props.navigation.navigate("Apply")}>
                    <Text style={{fontSize:20,color:"#fff",fontFamily:"Ubuntu-Regular"}}>
                    Details
                    </Text>
                    </TouchableOpacity>
                    </LinearGradient>
                    </View>
                    </View>
                    </TouchableOpacity>
                    </View>
                </View>

                {/* <View style={{flexDirection:"row",width:width(99),height:height(30),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(45),height:height(30),alignItems:"center",justifyContent:"space-between"}}>
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff"}}>

                    </TouchableOpacity>
                    </View>
                    <View style={{width:width(45),height:height(30),alignItems:"center",justifyContent:"space-between"}}>
                
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff"}}>

                    </TouchableOpacity>
                    </View>
                </View> */}

                {/* <View style={{flexDirection:"row",width:width(99),height:height(30),alignItems:"center",justifyContent:"center"}}>
                    <View style={{width:width(45),height:height(30),alignItems:"center",justifyContent:"space-between"}}>
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff"}}>

                    </TouchableOpacity>
                    </View>
                    <View style={{width:width(45),height:height(30),alignItems:"center",justifyContent:"space-between"}}>
                
                    <TouchableOpacity style={{width:width(40),height:height(25),borderRadius:18,backgroundColor:"#fff"}}>

                    </TouchableOpacity>
                    </View>
                </View> */}
                
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height:height(95),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1972d7',
    },
});

//make this component available to the app
export default OtherLoans;
