import React, { Component ,createRef} from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,CheckBox,Picker,PickerIOS} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import CodeInput from 'react-native-confirmation-code-field';
export default class otpverifyscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index:'',
      index1:'',
      index2:'',
      index3:'',
      index4:'',
      index5:''
    };
  }
  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };
  onOtpEnetered(code){
    console.log('====================================');
    console.log(code);
    console.log('====================================');
  }
  handlerOnFulfill(code) {
    if (isValidCode(code)) {
      console.log(code);
    } else {
      this.clearCode();
    }
  };
 
  field = createRef();
 
  clearCode() {
    const { current } = this.field;
 
    if (current) {
      current.clear();
    }
  }
 
  pasteCode() {
    const { current } = this.field;
 
    if (current) {
      current.handlerOnTextChange(value);
    }
  }
  onResendPressed(){
    
  }
  onOtpEnetered(text,index){
    console.log("ffffff");
    
    if(index === 0){
      this.setState({index:text});
      this.secondTextInput.focus();
    }
  }
  componentDidMount(){
    console.log("dddddddddddddddd");
    
    this.firstTextInput.focus()
  }
  onNextClicked(){
    this.props.navigation.navigate("PaymentScreen")
  }
  render() {
    return (
      <View>
          <View style={{width:width(99),height:height(80),alignItems:"center",justifyContent:"center"}}>
            {/* <Text style={{fontSize:22}}>Verify Your ID</Text> */}
            <View style={{width:width(99),height:height(10),alignItems:"center",justifyContent:"center"}}>
            <Text  style={{fontSize:22,fontFamily:"Ubuntu-Bold"}}>OTP has been sent to your</Text>
            <Text style={{fontSize:22,fontFamily:"Ubuntu-Bold"}}>mobile number</Text>
            </View>
            <View style={{width:width(80),height:height(15),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                {/* <TextInput placeholder="0" maxLength={1} value={this.state.index} 
                 style={{width:width(12),fontSize:22,borderWidth:1,textAlign:"center",borderColor:"#0354F1",borderRadius:12}}  
                onChangeText={(text) => this.onOtpEnetered.bind(this,text,0)}
                ref={(input) => { this.firstTextInput = input; }}/> */}
              <TextInput placeholder="0"  maxLength={1}  onChangeText={(text) => this.setState({index1:text})}  style={{width:width(12),fontSize:22,borderWidth:1,textAlign:"center",borderColor:"#0354F1",borderRadius:12}}
              autoFocus={true}
              keyboardType={"number-pad"}
                onChangeText={(text) => { text.length === 1 ?  this.secondTextInput.focus() : "" }}
                ref={(input) => { this.firstTextInput = input; }}/>


                <TextInput placeholder="0"  maxLength={1}  onChangeText={(text) => this.setState({index1:text})}  style={{width:width(12),fontSize:22,borderWidth:1,textAlign:"center",borderColor:"#0354F1",borderRadius:12}}
              keyboardType={"number-pad"}
                onChangeText={(text) => { text.length === 1 ?  this.thirdTextInput.focus() : "" }}
                ref={(input) => { this.secondTextInput = input; }}/>

                <TextInput placeholder="0"  maxLength={1} onChangeText={(text) => this.setState({index2:text})}  style={{width:width(12),fontSize:22,borderWidth:1,textAlign:"center",borderColor:"#0354F1",borderRadius:12}}
              keyboardType={"number-pad"}
                onChangeText={(text) => { text.length === 1 ?  this.fourthTextInput.focus() : "" }}
                ref={(input) => { this.thirdTextInput = input; }}/>

                <TextInput placeholder="0"  maxLength={1}   onChangeText={(text) => this.setState({index3:text})} style={{width:width(12),fontSize:22,borderWidth:1,textAlign:"center",borderColor:"#0354F1",borderRadius:12}}
              keyboardType={"number-pad"}
                onChangeText={(text) => { text.length === 1 ?  this.fifthTextInput.focus() : "" }}
                ref={(input) => { this.fourthTextInput = input; }}/>

                <TextInput placeholder="0"  maxLength={1}  onChangeText={(text) => this.setState({index4:text})}  style={{width:width(12),fontSize:22,borderWidth:1,textAlign:"center",borderColor:"#0354F1",borderRadius:12}}
              keyboardType={"number-pad"}
                onChangeText={(text) => { text.length === 1 ?  this.sixthTextInput.focus() : "" }}
                ref={(input) => { this.fifthTextInput = input; }}/>

                <TextInput placeholder="0"  maxLength={1}   onChangeText={(text) => this.setState({index5:text})} style={{width:width(12),fontSize:22,borderWidth:1,textAlign:"center",borderColor:"#0354F1",borderRadius:12}}
              keyboardType={"number-pad"}
                onChangeText={(text) => { text.length === 1 ?  this.sixthTextInput.blur() : "" }}
                ref={(input) => { this.sixthTextInput = input; }}/>

            </View>
            <View style={{width:width(80),height:height(10)}}>
            <Text  style={{fontSize:14,fontFamily:"Ubuntu-Regular"}}>
            Didn't receive the OTP?  <Text style={{fontSize: 16,fontFamily:"OpenSans-Regular"}} onPress={this.onResendPressed.bind(this)} style={{color:"#097FF5",textDecorationLine:"underline",fontFamily:"OpenSans-Bold"}} textDecorationLine={"underline"}>Resend
            </Text>
            </Text>
            </View>
            <View style={{width:width(80),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
   
                   {/* <TouchableOpacity style={{width:width(30),height:height(6),marginRight:width(5),alignItems:"center",justifyContent:"center",borderWidth:1,borderColor:"#0D9CF8"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{fontSize:18}}>
                        Resend
                      </Text>
                   </TouchableOpacity> */}
              <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(80),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(30),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"OpenSans-Bold"}}>
                        SUBMIT
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>

            </View>
          </View>
      </View>
    );
  }
}
