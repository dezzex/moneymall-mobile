import React, { Component ,createRef} from 'react';
import { View, Text ,Image,ScrollView,TouchableOpacity,KeyboardAvoidingView,TextInput,Picker,PermissionsAndroid,Platform, Keyboard} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import { Header } from 'react-navigation-stack';

import { CardIOModule, CardIOUtilities } from 'react-native-awesome-card-io';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
export default class PaymentScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index:0,
      index1:0,
      index2:0,
      index3:0,
      cardnumber:'',
      cardname:'',
      validity:'',
      cvv:''
    };
  }
  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };
  onOtpEnetered(code){
    console.log('====================================');
    console.log(code);
    console.log('====================================');
  }
  handlerOnFulfill(code) {
    if (isValidCode(code)) {
      console.log(code);
    } else {
      this.clearCode();
    }
  };
 
  field = createRef();
 
  clearCode() {
    const { current } = this.field;
 
    if (current) {
      current.clear();
    }
  }
   onScanPressed = async () =>{
      console.log("heeee");
      var self = this;
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "Cool Photo App Camera Permission",
            message:
              "Cool Photo App needs access to your camera " +
              "so you can take awesome pictures.",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the camera");
          CardIOModule
      .scanCard()
      .then(card => {
        // the scanned card
        console.log(card);
        if(card !== undefined){
            self.setState({cardnumber:card.cardNumber,cardname:card.cardholderName,cvv:card.cvv,validity:card.expiryMonth+"/"+card.expiryYear})
        }
        
      })
      .catch(() => {
        // the user cancelled
      })
        } else {
          console.log("Camera permission denied");
        }
      } catch (err) {
        console.warn(err);
      }
    
      
  }
  componentDidMount(){
    if (Platform.OS === 'ios') {
        CardIOUtilities.preload();
      }
  }
  pasteCode() {
    const { current } = this.field;
 
    if (current) {
      current.handlerOnTextChange(value);
    }
  }
  onResendPressed(){
    
  }
  onCardNumberPressed(text){
    console.log(text.length);
    if(text.length === 4 || text.length === 9 || text.length === 14){
      text = text + "-"
    }
    else if(text.length === 19){
      this.cardnameinput.focus();
    }
    this.setState({cardnumber:text})
    
  }
  onValidityPressed(text){
    if(text.length === 2){
      text = text + "/"
    }
    else if(text.length === 5){
      this.cvvinput.focus();
    }
    this.setState({validity:text})
    
  }
  onCVVPressed(text){
    if(text.length === 3){
      Keyboard.dismiss()
    }
    this.setState({cvv:text})
  }
  onNextClicked(){
    this.props.navigation.navigate("CreditScore")
  }
  render() {
    return (
      <KeyboardAwareScrollView
      keyboardVerticalOffset = {Header.HEIGHT + 20} // adjust the value here if you need more padding
      style = {{ flex: 1 }}
      behavior = "padding" >
          <View style={{width:width(99),height:height(90),alignItems:"center",justifyContent:"center"}}>
           <View style={{width:width(90),height:height(17),alignItems:"center",justifyContent:"flex-start",borderColor:"grey",borderLeftWidth:1,borderBottomWidth:1,borderRightWidth:1,borderRadius:12}}>
           <LinearGradient style={{alignItems:"center",flexDirection:"row",paddingLeft:width(2),justifyContent:"flex-start",width:width(90),height:height(5)}}
                 colors={["#0D9CF8","#0354F1"]} >
                       <Image source={require("../image/aecbwhite.png")} resizeMode="contain" style={{width:width(65),height:height(4)}}/>
                    {/* <View style={{width:width(45),height:height(4),marginLeft:width(1),alignItems:"flex-start",justifyContent:'center'}}>
                    <Text style={{fontSize:28,fontFamily:"Ubuntu-Bold",color:"#fff"}}>
                    Credit Report  
                    </Text> 
                    </View> */}
            </LinearGradient>
            <View style={{width:width(85),height:height(10)}}>
                <View style={{width:width(85),height:height(10),alignItems:"center",justifyContent:"center"}}>
                <Text  style={{fontSize:11,fontFamily:"OpenSans-Regular"}}>
                The Credit Score is a Three Digit Number provided by Al Etihad Credit Bureau,  If in case you don't have a score at the moment, a response will provide for the same. AECB charges to check your records is 
                
                </Text>
                </View>
                <View style={{width:width(85),alignItems:"flex-start",justifyContent:"center",height:height(1)}}>
               <Text style={{fontSize:11,fontFamily:"OpenSans-Bold"}}>AED 31.50 Incl VAT</Text> 
               </View>
                </View>
               
           </View>
          
           <View style={{height:height(70),width:width(90),alignItems:"flex-start",justifyContent:"center"}}>
           
             <View style={{width:width(90),height:height(10),alignItems:"center",justifyContent:"center"}}>      
            <TouchableOpacity style={{width:width(85),height:height(6),alignItems:"center",justifyContent:"center",borderRadius:12,borderWidth:1,flexDirection:"row"}} onPress={this.onScanPressed.bind(this)}>
                <Image source={require("../image/camera.png")} style={{width:width(5),height:height(5)}} resizeMode="contain"/>
                <View style={{width:width(35),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Regular"}}>
                    Scan Card
                </Text>
                </View>
            </TouchableOpacity>
               </View>
               
            <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(88),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                 <TextInput
                  textAlign={"center"}
                  autoFocus={true}
               placeholder="Card Number"
               keyboardType="number-pad"
               value={this.state.cardnumber}
               onChangeText={this.onCardNumberPressed.bind(this)}
               style={{width:width(90),alignItems:"center",justifyContent:"center"}}
              //  ref={(input) => { this.fifthTextInput = input; }}
               type="name"
               />
          
            </TouchableOpacity>
            </View>

            {/* <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> FULL NAME </Text> */}
            <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(88),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                 <TextInput
                  textAlign={"center"}
               placeholder="Name on card"
              //  value={this.state.cardname}
               style={{width:width(90),alignItems:"center",justifyContent:"center"}}
               ref={(input) => { this.cardnameinput = input; }}
               onChangeText={(text) => this.setState({cardname:text})}
               type="name"
               />
          
            </TouchableOpacity>
            </View>
            {/* <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold"}}> MM/YY </Text> */}
            <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(88),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                 <TextInput
                  textAlign={"center"}
                  value={this.state.validity}
               placeholder="Expiry Date - MM/YY"
               style={{width:width(90),alignItems:"center",justifyContent:"center"}}
               onChangeText={this.onValidityPressed.bind(this)}
               type="name"
               keyboardType="number-pad"
               />
          
            </TouchableOpacity>
            </View>
            <View style={{flexDirection:"row",marginTop:height(1),marginBottom:height(3),alignItems:"center",justifyContent:"center",borderColor:"#B8B8B8",borderWidth:2,borderColor:"#E6E6E6"}}>
            <TouchableOpacity style={{width:width(88),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
                 <TextInput
                  textAlign={"center"}
                  // value={this.state.cvv}
               placeholder="CVV"
               keyboardType="number-pad"
               style={{width:width(90),alignItems:"center",justifyContent:"center"}}
               ref={(input) => { this.cvvinput = input; }}
               onChangeText={this.onCVVPressed.bind(this)}
               type="name"
               />
          
            </TouchableOpacity>
            </View>
            <View style={{height:height(6),width:width(90),alignItems:"center",justifyContent:"center"}}>
            <Text style={{fontSize:12,fontFamily:"OpenSans-Regular"}}>
                Your Card data is saved in a PCI DSS compliant secure vault.
            </Text>
            </View>
            
            <View style={{width:width(90),alignItems:"center",justifyContent:"center"}}>
            <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(80),height:height(6)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(80),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18,fontFamily:"OpenSans-Bold"}}>
                        Buy Now
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
              </View>

           </View>
         
      </View>
      
      </KeyboardAwareScrollView>
    );
  }
}
