//import liraries
import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,Picker,ActivityIndicator,AsyncStorage, TextInput} from 'react-native';
import {width,height} from 'react-native-dimension';

// create a component
class PersonalLoanDetails extends Component {

    static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
           <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
       
        </TouchableOpacity>
       ,
        headerStyle:{
          height:height(8),
        }
        }
      };
    componentDidMount(){
        
      var value = this.props.navigation.getParam('value', undefined);
      console.log("fffffff",value);
      
    }
    render() {
        var value = this.props.navigation.getParam('value', undefined);
        var tenure = this.props.navigation.getParam('tenure', undefined);
        var loan = this.props.navigation.getParam('loan', undefined);
        return (
            <View style={styles.container}>
                <View style={{width:width(100),height:height(15),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",color:"#fff"}}>
                    Personal Instalment Loan
                </Text>
                <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold",color:"#000"}}>
                    EXPLORE AND ENJOY THE WORLD OF
                </Text>
                <Text style={{fontSize:16,fontFamily:"Ubuntu-Bold",color:"#000"}}>
                   TRAVEL AND LIFESTYLE PERKS
                </Text>
                </View>
                <View style={{height:height(32),marginTop:height(8)}}>
                <View style={{width:width(85),height:height(30),borderRadius:18,backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}}>
                <View style={{backgroundColor:"#f0f0f0",width:width(75),height:height(20),alignItems:"center",justifyContent:"center",borderRadius:12}}>
                <Image  source={{uri: value.card_image}} style={{width:width(65),height:height(12),borderRadius:12}} resizeMode="contain"/>
                    </View>
                <View style={{height:height(25),width:width(85),alignItems:"flex-start",justifyContent:"flex-start",flexDirection:"row"}}>
                <View style={{width:width(27),height:height(20),flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",color:"#000"}}>
                   {value.interest_rate}
                </Text>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Interest
                </Text>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Rate
                </Text>
                </View>
                <View style={{width:width(27),height:height(20),flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:24,fontFamily:"Ubuntu-Bold",color:"#000"}}>
                   {value.arrangement_fee}
                </Text>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Processing
                </Text>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Fees
                </Text>
                </View>
                <View style={{width:width(27),height:height(20),flexDirection:"column",alignItems:"center",justifyContent:"center"}}>
                <TouchableOpacity style={{width:width(22),alignItems:"center",justifyContent:"center"}}>
                                <Text style={{color:value.id % 2 === 0 ? "red" : "green",fontSize:24,fontFamily:"Ubuntu-Bold"}}>
                                {value.id % 2 === 0 ? "LOW" : "HIGH"}
                                </Text>
                            </TouchableOpacity>
                <View style={{width:width(22),height:height(4),marginLeft:width(5),alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:12,fontFamily:"Ubuntu-Regular",color:"#747474"}}>
                   Chances of Approval
                </Text>
                </View>
                </View>
                </View>
                </View>
                </View>
                <View style={{height:height(34)}}>
                <View style={{width:width(85),height:height(32),borderRadius:12,backgroundColor:"#fff"}}>
                <View style={{width:width(85),height:height(10),alignItems:"flex-start",padding:height(1),justifyContent:"center",flexDirection:"row"}}>
                    <View style={{width:width(27),height:height(5),alignItems:"flex-start",justifyContent:"space-between"}}>
                        <Text style={{fontSize:9,color:"#000"}}>
                            Applying For
                        </Text>
                        <View style={{width:width(25),height:height(3),alignItems:"flex-start",justifyContent:"center",borderWidth:1}}>
                            <Text>
                                {loan}
                            </Text>
                        </View>

                    </View>
                    <View style={{width:width(8),height:height(5),justifyContent:"center",alignItems:"center"}}>
 <Text>
                               for
                            </Text>
                    </View>
                    <View style={{width:width(10),height:height(5),alignItems:"flex-start",justifyContent:"space-between"}}>
                        <Text style={{fontSize:9,color:"#000"}}>
                            Months
                        </Text>
                        <View style={{width:width(8),height:height(3),alignItems:"center",justifyContent:"center",borderWidth:1}}>
                            <Text>
                                {tenure}
                            </Text>
                        </View>

                    </View>
                    <View style={{width:width(10),height:height(5),alignItems:"center",justifyContent:"center"}}>
                            <Text style={{fontSize:52,color:"#747474"}}>
                                >
                            </Text>
                    </View>
                    <View style={{width:width(20),paddingTop:height(2),alignItems:"flex-start",justifyContent:"center"}}>
                            <Text style={{fontSize:24,color:"#000",fontFamily:"Ubuntu-Bold"}}>
                                {value.monthly_repayment.split(" ")[1]}
                            </Text>
                            <Text style={{fontSize:6,color:"#747474"}}>
                                Monthly Repayment (AED)
                            </Text>
                    </View>
                </View>
                <View style={{height:height(15),width:width(85),alignItems:"center",justifyContent:"center"}}>
                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",width:width(75),borderBottomWidth:1,borderBottomColor:"#747474"}}>
                    <View style={{width:width(55),alignItems:"flex-start",justifyContent:"center",paddingLeft:width(6)}}>
                    <Text style={{fontSize:16,color:"#747474"}}>
                        Minimum Salary:
                    </Text>
                    </View>
                    <View style={{width:width(30),alignItems:"flex-start",justifyContent:"center"}}>
                    <Text style={{fontSize:16,color:"#707273"}}>
                        AED 5,000
                    </Text>
                    </View>
                </View>

                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",width:width(75),borderBottomWidth:1,borderBottomColor:"#747474"}}>
                    <View style={{width:width(55),alignItems:"flex-start",justifyContent:"center",paddingLeft:width(6)}}>
                    <Text style={{fontSize:16,color:"#747474"}}>
                        Buy Out Loan:
                    </Text>
                    </View>
                    <View style={{width:width(30),alignItems:"flex-start",justifyContent:"center"}}>
                    <Text style={{fontSize:16,color:"#707273"}}>
                        {value.buy_outloans.toUpperCase()}
                    </Text>
                    </View>
                </View>

                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",width:width(75),borderBottomWidth:1,borderBottomColor:"#747474"}}>
                    <View style={{width:width(55),alignItems:"flex-start",justifyContent:"center",paddingLeft:width(6)}}>
                    <Text style={{fontSize:16,color:"#747474"}}>
                        Salary Transfer:
                    </Text>
                    </View>
                    <View style={{width:width(30),alignItems:"flex-start",justifyContent:"center"}}>
                    <Text style={{fontSize:16,color:"#707273"}}>
                    {value.salary_transferloan.toUpperCase()}
                    </Text>
                    </View>
                </View>

                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",width:width(75),borderBottomWidth:1,borderBottomColor:"#747474"}}>
                    <View style={{width:width(55),alignItems:"flex-start",justifyContent:"center",paddingLeft:width(6)}}>
                    <Text style={{fontSize:16,color:"#747474"}}>
                        Best Feature:
                    </Text>
                    </View>
                    <View style={{width:width(30),alignItems:"flex-start",justifyContent:"center"}}>
                    <Text style={{fontSize:16,color:"#707273"}}>
                    {value.Loan_Feature}
                    </Text>
                    </View>
                </View>
                </View>
                <View style={{width:width(85),height:height(8),alignItems:"center",justifyContent:"center"}}>
                    <TouchableOpacity style={{backgroundColor:"#65a3dd",borderRadius:25,width:width(80),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={() => this.props.navigation.navigate("Apply")}>
                    <Text style={{fontSize:24,color:"#fff",fontFamily:"Ubuntu-Regular"}}>
                    Apply Now
                    </Text>
                    </TouchableOpacity>

                </View>
                </View>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height:height(90),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1972d7',
    },
});

//make this component available to the app
export default PersonalLoanDetails;
