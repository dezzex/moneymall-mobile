//import liraries
import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,Picker,ActivityIndicator,AsyncStorage, TextInput, ImageBackground} from 'react-native';
import {width,height} from 'react-native-dimension';

import LinearGradient from 'react-native-linear-gradient';
import branch, { BranchEvent } from 'react-native-branch'
// create a component
class Refer extends Component {
async onReferPressed(){
  console.log("hereeeee");
  
        let branchUniversalObject = await branch.createBranchUniversalObject('canonicalIdentifier', {
            locallyIndex: true,
            title: 'Cool Content!',
            contentDescription: 'Cool Content Description',
            contentMetadata: {
              ratingAverage: 4.2,
              customMetadata: {
                prop1: 'test',
                prop2: 'abc'
              }
            }
          })

          let linkProperties = {
            feature: 'share',
            channel: 'RNApp'
        }
        
        let controlParams = {
             $desktop_url: 'http://desktop-url.com/monster/12345'
        }
        
        let {url} = await branchUniversalObject.generateShortUrl(linkProperties, controlParams)


          let shareOptions = { messageHeader: 'Check this out', messageBody: 'No really, check this out!' }
// let linkProperties = { feature: 'share', channel: 'RNApp' }
// let controlParams = { $desktop_url: url, $ios_url: url }
let {channel, completed, error} = await branchUniversalObject.showShareSheet(shareOptions, linkProperties, controlParams)
          console.log(url);
          
    }
    static navigationOptions = ({ navigation  }) => {
        return{
        headerRight:null,
        
        headerLeft:<TouchableOpacity style={{flexDirection:"row",width:width(25),alignItems:"center",justifyContent:"center"}} >
            <Image source={require("../image/back.png")} style={{width:width(3),height:height(3)}}/>
            <View style={{height:height(4),width:width(15),alignItems:"center",justifyContent:"center"}}>
            <Text style={{fontSize:18,fontFamily:"Ubuntu-Regular",color:"#fff"}}>Back</Text>
            </View>
        </TouchableOpacity>,
        headerStyle:{
          height:height(5),
          borderBottomWidth:0,
        backgroundColor: '#1972d7',
        }
    }
      };
    render() {
        return (
            <ImageBackground style={styles.container} source={require("../image/refer.png")}>
               <TouchableOpacity style={{flexDirection:"row",width:width(80),height:height(6),marginTop:height(15),borderRadius:25,alignItems:"center",justifyContent:"center",backgroundColor:"#1972d7"}} onPress={this.onReferPressed.bind(this)}>
                 <Text style={{fontSize: 18,fontFamily:"Ubuntu-Bold",color:"#fff"}}>
                   SHARE - BE A GOOD FRIEND
                 </Text>
           
        </TouchableOpacity> 
            </ImageBackground>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height:height(92),
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default Refer;
