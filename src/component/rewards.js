import React, { Component } from 'react'
import { View, Text ,Image,ImageBackground,StyleSheet,TouchableOpacity} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';

var rewardlist =[
    {image:require("../image/1.png")},
    {image:require("../image/2.png")},
    {image:require("../image/3.png")},
    {image:require("../image/4.png")},

]

var bglist=[
    {image:require("../image/bg_1.png")},
    {image:require("../image/bg_2.png")},
    {image:require("../image/bg_1.png")},
    {image:require("../image/bg_2.png")},
]

export default class Rewards extends Component {
  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  onApplyClicked(){
      this.props.navigation.navigate("MyRewards");
  }
  onCheckPressed(){
    this.props.navigation.navigate("CreditScore")
  }
  onCreditScoreClicked(){
    this.props.navigation.navigate("CreditScore")
  }
  onMyRewardsClicked(){
    this.props.navigation.navigate("Rewards")
  }
  onMyMoneyPressed(){
    this.props.navigation.navigate("MyMoney")
  }

  render() {
    return (
      <View style={styles.container}>
      <ImageBackground style={{width:width(100),height:height(100),alignItems:"center",justifyContent:"center"}} source={require("../image/magnates.png")}>
              <TouchableOpacity style={{width:width(85),height:height(6),alignItems:"center",justifyContent:"center",backgroundColor:"#fff",opacity:0.75,marginTop:height(24),borderRadius:24,flexDirection:"row"}} onPress={() => this.props.navigation.navigate("Apply")}>
                <Text style={{fontSize:22,color:"#156eb2",fontFamily:"Ubuntu-Bold"}}>
                  Request Your Invite
                </Text>
                <Image source={require("../image/go.png")} style={{width:width(5),height:height(5)}} resizeMode="contain"/>
              </TouchableOpacity>
      </ImageBackground>
      </View>
    //   <View style={styles.container}>
    //     <View style={{height:height(80),alignItems:"center",justifyContent:"center",marginTop:height(2)}}>
    //     <Text style={{fontSize:26,fontWeight:"bold",marginBottom:height(2)}}> Magnates </Text>
    //   <View style={{width:width(99),height:height(25)}}>
    //       <View style={{flexDirection:"row",height:height(5)}}>
    //         <View style={{width:width(80)}}>
    //             <Text>
    //             Rewards from  <Text style={{fontWeight:"bold"}}>
    //             Moneymall
    //                 </Text>
    //             </Text>
    //         </View>
    //         <Text>
    //             View All
    //         </Text>
    //       </View>
    //       <View style={{height:height(20)}}>
    //       <ScrollView
    //         showsHorizontalScrollIndicator={false}
    //         horizontal={true}>
    //             {rewardlist.map((value,idx) => {
    //                     return(
    //                         <Image key={idx} source={value.image} style={{width:height(10),height:height(10),borderRadius:height(10)/2,marginLeft:width(5)}} resizeMode="contain"/>
    //                     )
    //             })}
    //         </ScrollView>
        
              
    //         </View>
            
    //     </View>

    //         <View style={{height:height(40)}}>
    //       <ScrollView
    //         showsHorizontalScrollIndicator={false}
    //         horizontal={true}>
    //             {bglist.map((value,idx) => {
    //                     return(
    //                         <Image key={idx} source={value.image} style={{width:height(30),height:height(40),borderRadius:12,marginLeft:width(5)}} resizeMode="contain"/>
    //                     )
    //             })}
    //         </ScrollView>
        
              
    //         </View>
    //         <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(100),height:height(8)}}
    //              colors={["#0D9CF8","#0354F1"]} >
    //                <TouchableOpacity style={{width:width(100),height:height(8),alignItems:"center",justifyContent:"center"}} onPress={this.onApplyClicked.bind(this)}>
    //                     <Text style={{color:"#fff",fontSize:18,fontWeight:"bold"}}>
    //                     Your Rewards
    //                   </Text>
    //                </TouchableOpacity>
              
    //           </LinearGradient>
    //           </View>
    //           <View style={{width:width(100),height:height(10),flexDirection: 'row',shadowOpacity: 0.75,borderTopLeftRadius: 25,
    //   borderTopRightRadius:25,
    //   shadowRadius: 15,
    //   elevation:8,
    //   shadowColor: '#707070',
    //   backgroundColor:"#fff",
    //   shadowOffset: { height: 10, width: 10 }}}>
    //     <TouchableOpacity style={{width:width(20),height:height(10),alignItems: 'center',borderTopLeftRadius: 25}} onPress={() => this.props.navigation.navigate("Home")}>
    //     <Image  source={require("../image/moneymallbottom.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
    //     <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
    //         Home
    //     </Text>
    //     </TouchableOpacity>
    //     <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyMoneyPressed.bind(this)}>
    //     <Image  source={require("../image/mymoneybottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
    //     <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
    //          My Cards
    //     </Text>
    //     </TouchableOpacity>
    //     <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onCreditScoreClicked.bind(this)}>
    //     <Image  source={require("../image/creditbottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
    //     <View style={{width:width(20),alignItems:"center"}}>
    //     <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
    //         My Credit
    //     </Text>
    //     </View>
    //     </TouchableOpacity>

    //     <TouchableOpacity style={{width:width(20),alignItems: 'center'}}  onPress={() => 
    // this.props.navigation.navigate("MyGoals")}>
    //     <Image  source={require("../image/mygoalbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
    //     <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
    //         My Goals
    //     </Text>
    //     </TouchableOpacity>

    //     <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyRewardsClicked.bind(this)}>
    //     <Image  source={require("../image/myrewardbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
    //     <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
    //         Magnates
    //     </Text>
    //     </TouchableOpacity>
      
    //   </View>
    //    </View>

    );
  }
}

const styles = StyleSheet.create({ 
    container:{
        flex:1,
        flexDirection: 'column',
        alignItems: 'center',
        height:height(92),
        justifyContent:'center'
    },
})
