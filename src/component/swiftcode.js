import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    Picker,
    ActivityIndicator,
    Clipboard,
    ToastAndroid,
    AsyncStorage
} from 'react-native'; 
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';

import  worldlist from './world.json';
import list from './swiftcode.json';
const fromCountry =  {
    "name": "United Arab Emirates",
    "native": "United Arab Emirates",
    "phone": "971",
    "continent": "UAE",
    "capital": "Abu Dhabi",
    "currency": "AED",
    "languages": [
      "arabic"
    ],
    "emoji": "🇦🇪",
    "emojiU": "U+1F1EC U+1F1E7"
  }

export default class SwiftCode extends Component {
    constructor(props){
        super(props)
        this.state={
            currentCountry:'',
            currentCity:'',
            currentBank:'',
            countryList:["UAE","India","USA"],
            cityList:["Dubai","Abu Dhabi","Ajman"],
            banknameList:[],
            swiftlist:[],
            searchResult:'',
            selectedCity:'',
            selectedBranch:'',
            selectedBank:'',
            branchlist:[],

            
            toCountry:''
            
        }
    }  
    onToCountryChanged(itemValue){
        this.setState({toCountry: itemValue})
     }
    onCheckPressed(){
        this.props.navigation.navigate("CreditScore")
      }
      onCreditScoreClicked(){
        this.props.navigation.navigate("CreditScore")
      }
      onMyRewardsClicked(){
        this.props.navigation.navigate("Rewards")
      }
      onMyMoneyPressed(){
        this.props.navigation.navigate("MyMoney")
      }
      onBankChanged(itemValue){
            var branchlist = [];
            var self = this;
            this.state.banknameList.map((bank,idx) => {
                if(itemValue.Bank === bank.Bank && self.state.selectedCity.toString() === bank.City.toString()){
                    branchlist.push(bank);
                }

                if(idx === this.state.banknameList.length-1){
                    self.setState({selectedBank:itemValue,branchlist:branchlist})
                }
            })
      }
      onCityChanged(itemValue){
        var branchlist = [];
        var self = this;
        list.data.map((bank,idx) => {
            if(self.state.selectedBank.Bank === bank.Bank && itemValue.toString() === bank.City.toString()){
                branchlist.push(bank);
            }
            if(idx === this.state.banknameList.length-1){
            self.setState({selectedCity:itemValue,branchlist:branchlist})
            }
        })
      }
    onSearchClicked(){
            if(list.data.length > 0){

                list.data.map((bank,idx) => {
                    if(bank.Bank === this.state.selectedBank.Bank && bank.City === this.state.selectedCity && bank["Branch Name"] === this.state.selectedBranch["Branch Name"]){
                        this.setState({searchResult:bank})
                    }
                })
            }
    }
    static navigationOptions = ({ navigation  }) => {
        return{
            headerRight:null,
           
           headerTitle:
        
        <TouchableOpacity style={{width:width(80),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
        <View style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",width:height(6),width:height(6),borderRadius: height(6)/2}}>
            <Image source={require("../image/swifticon.png")}  width={width(40)} height={height(10)} resizeMode="contain"/>
        </View>
        <View style={{marginLeft:width(2)}}>
        <Text style={{fontSize: 18,
    fontFamily:"Ubuntu-Bold"}}>
          Swift Code
        </Text>
        </View>
        </TouchableOpacity>,
        headerLeft:
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
        <Image style={{marginLeft:width(3)}} source={require("../image/headerback.png")} width={width(7)} height={height(6)} resizeMode="contain"/>
        </TouchableOpacity>,
        headerStyle:{
          borderBottomWidth:0,
          height:height(15),
          elevation: 0,
          shadowOpacity: 0
        }}
      };
      componentDidMount(){
          var self = this;
          var tempList = [];
          var nameList = [];
          list.data.map((bank,idx) => {
              if(nameList.includes(bank.Bank.toLowerCase().trim()) === false){
                  tempList.push(bank);
                  var bankName = bank.Bank.toLowerCase();
                  bankName = bankName.trim();
                  nameList.push(bank.Bank.toLowerCase());
              }
              
          })
          if(tempList !== []){
              var selectedCity = "Dubai";
              var branchList = [];
          tempList.map((bank) => {
              if(tempList[0].Bank === bank.Bank && selectedCity === bank.City){
                    branchList.push(bank);
              }
          })
          self.setState({swiftlist:list.data,selectedBank:list.data[0],banknameList:tempList,selectedCity:selectedCity,branchlist:branchList,selectedBranch:branchList[0]})
          }
      }
      onCopyClicked(copyText){
          Clipboard.setString(copyText)
          ToastAndroid.show("Copied to Clipboard....",ToastAndroid.SHORT)
      }
      onBranchSelected(itemValue,itemIndex){
        this.setState({currentBank: itemValue})
        if(this.state.swiftlist.length > 0){
            this.state.swiftlist.map((bank,idx) => {
                if(bank.Bank === itemValue.Bank && bank.Branch === itemValue.Branch){
                    this.setState({searchResult:bank})
                }
            })
        }
        // this.onSearchClicked();
      }
    render() {
        var branchIdx = -1;
        return (
            <View style={styles.container}>
                <View style={{height:height(73),width:width(99),alignItems:"center"}}>

               {/* <View style={{height:height(6),width:width(88),marginTop:height(1),marginBottom:height(3), shadowOffset: { width: 10, height: 10 },
                    shadowColor: 'black',
                    
                    alignItems:"center",
                    justifyContent:"center",
                    shadowOpacity: 2,
                    borderColor:"#1972D7",
                    borderWidth:1,
                    borderRadius:12,
                    backgroundColor : "#f7faff"}}>
               <Picker
               style={{height:height(6),width:width(88)}}
                    selectedValue={this.state.toCountry}
                    
                    onValueChange={this.onToCountryChanged.bind(this)}> */}
                        {/* {Object.keys(worldlist).map((key,idx)=>{ */}
                            {/* return( */}
                             {/* <Picker.Item style={{marginLeft: width(2)}} label={"   "+fromCountry.emoji+"  "+"      "+fromCountry.name} value={fromCountry} /> */}
                            {/* ) */}
                        {/* } */}
                        {/* )} */}
                    {/* </Picker>   */}
               {/* <Picker
               style={{height:height(6),width:width(88)}}
                    selectedValue={this.state.currentCountry}
                    
                    onValueChange={(itemValue, itemIndex) =>
                        this.setState({currentCountry: itemValue})
                    }>
                        {this.state.countryList.map((city,idx)=>{
                            return(
                             <Picker.Item label={city} value={city} key={idx} />
                            )
                        }
                        )}
                    </Picker>   */}
               {/* </View> */}

               <View style={{width:width(88)}}>
                    <Text style={{fontSize: 20,fontFamily:"Ubuntu-Bold"}}>
                        Bank
                    </Text>
               </View>
               <View  style={{height:height(8),width:width(88),marginTop:height(1),marginBottom:height(3),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               {this.state.banknameList !== []
                      ?
             <Picker
             style={{height:height(6),width:width(88)}}
                  selectedValue={this.state.selectedBank}
                  
                  onValueChange={this.onBankChanged.bind(this)}>
                     
                    {this.state.banknameList.map((bank,idx)=>{
                          return(
                           <Picker.Item label={bank.Bank} value={bank} key={idx} />
                          )
                      }

                      )}
                  </Picker>  
                  :
                  <View/>
                }
             </View>




               <View style={{width:width(88)}}>
                    <Text style={{fontSize: 20,fontFamily:"Ubuntu-Bold"}}>
                        City
                    </Text>
               </View>
               <View  style={{height:height(8),width:width(88),marginTop:height(1),marginBottom:height(3),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
             
               <Picker
               style={{height:height(6),width:width(88)}}
                    selectedValue={this.state.selectedCity}
                    
                    onValueChange={this.onCityChanged.bind(this)}>
                        {this.state.cityList.map((city,idx)=>{
                            return(
                             <Picker.Item label={city} value={city} key={idx} />
                            )
                        }
                        )}
                    </Picker>  
               </View>

               <View style={{width:width(88)}}>
                    <Text style={{fontSize: 20,fontFamily:"Ubuntu-Bold"}}>
                        Branch
                    </Text>
               </View>
               <View  style={{height:height(8),width:width(88),marginTop:height(1),marginBottom:height(3),flexDirection:"row",backgroundColor:"#F6F6F6",alignItems:"center",justifyContent:"center",height:height(6)}}>
               
                   
               {/* <View style={{height:height(6),width:width(88),marginTop:height(1),marginBottom:height(3), shadowOffset: { width: 10, height: 10 },
                    shadowColor: 'black',
                    shadowOpacity: 1,
                    
                    alignItems:"center",
                    justifyContent:"center",
                    borderColor:"#1972D7",
                    borderWidth:1,
                    borderRadius:12,
                    backgroundColor : "#f7faff"}}> */}
                {this.state.branchlist !== undefined && this.state.branchlist.length > 0
                ?
               <Picker
               style={{height:height(6),width:width(88)}}
                    selectedValue={this.state.selectedBranch}
                    
                    onValueChange={(branch,idx) => this.setState({selectedBranch:branch})}>
                  
                        {this.state.branchlist !== undefined  && this.state.branchlist.map((bank,idx)=>{
                            return(
                             <Picker.Item label={bank["Branch Name"]} value={bank} key={idx} />
                            )
                        }
                        )}
                    </Picker>  
                :
                <Picker
               style={{height:height(6),width:width(88)}}
                    selectedValue={this.state.currentBank}
                    enabled={false}
                    onValueChange={(bank, index) => this.onBranchSelected(bank, index)}>
                     <Picker.Item label={"No Branch"} value={"No Branch"}  />
                          
                    </Picker>  
                    }
               </View>
               {/* <View style={{width:width(90)}}>
                    <Text style={{fontSize:20,fontWeight:"bold"}}>
                        Branch Name
                    </Text>
               </View> */}
               {/* <View style={{height:height(6),width:width(88),marginTop:height(3),marginBottom:height(3), shadowOffset: { width: 10, height: 10 },
                    shadowColor: 'black',
                    shadowOpacity: 1,
                    
                    alignItems:"center",
                    justifyContent:"center",
                    elevation: 8,
                    borderRadius:12,
                    backgroundColor : "#f7faff"}}>
                {this.state.currentBank.Bank !== ''
                ?
               <Picker
               style={{height:height(6),width:width(88)}}
                    selectedValue={this.state.currentBank}
                    
                    onValueChange={(itemValue, itemIndex) =>
                        this.setState({currentBank: itemValue})
                    }>
                        {this.state.swiftlist !== undefined && this.state.swiftlist.map((bank,idx)=>{
                           
                            if(this.state.currentBank.Bank === bank.Bank){

                                branchIdx = branchIdx + 1;
                            return(
                                
                             <Picker.Item label={bank.Branch} value={bank} key={branchIdx} />
                            )
                           
                            }
                            else{
                                return(<View/>)
                            }
                        }
                        )}
                    </Picker> 
                :
                <ActivityIndicator/>
                    } 
               </View> */}

               <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(88),marginTop:height(1),marginBottom:height(1),height:height(6),
                    borderRadius:12}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(88),height:height(4),alignItems:"center",justifyContent:"center"}} onPress={this.onSearchClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:20,fontFamily:"Ubuntu-Bold"}}>
                        Find
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>

              <View style={{height:height(12)}}>
                  {this.state.searchResult === ""
                  ?
                  <Text>

                  </Text>
                  :
                  <View  style={{height:height(12)}}>
                  <View style={{width:width(95),flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                    
                  <View style={{alignItems:"center"}}>
                      <Image style={{width:width(8),height:height(8)}} source={require("../image/swifticon.png")} resizeMode="contain"/>
                  </View>
                  <View style={{marginLeft:width(5)}}>
                            <Text style={{fontSize: 22,fontFamily:"Ubuntu-Bold"}}>
                                {this.state.searchResult.SwiftCode}
                            </Text>
                            </View>
                </View>
                <View style={{width:width(95),height:height(4),alignItems:"center",}}>
                <Text style={{fontFamily:"Ubuntu-Regular"}}>
                {this.state.searchResult.Bank}
                </Text>
                <Text style={{fontFamily:"Ubuntu-Regular"}}>
                {this.state.searchResult.Branch},{this.state.searchResult.City}
                </Text>
                </View>
                <View style={{height:height(8),width:width(99),alignItems:"center",justifyContent:"center",flexDirection:"row",marginTop:height(1)}}>
                    <View style={{width:width(30),height:height(5),marginRight:width(5),borderRadius:12,borderColor:"#1972D7",borderWidth:2,alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontFamily:"Ubuntu-Medium"}}>
                    {this.state.searchResult.SwiftCode} 
                    </Text>
                    </View>
                    <TouchableOpacity style={{width:width(16),height:height(5),alignItems:"center",justifyContent:"center",borderRadius:25,backgroundColor:"#1972D7"}} onPress={this.onCopyClicked.bind(this,this.state.searchResult.SwiftCode)}>
                        <Text style={{fontSize: 12,color:"#fff",fontFamily:"Ubuntu-Medium"}}>
                            Copy
                        </Text>
                    </TouchableOpacity>
                </View>
        
                </View>
                
                }
              </View>
</View>
<View style={{width:width(100),height:height(10),flexDirection: 'row',shadowOpacity: 0.75,borderTopLeftRadius: 25,
      borderTopRightRadius:25,
      shadowRadius: 15,
      elevation:8,
      shadowColor: '#707070',
      backgroundColor:"#fff",
      shadowOffset: { height: 10, width: 10 }}}>
        <TouchableOpacity style={{width:width(20),height:height(10),alignItems: 'center',borderTopLeftRadius: 25}} onPress={() => this.props.navigation.navigate("Home")}>
        <Image  source={require("../image/moneymallbottom.png")} style={{width:width(6),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Home
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyMoneyPressed.bind(this)}>
        <Image  source={require("../image/mymoneybottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
             My Cards
        </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onCreditScoreClicked.bind(this)}>
        <Image  source={require("../image/creditbottom.png")} style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <View style={{width:width(20),alignItems:"center"}}>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Credit
        </Text>
        </View>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}}>
        <Image  source={require("../image/mygoalbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            My Goals
        </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{width:width(20),alignItems: 'center'}} onPress={this.onMyRewardsClicked.bind(this)}>
        <Image  source={require("../image/myrewardbottom.png")}  style={{width:width(8),height:height(6)}} resizeMode="contain"/>
        <Text style={{fontSize: 14,fontFamily:"OpenSans-Regular"}}>
            Magnates
        </Text>
        </TouchableOpacity>
      
      </View>
          </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        height:height(90),
    },
});
