import React, { Component } from 'react'
import * as conversionLib from '../../mainframe/lib/openapi_data_conversion';
export default class litabheader extends Component {
    constructor(props){
        super(props)
        this.state={
            splittedIdx:15,
            themeName: (conversionLib.getLocalStorageValue("core.app.theme") !== null) ? conversionLib.getLocalStorageValue("core.app.theme") : "LightTheme"
        }
    }
    componentWillReceiveProps(nextProps){
        if(this.props.parentWidth !== nextProps.parentWidth && nextProps.parentWidth !== undefined){
           
            if(nextProps.parentWidth !== undefined){
                var elem = document.getElementById("tabheader_"+this.props.id)
                for(var i =0;i<=elem.children.length;i++){
                var headerElem = elem.childNodes[i];
                    if(headerElem !== undefined){
                    var percentage = nextProps.parentWidth;
                    // var wholeNum = percentage.substring(0,percentage.length-1);
                    headerElem.style.width = 100/this.props.children.length + "%";
                    
                }
                }
            }
        }
        this.setState({
            themeName: (conversionLib.getLocalStorageValue("core.app.theme") !== null) ? conversionLib.getLocalStorageValue("core.app.theme") : "LightTheme"
        });
    }
    updateDimensions(){
        
        
        if(this.props.width !== undefined) return;

        
        var splittedIdx = 15;
        if(window.innerWidth < 400)
            splittedIdx = 3;
        else if(window.innerWidth >= 400 && window.innerWidth <= 900)
            splittedIdx = 5;
        else if(window.innerWidth > 800 && window.innerWidth <= 950)
            splittedIdx = 10
     
       
        this.setState({splittedIdx:splittedIdx})

    }

    componentDidMount(){

        var elem = document.getElementById("tabheader_"+this.props.id)
        window.addEventListener("resize", this.updateDimensions.bind(this));

        if(this.props.width !== undefined){
            for(var i =0;i<=elem.children.length;i++){
                var headerElem = elem.childNodes[i];
                if(headerElem !== undefined){
                    headerElem.style.width = this.props.width/this.props.children.length + 'px';
                    headerElem.childNodes[0].style.maxWidth = this.props.width/this.props.children.length + 'pc';
                }
            }
            var splittedIdx = 15;
            if(this.props.width < 100)
                splittedIdx = 3;
            else if(this.props.width >= 100 && this.props.width <= 300)
                splittedIdx = 5;
            else if(this.props.width > 300 && this.props.width <= 400)
                splittedIdx = 10
            else if(this.props.width > 400 && this.props.width <= 500)
                splittedIdx = 15;
            else
                splittedIdx = 20;
            
            this.setState({splittedIdx:splittedIdx})
        }
        this.setState({
            themeName: (conversionLib.getLocalStorageValue("core.app.theme") !== null) ?conversionLib.getLocalStorageValue("core.app.theme") : "LightTheme"
        });
}
    render() {

        const theme = this.state.themeName;
        
        return (
            <div id={"tabheader_"+this.props.id} className="tabheader">
            {this.props.children.length > 0 
            ?
            this.props.children.map((val,idx) => {
            console.log('=======hereeeee======================');
            console.log(val.props.tabLabel);
            console.log('====================================');
            return(
            <div  key={idx} className={theme+" tabcomp"} tabIndex={"tab_"+idx} id={val.props.id} style={val.props.disabled !== undefined && val.props.disabled ? {cursor: "not-allowed",} : {}}> 
            <div id={val.props.id} tabIndex={"tab_"+idx} key={idx} style={val.props.disabled !== undefined && val.props.disabled ? { pointerEvents: "none",opacity: "0.4"} : {}}>
            
                    
                    {
                    val.props.tabLabel.props !== undefined     // for object multi term
                    ?
                            val.props.tabLabel.length <= this.state.splittedIdx
                            ?
                            <div id={val.props.id} tabIndex={"tab_"+idx} className={theme+ "tablabel"}>
                            {val.props.tabLabel}
                            </div>
                            :
                            <div id={val.props.id} tabIndex={"tab_"+idx} className={theme+ "tablabel"}>
                            {val.props.tabLabel} 
                
                            <div className={theme+" tooltiptext"}>
                            {val.props.tabLabel}
                            </div>
                            </div>
                    :
                            val.props.tabLabel.length <= this.state.splittedIdx                //for string    
                            ?
                            <div id={val.props.id} tabIndex={"tab_"+idx} className={theme+ "tablabel"}>
                            {val.props.tabLabel}
                            </div>
                            :
                            <div id={val.props.id} tabIndex={"tab_"+idx} className={theme+ "tablabel"}>
                            {val.props.tabLabel.substring(0,this.state.splittedIdx)+"..."}
                
                            <div className={theme+" tooltiptext"}>
                            {val.props.tabLabel.substring(0,this.state.splittedIdx)+"..."}
                            </div>
                            </div>
                    }
            
            </div>
            </div>
            )
            })
            :
            <div/>
            }
            
            </div>
        )
    }
}
