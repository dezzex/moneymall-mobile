import React, { Component ,createRef} from 'react';
import { View, Text ,Image,TouchableOpacity,StyleSheet,CheckBox,Picker,PickerIOS} from 'react-native';
import {width,height} from 'react-native-dimension';
import LinearGradient from 'react-native-linear-gradient';
import DocumentPicker from 'react-native-document-picker';

export default class uploadfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCompleteAppy:false,
      onSucess:false
    };
  }
  static navigationOptions = ({ navigation  }) => {
    return{
    headerRight:null,
    
    headerLeft:
    <TouchableOpacity onPress={() => navigation.navigate("Home")}>
       <Image  source={require("../image/MonayMallheadericon.png")} style={{width:width(30),height:height(8),alignItems:"center",justifyContent:"center",marginLeft:width(6)}} resizeMode="contain"/>
   
    </TouchableOpacity>
   ,
    headerStyle:{
      height:height(8),
    }
    }
  };
onNextClicked(){
  this.setState({isCompleteAppy:true})

}   
 onSalaryCertificateClicked(){
    try {
        const res =  DocumentPicker.pick({
          type: [DocumentPicker.types.images],
        });
        console.log(
          res.uri,
          res.type, // mime type
          res.name,
          res.size
        );
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw err;
        }
      }
}
onCompleteClicked(){
    this.setState({onSucess:true})
}
onHomeClicked(){
  this.props.navigation.navigate("Home");
}

  render() {
    return (
      <View style={{flex:1,alignItems:"center",height:height(90)}}>
        {this.state.onSucess
        ?
        <View style={{width:width(100),height:height(70),alignItems:"center"}}>
          <Image source={require("../image/sucess.png")} resizeMode="contain" style={{width:width(70),height:height(65)}}/>
          <View style={{width:width(85),alignItems:"center",justifyContent:"center"}}>
          <Text>
                your Application is forwarded to the bank Thank you so much for applying through Moneymall
          </Text>
          </View>
          <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6),marginTop:height(5)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onHomeClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18}}>
                        Home
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>
        </View>
        :

        <View style={{width:width(100),alignItems:"center",justifyContent:"center"}}>
        <View style={{width:width(85),height:height(10)}}>
        <Text style={{fontSize: 16}}> Upload other supporting documents to fasten your process ( take a clear pics from your mobile/Mail your docs to ) </Text>
        </View>
        {this.state.isCompleteAppy
        ?
        <View  style={{width:width(100),alignItems:"center",justifyContent:"center"}}>
        <View style={{height:height(55),width:width(99),alignItems:"center",justifyContent:"center"}}>
                <TouchableOpacity style={{height:height(22),width:width(45),alignItems:"center",marginBottom:height(2),justifyContent:"center",borderWidth:1,borderColor:"#707070"}} onPress={this.onSalaryCertificateClicked.bind(this)}>
                    <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text>Bank Statement</Text>
                    </View>
                    <Image source={require("../image/addicon.png")} resizeMode="contain" style={{width:width(15),height:height(15)}}/>
                    <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text></Text>
                    </View>

                </TouchableOpacity>
                <TouchableOpacity style={{height:height(22),width:width(45),alignItems:"center",marginBottom:height(2),justifyContent:"center",borderWidth:1,borderColor:"#707070"}} onPress={this.onSalaryCertificateClicked.bind(this)}>
                  <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text>Loan/Cards Letters</Text>
                    </View>
                    <Image source={require("../image/addicon.png")} resizeMode="contain" style={{width:width(15),height:height(15)}}/>
                    <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text></Text>
                    </View>

                </TouchableOpacity>

        </View>
        <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6),marginTop:height(5)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onCompleteClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18}}>
                        Complete & Apply
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>

        </View>    
        :
        <View style={{width:width(100),alignItems:"center",justifyContent:"center"}}>
        <View style={{height:height(55),width:width(99),alignItems:"center",justifyContent:"center"}}>
                <TouchableOpacity style={{height:height(22),width:width(45),alignItems:"center",marginBottom:height(2),justifyContent:"center",borderWidth:1,borderColor:"#707070"}} onPress={this.onSalaryCertificateClicked.bind(this)}>
                    <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text>Salary Certificate</Text>
                    </View>
                    <Image source={require("../image/addicon.png")} resizeMode="contain" style={{width:width(15),height:height(15)}}/>
                    <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text></Text>
                    </View>

                </TouchableOpacity>
                <TouchableOpacity style={{height:height(22),width:width(45),alignItems:"center",marginBottom:height(2),justifyContent:"center",borderWidth:1,borderColor:"#707070"}} onPress={this.onSalaryCertificateClicked.bind(this)}>
                  <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text>Passport</Text>
                    </View>
                    <Image source={require("../image/addicon.png")} resizeMode="contain" style={{width:width(15),height:height(15)}}/>
                    <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text></Text>
                    </View>

                </TouchableOpacity>

        </View>
        <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(90),height:height(6),marginTop:height(5)}}
                 colors={["#0D9CF8","#0354F1"]} >
                   <TouchableOpacity style={{width:width(90),height:height(6),alignItems:"center",justifyContent:"center"}} onPress={this.onNextClicked.bind(this)}>
                        <Text style={{color:"#fff",fontSize:18}}>
                        Next
                      </Text>
                   </TouchableOpacity>
              
              </LinearGradient>

        </View>    
        }
      </View>
        }
        </View>
    );
  }
}
